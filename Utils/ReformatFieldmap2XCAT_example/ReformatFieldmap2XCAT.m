% Example script to reformat an in vivo field map to an XCAT mask. 
% This script is setup to work for a single slice only. 
% 
% Robbert JH van Gorkum, Sebastian Kozerke.
% University and ETH Zurich, IBT, November 17th 2020
%
% Code complementary to MRXCAT-CDTI 

%-------------------------------------------------------------------------%
% Add utils folder to path 
%-------------------------------------------------------------------------%
addpath(genpath(fullfile('Utils')));

%-------------------------------------------------------------------------%
% Set target directory for exporting fieldmaps 
%-------------------------------------------------------------------------%
target_directory = fullfile(cd,'Export');

%-------------------------------------------------------------------------%
% Select XCAT mask case 
%-------------------------------------------------------------------------%
XCAT_case = 'LR'; % 'AP' or 'LR'

%-------------------------------------------------------------------------%
% Load fieldmap data 
%-------------------------------------------------------------------------%
load(fullfile('Data','fieldmap_data.mat')); 

%-------------------------------------------------------------------------%
% Get the raw unwrapped field map 
%-------------------------------------------------------------------------%
phi_1 = b0_data(:,:,1); 
phi_2 = b0_data(:,:,2); 

% Complex division to subtract the phases 
delta_phi_wrapped = (phi_2 .* conj(phi_1)) ./ (phi_1 .* conj(phi_1)); 
delta_phi_wrapped = angle(delta_phi_wrapped); 
delta_phi_wrapped(isnan(delta_phi_wrapped)) = 0; 

% Get magnitude data of first TE image for computing the mask later  
anat_magn = abs(phi_1); 
anat_magn = cast(anat_magn,'uint8'); 

% Use Otsu's method to generate a mask 
[counts,~] = imhist(nonzeros(anat_magn),8);
T = otsuthresh(counts);
mask = imbinarize(anat_magn,T);
se = strel('disk',12);
mask = imopen(mask,se); 
delta_phi_wrapped = delta_phi_wrapped .* mask; 

% Unwrap map and compute the field map 
% - May need manual intervention to set the reference point
reference_point = [fix(size(delta_phi_wrapped,1)/2),fix(size(delta_phi_wrapped,2)/2),1]; 
delta_phi_unwrapped = GoldsteinUnwrap2D(delta_phi_wrapped, mask, reference_point);
delta_f0 = delta_phi_unwrapped ./ (2*pi*dTE);

%-------------------------------------------------------------------------%
% Manually mask of liver signal
% - This helps the extrapolation later on, otherwise the gradient between
%   liver and myocardium is enhanced, leading to artificial effects during
%   the forward model simulation. 
%-------------------------------------------------------------------------%
if exist(fullfile('Data','liver_mask.mat'),'file') == 2
    load(fullfile('Data','liver_mask.mat')); 
else
    figure; imagesc(delta_f0); daspect([1 1 1]); colormap jet; axis off; 
    p = drawpolygon('LineWidth',2,'Color','cyan');
    ROI_positions = p.Position; 
    liver_mask = poly2mask(ROI_positions(:,1),ROI_positions(:,2),size(delta_f0,1),size(delta_f0,2));
    save(fullfile('Data','liver_mask.mat'),'liver_mask'); 
end 

mask(liver_mask) = 0; 

%-------------------------------------------------------------------------%
% Apply cutoff (optional) 
%-------------------------------------------------------------------------%
f0_max_cutoff = 300; % [Hz]
mask(delta_f0 > f0_max_cutoff) = 0;

delta_phi_unwrapped = delta_phi_unwrapped .* mask; 
delta_f0 = delta_f0 .* mask;

%-------------------------------------------------------------------------%
% Apply median filtering to remove any outliers in the field map after
% unwrapping, i.e. incorrectly unfolded phase data. 
%-------------------------------------------------------------------------%
mask = permute(mask,[1 2]);
delta_f0_filt = medfilt2nan(delta_f0, 3);

%-------------------------------------------------------------------------%
% Extrapolate field map to support reformatting 
%-------------------------------------------------------------------------%
delta_f0_filt(mask==0) = NaN;
delta_f0_smooth = smoothn(delta_f0_filt,mask,1e-4);

%-------------------------------------------------------------------------%
% Reformat data to fit XCAT mask orientation 
%-------------------------------------------------------------------------%
switch lower(XCAT_case) 
    case 'lr'
        load(fullfile('Data','XCATMasks_LR.mat'));
        
        % The loaded XCAT pixel sizes are defined for 
        % LR preparation direction. 
    case 'ap'
        load(fullfile('Data','XCATMasks_AP.mat'));
        
        % Be aware that for non-square matrix sizes this has to be
        % applied. 
        XCAT_pixel_size_mm = fliplr(XCAT_pixel_size_mm); 
end 

XCAT_matrix_size   = size(XCATMasks);  

% Define transformation parameters for XCAT object 
% Offset LR (+ : shift to left, - : shift to right)
% Offset AP (+ : shift up, - : shift down)
% - May require retweaking when using a different field map
rot_angle = 50; % [�] 
offset_LR = 9; % [mm]
offset_AP = -60; % [mm]

anat_magn = cast(anat_magn,'single'); 

% In-plane rotation matrix to align XCAT data
Rz = @(a) [cosd(a), -sind(a), 0; sind(a), cosd(a), 0; 0, 0, 1]; % In-plane rotation 

% Transformation matrix for field map
B_ijk2mps(1:3,4)= 0;
B_ijk2mps(3,3)  = 1;
B_ijk2raf = B_mps2raf*B_ijk2mps;

% Transformation matrix for XCAT data 
% - Definition of the ijk2mps matrix may require intervention!!
A_ijk2mps        = [0 XCAT_pixel_size_mm(2) 0 0; XCAT_pixel_size_mm(1) 0 0 0; 0 0 1 0; 0 0 0 1];
A_ijk2mps(1:3,4) = 0; %->sampling index symmetric -> remove offsets
A_ijk2mps(3,3)   = 1;
A_ijk2mps(1,4)  = offset_LR; % Offset LR  (+ : shift to left, - : shift to right)
A_ijk2mps(2,4)  = offset_AP; % Offset AP (+ : shift up, - : shift down)

% Initialize the transformation matrix 
A_mps2raf = [1 0 0 0; 0 1 0 0; 0 0 1 0; 0 0 0 1]; 

% Transfer the mps2raf coordinates of the field map
A_mps2raf(1:3,1:4) = B_mps2raf(1:3,1:4); 

% Adjust the in-plane rotation of the XCAT mask until the resliced field
% map matches the main orientation 
A_mps2raf(1:3,1:3) = A_mps2raf(1:3,1:3) * Rz(rot_angle);
A_ijk2raf=A_mps2raf*A_ijk2mps;
C = pinv(B_ijk2raf) * A_ijk2raf;   

% Define the grid the reslicing has to occur on 
nx = XCAT_matrix_size(1); 
ny = XCAT_matrix_size(2); 
nx_regrid = XCAT_matrix_size(1); 
ny_regrid = XCAT_matrix_size(2); 

xq = linspace(-nx/2,nx/2,nx_regrid);
yq = linspace(-ny/2,ny/2,ny_regrid);

[x,y,z] = ndgrid( xq, yq, 0 ); 
X = [x(:), y(:), z(:), ones( size(x(:)))]'; 
Xref = C * X;
Xref(3,:)=round(Xref(3,:)); 

% Perform the reslicing of the field map 
xq_f0 = linspace(-size(delta_f0_smooth,2)/2,size(delta_f0_smooth,2)/2,size(delta_f0_smooth,2));
yq_f0 = linspace(-size(delta_f0_smooth,1)/2,size(delta_f0_smooth,1)/2,size(delta_f0_smooth,1)); 

resliced_b0_temp = interp2( xq_f0, yq_f0, delta_f0_smooth, Xref(2,:), Xref(1,:), 'linear');  
resliced_b0_nan = reshape( resliced_b0_temp, [nx_regrid,ny_regrid,1] ); 
nan_points = isnan(resliced_b0_nan);

% Perform the reslicing of the magnitude data as reference
xq_anat = linspace(-size(anat_magn,2)/2,size(anat_magn,2)/2,size(anat_magn,2));
yq_anat = linspace(-size(anat_magn,1)/2,size(anat_magn,1)/2,size(anat_magn,1));

resliced_anat_temp = interp2( xq_anat, yq_anat, anat_magn, Xref(2,:), Xref(1,:), 'linear');  
resliced_anat = reshape( resliced_anat_temp, [nx_regrid,ny_regrid,1] );  
resliced_anat(nan_points) = 0;

%-------------------------------------------------------------------------%
% Touch up any isnan areas with extrapolation and smoothing
%-------------------------------------------------------------------------%
fieldmaps = smoothn(resliced_b0_nan,1e2);

%-------------------------------------------------------------------------%
% Dampen the background, lung, and stomach areas 
%-------------------------------------------------------------------------%
foreground = ones(size(XCATMasks)); 
foreground(XCATMasks==20) = 0; % Stomach
foreground(XCATMasks==16) = 0; % Lung 
foreground(XCATMasks==0) = 0;  % Background 

SE = strel('disk',8'); 
foreground = imdilate(foreground,SE); 

% Get rid of the vein signal in the lung
% - May require intervention to set the cutoff-value
L = bwlabel(foreground);

% Determine the amount of pixels per label
max_labels = max(L(:)); 
bincounts = histcounts(L,max_labels); 
bincounts_normalized = bincounts ./ numel(L); 
selected_bins = (bincounts_normalized > 0.1); 

selected_labels = selected_bins .* (1:max_labels); 
labeledobjects = L; 
labeledobjects(labeledobjects > (max(selected_labels)-1)) = 0; 
dampening_mask = logical(labeledobjects); 

dist = bwdist(dampening_mask); 
dist(dist == 0) = 1; 
dist = nthroot(dist,4); 

fieldmaps = fieldmaps ./ dist; 

%-------------------------------------------------------------------------%
% Export field map 
%-------------------------------------------------------------------------%
if ~isfolder(fullfile('Export')) 
   mkdir(fullfile('Export')); 
end
    
switch lower(XCAT_case) 
    case 'lr'
        save(fullfile('Export','fieldmaps_LR.mat'),'fieldmaps');
    case 'ap'
        save(fullfile('Export','fieldmaps_AP.mat'),'fieldmaps');
end 

%-------------------------------------------------------------------------%
% Compute and display gradient maps
% - Fx = gradient in horizontal direction
% - Fy = gradient in horizontal direction
%-------------------------------------------------------------------------%
[Fx,Fy] = gradient(fieldmaps,XCAT_pixel_size_mm(1), XCAT_pixel_size_mm(2));

figure; 

subplot(211); 
imagesc(abs(Fx)); colormap jet; daspect([1 1 1]); axis off; colorbar; 
title('Fx'); 

subplot(212); 
imagesc(abs(Fy)); colormap jet; daspect([1 1 1]); axis off; colorbar; 
title('Fy'); 

%-------------------------------------------------------------------------%
% Display output 
%-------------------------------------------------------------------------%
figure, 

subplot(241); 
imagesc(anat_magn); colormap gray; daspect([1 1 1]); axis off; freezeColors; 
title('Abs - TE_{1}');

subplot(242); 
imagesc(delta_phi_wrapped); colormap jet; daspect([1 1 1]); axis off; freezeColors 
hcb=colorbar;
hcb.Title.String = "[rad]";
title('\Deltaphi wrapped');

subplot(243); 
imagesc(delta_phi_unwrapped); colormap jet; daspect([1 1 1]);axis off; freezeColors
hcb=colorbar;
hcb.Title.String = "[rad]";
title('\Deltaphi unwrapped - Masked');

subplot(244); 
imagesc(delta_f0); colormap jet; daspect([1 1 1]); axis off;  freezeColors
hcb=colorbar;
hcb.Title.String = "[Hz]";
title('\DeltaF_{0} [Hz] - Masked');

subplot(245); 
imagesc(delta_f0_smooth); colormap jet; daspect([1 1 1]); axis off;  freezeColors;
hcb=colorbar;
hcb.Title.String = "[Hz]";
title('\DeltaF_{0} [Hz] - Extrapolated.');

subplot(246); 
imagesc(XCATMasks(:,:,1,1)); colormap gray; daspect([1 1 1]); axis off; freezeColors
title('XCAT mask - Target')

subplot(247); 
imagesc(resliced_anat); colormap gray; daspect([1 1 1]); axis off; freezeColors
title('Abs - TE_{1} - Resliced.')

subplot(248); 
imagesc(fieldmaps); colormap jet; daspect([1 1 1]); axis off; freezeColors;
hcb=colorbar;
hcb.Title.String = "[Hz]";
title('\DeltaF_{0} [Hz] - Resl.')