function open_parallel_workers(profile,nWorkers)
if nWorkers > 1
    % Setup the local parcluster 
    clusterPool = parcluster(profile);

    % Delete pre-existing parallel jobs
    try
        delete(clusterPool.Jobs);
        pause(0.1); 
    catch
    end

    % Initialize workers 
    UpdatedClusterMethods = exist('gcp', 'file');

    if UpdatedClusterMethods % new matlab (R2014a and beyond)
        nWorkers = min(clusterPool.NumWorkers, nWorkers);

        % try to open pool until successful - sometimes it just
        % fails randomly...
        while isempty(gcp('nocreate'))
            try
                clusterPool = parpool(clusterPool, nWorkers);
                disp('opened cluster parpool');
            catch
                disp('failed to open cluster parpool');
            end
        end
        % Stop matlab from opening pool on parfor automatically
        ps = parallel.Settings;
        ps.Pool.AutoCreate = false;
    else
        % In case of older Matlab releases. 
        while (matlabpool('size') <= nWorkers)
            try
                matlabpool(clusterPool, nWorkers);
            catch
            end
        end
    end
else
   % Stop matlab from opening pool on parfor automatically
    ps = parallel.Settings;
    ps.Pool.AutoCreate = false;
end

end