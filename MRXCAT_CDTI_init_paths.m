p =  fileparts(which('MRXCAT_CDTI_init_paths.m')); cd(p); 

% Add to path
addpath(genpath('Data'));
addpath(genpath(fullfile('Utils','NUFFT')));
addpath(genpath(fullfile('Utils','smoothn')));
addpath(genpath(fullfile('Utils','cluster')));

% Extract zip-files containing all *.bin files
if ~isfile(fullfile('Data','dti_breathhold','DIFF_act_1.bin'))
    binfilepath_bh = fullfile('Data','dti_breathhold'); 
    unzip(fullfile(binfilepath_bh,'5X_OVS_1_slice.zip'),binfilepath_bh);
end

if ~isfile(fullfile('Data','dti_freebreathing','DIFF_act_1.bin'))
    binfilepath_fb = fullfile('Data','dti_freebreathing'); 
    unzip(fullfile(binfilepath_fb,'5X_OVS_7_slices.zip'),binfilepath_fb);
end

% Extract the zip-files containing the eigenvalue maps 
if ~isfolder(fullfile('Data','dti_breathhold','evmaps'))
    binfilepath_fb = fullfile('Data','dti_breathhold'); 
    unzip(fullfile(binfilepath_fb,'evmaps.zip'),binfilepath_fb);
end

if ~isfolder(fullfile('Data','dti_freebreathing','evmaps'))
    binfilepath_fb = fullfile('Data','dti_freebreathing'); 
    unzip(fullfile(binfilepath_fb,'evmaps.zip'),binfilepath_fb);
end