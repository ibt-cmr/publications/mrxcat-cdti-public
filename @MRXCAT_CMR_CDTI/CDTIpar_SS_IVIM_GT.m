function CDTIpar_SS_IVIM_GT( MRX, filename )
% This function is the parameter file for MRXCAT_CMR_CDTI.
% Change parameters in section "MRXCAT settings" only
%
% Note: Not all combinations of any parameter values are possible.
%       Some parameter changes require changes in the XCAT files.
%       E.g. if you want to include free-breathing, you will need additional
%       XCAT motion states, i.e. additional	*.bin files.

% --------------------------------------------------------------------
%   MRXCAT settings
% --------------------------------------------------------------------
RhoMuscle   = 80.0;                         % Proton density muscle [%]
RhoFat      = 70.0;                         % Proton density fat    [%]
RhoBlood    = 95.0;                         % Proton density blood  [%]
RhoLiver    = 90.0;                         % Proton density liver  [%]
RhoBone     = 12.0;                         % Proton density bone   [%]

T1Muscle    = 1000.0;                       % T1 muscle             [ms]
T1Fat       = 350.0;                        % T1 fat                [ms]
T1Blood     = 1500.0;                       % T1 blood              [ms]
T1Liver     = 800.0;                        % T1 liver              [ms]
T1Bone      = 250.0;                        % T1 bone               [ms]

T2Muscle    = 50.0;                         % T2 muscle             [ms]
T2Fat       = 30.0;                         % T2 fat                [ms]
T2Blood     = 100.0;                        % T2 blood              [ms]
T2Liver     = 50.0;                         % T2 liver              [ms]
T2Bone      = 20.0;                         % T2 bone               [ms]

T2starMuscle    = 35.0;                     % T2star muscle         [ms] 
T2starFat       = 20.0;                     % T2star fat            [ms] 
T2starBlood     = 60.0;                     % T2star blood          [ms] 
T2starLiver     = 26.0;                     % T2star liver          [ms] 
T2starBone      = 10.0;                     % T2star bone           [ms] 

% Distribution standard deviations for tissue properties 
% - Set as percentage of max. signal
T2starStd   = 15;                           % Std. dev T2star       [%-max signal]

% Bounding box setup for for single-slice *.bin file
BoundingBox = [0.1,0.5;0.26,0.66;0.0,1.0]; % BoundingBox in rel. units

RotationXYZ = [115.0;35.0;240.0];           % Rotations about x,y,z [deg]  (default: 115/35/240)
                                            % x=(RL) y=(AP) z=(FH)
                                                                                                        
SNR         = 50;                           % signal-to-noise ratio of b = 0 s/mm2 image for a single NSA
Coils       = 4;                            % number of coils (Biot-Savart)
CoilDist    = 450;                          % body radius [mm] = distance of coil centres from origin
CoilsPerRow = 8;                            % number of coils on 1 "ring" or row of coil array (default: 8)
CoilRadius  = 0;                            % Coil radius [mm]. If 0, coil radius is generated automatically. 

RespiratoryMotion = 0;                      % 0 = Static, 1 = Breath-hold, 2 = free-breathing
NumberOfAverages = 1;                       % Number of signal averages

ExportMasks = 'no';                         % Export XCAT and LV masks 
ExportData2Mat = 'no';                      % Export MRXCAT-CDTI data to mat-files 

% --------------------------------------------------------------------
% Select a sub-selection of all XCAT slices 
% - Check the XCAT *.bin files for the total number of slices to set 
%   the correct range. 
% - If left empty, all slices are loaded in. 
% --------------------------------------------------------------------
SelectedSlices = [];

% --------------------------------------------------------------------
% Lesion parameters 
% - Add lesion section in the left ventricular myocardium
% - Supports multiple lesions
%
% Example 
% - LesionSectorAngles = [0,60; 240,300];
% - LesionTMDranges    = [0.3,0.6; 0.4,0.8];
% - LesionValue        = 0;
% --------------------------------------------------------------------
EnableLesion       = 'no'; 
LesionSectorAngles = [240,300]; % [degrees]
LesionTMDranges    = [0.2,0.6]; % [0-1]
LesionValue        = 0; % [-]

% --------------------------------------------------------------------
% CDTI parameters 
% --------------------------------------------------------------------
% (I) Available encoding schemes 
%       See MRXCAT_CMR_CDTI function 'setDiffusionDirections' for details.
%
%       1 (default) = perfusion-suppressive scheme
%       2           = 6 directions
%       3           = 9 directions
%       4           = 12 directions
%       5           = 15 directions
%       6           = 30 directions
% 
% (II) Enabling diffusion and/or perfusion contrast 
%       MRXCAT-CDTI uses tensors to generate both diffusion and 
%       perfusion contrast, according to
%
%       S = S(0)*( f*exp(-b*(gT*Dstar*g)) + (1-f)*exp(-b*(gT*D*g)) )
% 
%       To perform a perfusion/IVIM simulation: 
%           - Select encoding scheme of choice
%           - Fill 'bValues' with b-values of interest 
%           - Set mean diffusivity (D), pseudo-diffusion (Dstar) coefficient, and
%             perfusion fraction
% 
%       To perform a cDTI only simulation:
%           - Select encoding scheme of choice
%           - Fill 'bValues' with only 0 and max b-value, e.g. [0, 450].
%           - Set mean diffusivity (D)
%           - Set perfusion fraction to 0 (optional for pseudo-diffusion
%             coefficient) 
% --------------------------------------------------------------------
EncodingScheme    = 2; 
bValues           = [0,25,50,75,100,150,200,250,300,400,500];
Sequence          = 'steam'; % 'steam' or 'spin-echo'

% Set linearly-varying helix angle range
HelixAngleRange   = 120; 

% E2A sheet angle definition 
% - Define either below or load from a *.mat file 
% - *.mat file location: selected bin-file path\e2amaps
LoadSheetAngleMaps = 'yes';
SheetAngleValue = 35; % [�]
E2ASubFolder = 'evmaps_STEAM_1_slice_IVIM'; 
E2AFileName     = fullfile(E2ASubFolder,'e2a_maps0.mat'); 
    
% Eigenvalue maps 
% Define either below or load from *.mat file  
% - Locally defined tensors can be found below. 
% - *.mat file location: selected bin-file path\evmaps
LoadEigenValueMaps = 'no';
EVSubfolder = 'evmaps_STEAM_1_slice_IVIM'; 
EVFileNameD     = fullfile(EVSubfolder, 'diffusion_healthy_ev_maps0.mat'); 
EVFileNameDstar = fullfile(EVSubfolder,'perfusion_healthy_ev_maps0.mat'); 
EVFileNameDlesion = fullfile(EVSubfolder,'diffusion_lesion_ev_maps0.mat'); 
EVFileNameDstarlesion = fullfile(EVSubfolder,'perfusion_lesion_ev_maps0.mat'); 

% Define diffusion and sequence parameters for the local option [mm2/s]
% - Note: FA values are defined by the default tensor. 
switch lower(Sequence)
    case {'spin-echo','se'}
        D           = 14.5E-4;
        Dstar       = 0E-3; % mm2/s
        f           = 0.0; 
        fStd        = 0; % [-] 
        TR          = 3000;  % Repetition time       [ms]
        TR_std      = 0;     % Standard deviation    [%-of TR]
        TE          = 88;    % Echo time             [ms]
        Flip        = 90;    % Flip angle            [deg]
        
        %   The eigenvalue basis is defined for a
        %   M012 compensated spin-echo sequence. 
        %   (von Deuster 2016). FA = 0.38. 
        DTensor    = [2,0,0;0,1.25,0;0,0,0.9]*1e-3; 
         
        % Give a dummy tensor for Dstar (unused for SE)
        DstarTensor = [0.2,0,0;0,0.8,0;0,0,0.8]*1e-3; 
        
        % Set lesion values 
        Dlesion     = 7.25E-4;
        Dstarlesion = 0E-3;
        
    case 'steam'
        f           = 0.14; % [-] 
        fStd        = 25; % [%-of f] 
        Dstar       = 10E-3; % mm2/s
        D           = 1E-3;  
        TR          = 2000;  % 2 R-R window          [ms]
        TR_std      = 0;     % Standard deviation    [%-of TR]
        TE          = 35;    % Echo time             [ms]
        Flip        = 90;    % Flip angle (fixed)    [deg]
        
        %   The eigenvalue basis is defined for a
        %   STEAM sequence. (von Deuster 2016). 
        %   FA = 0.62. 
        DTensor     = [1.8,0,0;0,0.8,0;0,0,0.4]*1e-3; 
        DstarTensor = [0.2,0,0;0,0.8,0;0,0,0.8]*1e-3; 
        
        % Set lesion values 
        Dlesion     = 0.6E-3;
        Dstarlesion = 5E-3;
end

% --------------------------------------------------------------------
% Encoding options 
% - Flip the EPI blip direction
% - Set the readout bandwidth in Hz/pixel
% - Define the acquisition resolution and matrix size
% --------------------------------------------------------------------
ReadoutMode              = 'epi'; % Only epi is currently supported. 
PreparationDirection     = 'AP'; % 'AP', or 'LR' 
FlipEpiBlipDirection     = 'no'; % Simulate different EPI phase-encode (blip) direction  
ReadoutBandwidthPix      =  35; % [Hz/pixel] 
RowFlipping              = 'no'; % Flip Kx direction for odd-numbered phase encode lines 
Interleaves              = 1; % Currently, only a single interleaf is supported 

% --------------------------------------------------------------------
%   Adjust image resolution
%   Inspect the original *.par file for dxcat2.exe to find the original 
%   object pixel size (parameter name: pixel_width)
% --------------------------------------------------------------------
AcqRes  = [0.25, 0.25]; % Acquisition resolution [cm]
AcqResMatrix  = [121, 43]; %  Matrix size (Frequency,Phase-encode). Note: Make this a bit larger along X, for cropping the data in image space later

% --------------------------------------------------------------------
%   F0 off-resonance simulation 
%   - Generate field map with a gradient at the inferior/lateral
%     left-ventricular (LV) wall, and the LV liver interface
%   - Target gradient value for inside the LV myocardium can be set 
%   - Through-slice field gradients can be included 
% 
%   - Notes: 
%       - Through-slice simulation only active when SimulateF0 is set 
%         to 'yes'
% --------------------------------------------------------------------
SimulateF0 = 'no';
TargetGradient = 8; % Hz/pix
F0_fat = -220; % Hz (Note: 220 Hz@1.5T / 440Hz @3T) 
FatFraction = 0.1; % Range between 0 and 1

% Use analytically-defined map or load external field map 
% - *.mat file location: selected bin-file path\fieldmaps
LoadF0Maps = 'no';
FieldMapSubfolder = 'fieldmaps'; 
FieldmapPath = fullfile(FieldMapSubfolder,strcat('fieldmaps_', PreparationDirection,'.mat'));

% Add through-slice dB0 effects on top of in-plane off-resonances
Nz_ts = 1; % Number of through-slice points 
ThroughSliceGradient = 7.5; % Hz/across slice 
ThroughSliceProfile = 'quadratic'; % 'linear' or 'quadratic'

% For reconstruction purposes, set the downsampled matrix size. 
% - Set according to the AcqResMatrix parameter
F0MatrixFactor = 2.5; 
DownsampledF0Matrix = ceil(AcqResMatrix .* F0MatrixFactor);

% Set ellipse parameters of analytically-defined field map to tune the 
% in-plane off-resonance profile. 
% - Only used when LoadF0Maps = 'No';
EllipseScaleX = 1;
EllipseScaleY = 2; 
EllipseRadius = 26; 
EllipseSectorAngleRange = [165, 325]; 

% --------------------------------------------------------------------
% Set the random number generator (RNG) seed points 
% - If left empty ([]), random motion states, noise data,
%   T2star distribution or perfusion fraction f distribution is generated 
%   during each simulation
% - If a seed is entered (e.g. 0), that seed is used to define the
%   motion states or noise generation. 
% --------------------------------------------------------------------
RngSeedMotionStates = [];
RngSeedNoise = 0; 
RngSeedT2star = 0; 
RngSeedFMap = 0; 

% --------------------------------------------------------------------
% Recon options 
% - Set reconstruction resolution and matrix size 
%   (unused by MRXCAT-CDTI, for post-processing only). 
% --------------------------------------------------------------------
% Recon resolution and matrix size
ReconRes = [0.12, 0.12]; 
ReconResMatrix = [192, 192]; 

% --------------------------------------------------------------------
%   Display title
% --------------------------------------------------------------------
fprintf ( '------------------------------------------\n' );
fprintf ( '        MRXCAT-CMR-CDTI (VER %s)           \n' , MRX.Version );
fprintf ( '------------------------------------------\n' );

% --------------------------------------------------------------------
%   Open window, select file
% --------------------------------------------------------------------
if ~exist('filename','var') || ~exist(filename,'file')
    [filename,pathname] = uigetfile({'*.bin;*.par','XCAT2 files (*.par,*.bin)'});
else
    pathname = pwd;
    if ispc, pathname = [pathname,'\']; else pathname = [pathname,'/']; end
    
    [pathname, name, ext] = fileparts(filename);
    filename = strcat(name,ext);  
end

% --------------------------------------------------------------------
%   Generate XCAT2 *.bin files
% --------------------------------------------------------------------
cwd = cd; 

if strcmp(filename(end-2:end),'par')
    fprintf ('Generating XCAT2 bin files...');
    fname = 'DIFF'; exe = 'dxcat2 ';
    if ispc  exe = 'dxcat2.exe '; end
    if ismac exe = 'dxcat2mac ';  end
    x = num2str(RotationXYZ(1));
    y = num2str(RotationXYZ(2));
    z = num2str(RotationXYZ(3));
    s = [exe,filename,' --phan_rotx ',x,' --phan_roty ',y,' --phan_rotz ',z,' ',fname];
    cd(pathname); system(s); clear x y z s;
    fprintf ('ok\n');
    filename = [fname,'_act_1.bin'];
    clear x y z s fname;
end

cd(cwd); % Reset to current working directory

% --------------------------------------------------------------------
%   Read log file
% --------------------------------------------------------------------
MRX.Filename = fullfile(pathname,filename);
MRX.readLogFile;

% --------------------------------------------------------------------
%   Store tissue, contrast and sequence parameters
% --------------------------------------------------------------------
MRX.Par.tissue.rhomuscle    = RhoMuscle;
MRX.Par.tissue.rhofat       = RhoFat;
MRX.Par.tissue.rhoblood     = RhoBlood;
MRX.Par.tissue.rholiver     = RhoLiver;
MRX.Par.tissue.rhobone      = RhoBone;

MRX.Par.tissue.t1muscle     = T1Muscle;
MRX.Par.tissue.t1fat        = T1Fat;
MRX.Par.tissue.t1blood      = T1Blood;
MRX.Par.tissue.t1liver      = T1Liver;
MRX.Par.tissue.t1bone       = T1Bone;

MRX.Par.tissue.t2muscle     = T2Muscle;
MRX.Par.tissue.t2fat        = T2Fat;
MRX.Par.tissue.t2blood      = T2Blood;
MRX.Par.tissue.t2liver      = T2Liver;
MRX.Par.tissue.t2bone       = T2Bone;

MRX.Par.tissue.t2starmuscle = T2starMuscle;
MRX.Par.tissue.t2starfat    = T2starFat;
MRX.Par.tissue.t2starblood  = T2starBlood;
MRX.Par.tissue.t2starliver  = T2starLiver;
MRX.Par.tissue.t2starbone   = T2starBone;

MRX.Par.tissue.t2star_std   = T2starStd;

MRX.Par.scan.tr             = TR;
MRX.Par.scan.tr_array       = 0; % Allocate
MRX.Par.scan.tr_std         = TR_std;
MRX.Par.scan.te             = TE;
MRX.Par.scan.flip           = pi*Flip/180;

MRX.Par.scan.segments       = 1;
MRX.Par.scan.bbox           = BoundingBox;
MRX.Par.scan.snr            = SNR;
MRX.Par.scan.coils          = Coils;
MRX.Par.scan.coildist       = CoilDist;
MRX.Par.scan.coilsperrow    = CoilsPerRow;
MRX.Par.scan.rotation       = pi*RotationXYZ/180;
MRX.Par.scan.frames         = 1;
MRX.Par.scan.phases         = 1; % Only single cardiac phase is considered for DIFF module
MRX.Par.scan.trajectory     = 'Cartesian';
MRX.Par.scan.undersample    = 1;  
MRX.Par.scan.coilradius     = CoilRadius; 
MRX.Par.scan.selectedslices = SelectedSlices; 

if (RespiratoryMotion~=0) && (RespiratoryMotion~=1) && (RespiratoryMotion~=2)
    error('Unknown respiratory motion mode selected'); 
end

MRX.Par.scan.respiratorymotion = RespiratoryMotion; 
MRX.Par.scan.numberofaverages  = NumberOfAverages; 
MRX.Par.scan.offcentres     = 0; % Allocate parameter

% --------------------------------------------------------------------
% Store diffusion scan parameters 
% --------------------------------------------------------------------
MRX.Par.contrast.diff.encodingscheme             = EncodingScheme; 
MRX.Par.contrast.diff.bvalues                    = bValues;
MRX.Par.contrast.diff.helixanglerange            = HelixAngleRange;
MRX.Par.contrast.diff.sheetanglevalue            = SheetAngleValue;
MRX.Par.contrast.diff.d                          = D;
MRX.Par.contrast.diff.dstar                      = Dstar;
MRX.Par.contrast.diff.f                          = f;
MRX.Par.contrast.diff.fstd                       = fStd;
MRX.Par.contrast.diff.nrdiffusiondirections      = 0;        % Allocate parameter
MRX.Par.contrast.diff.directions                 = 0;        % Allocate parameter
MRX.Par.contrast.diff.bvaluearray                = 0;        % Allocate parameter
MRX.Par.contrast.diff.numberofbvalues            = 0;        % Allocate parameter
MRX.Par.contrast.diff.sequence                   = Sequence;
MRX.Par.contrast.diff.loadeigenvaluemaps         = LoadEigenValueMaps;
MRX.Par.contrast.diff.evfilenamed                = EVFileNameD;
MRX.Par.contrast.diff.evfilenamedstar            = EVFileNameDstar;
MRX.Par.contrast.diff.evfilenamedlesion          = EVFileNameDlesion;
MRX.Par.contrast.diff.evfilenamedstarlesion      = EVFileNameDstarlesion;
MRX.Par.contrast.diff.dtensor                    = DTensor; 
MRX.Par.contrast.diff.dstartensor                = DstarTensor;
MRX.Par.contrast.diff.dlesion                    = Dlesion;
MRX.Par.contrast.diff.dstarlesion                = Dstarlesion;
MRX.Par.contrast.diff.loadsheetanglemaps         = LoadSheetAngleMaps; 
MRX.Par.contrast.diff.e2afilename                = E2AFileName;

% --------------------------------------------------------------------
% Store lesion parameters 
% --------------------------------------------------------------------
MRX.Par.lesion.sectorangles = LesionSectorAngles;
MRX.Par.lesion.tmdranges    = LesionTMDranges;
MRX.Par.lesion.lesionvalue  = LesionValue; 
MRX.Par.lesion.enable       = EnableLesion; 

% --------------------------------------------------------------------
% Store F0 parameters 
% --------------------------------------------------------------------
MRX.Par.f0.simulatef0             = SimulateF0;
MRX.Par.f0.targetgradient         = TargetGradient; 
MRX.Par.f0.f0_fat                 = F0_fat;
MRX.Par.f0.fatfraction            = FatFraction;
MRX.Par.f0.loadf0maps             = LoadF0Maps;
MRX.Par.f0.fieldmappath           = FieldmapPath;
MRX.Par.f0.ellipsescalex          = EllipseScaleX; 
MRX.Par.f0.ellipsescaley          = EllipseScaleY; 
MRX.Par.f0.ellipseradius          = EllipseRadius; 
MRX.Par.f0.ellipsesectoranglerange = EllipseSectorAngleRange; 

warning on;
if (mod(Nz_ts,1)~=0) && (Nz_ts ~= 0)
    warning(['The number of through-slice points should be odd-numbered. Setting Nz_ts to: ', num2str(Nz_ts+1)]');
    Nz_ts = Nz_ts + 1; 
end
if strcmpi(SimulateF0,'no') && Nz_ts > 1
    warning(['Nz_ts > 1, but SimulateF0 is set to no. Resetting Nz_ts to 1.']);
    Nz_ts = 1; 
end

MRX.Par.f0.nz_ts                  = Nz_ts; 
MRX.Par.f0.throughslicegradient   = ThroughSliceGradient; 
MRX.Par.f0.throughsliceprofile    = ThroughSliceProfile;
MRX.Par.f0.downsampledf0matrix    = DownsampledF0Matrix;

% --------------------------------------------------------------------
% Store encoding parameters 
% --------------------------------------------------------------------
MRX.Par.enc.preparationdirection   = PreparationDirection; 
MRX.Par.enc.flipepiblipdirection   = FlipEpiBlipDirection;
MRX.Par.enc.readoutbandwidth       = ReadoutBandwidthPix;
MRX.Par.enc.readoutmode            = ReadoutMode; 
MRX.Par.enc.fov                    = 0;
MRX.Par.enc.nx                     = 0;
MRX.Par.enc.ny                     = 0; 
MRX.Par.enc.nz                     = 0;
MRX.Par.enc.OVS                    = 1; 
MRX.Par.enc.nx_OVS                 = 0;
MRX.Par.enc.ny_OVS                 = 0;
MRX.Par.enc.dx                     = 1; % mm
MRX.Par.enc.dy                     = 1; % mm
MRX.Par.enc.dz                     = 1; % mm
MRX.Par.enc.acqres                 = AcqRes; 

if (mod(AcqResMatrix(2),2) == 0) && (strcmpi(ReadoutMode,'epi'))
   error(['Number of phase encodes for epi is even, set to AcqResMatrix(2) to an uneven number.']);  
end

MRX.Par.enc.acqresmatrix           = AcqResMatrix;
MRX.Par.enc.samplingtimes          = 0; 
MRX.Par.enc.rowflipping            = RowFlipping;
MRX.Par.enc.no_interleaves         = Interleaves; 

% --------------------------------------------------------------------
% Store reconstruction parameters 
% --------------------------------------------------------------------
MRX.Par.recon.reconres             = ReconRes; 
MRX.Par.recon.reconresmatrix       = ReconResMatrix; 

% --------------------------------------------------------------------
% Store process parameters 
% --------------------------------------------------------------------
MRX.Par.proc.img_nr = 1; 
MRX.Par.proc.exportmasks = ExportMasks;
MRX.Par.proc.exportdata2mat = ExportData2Mat;
MRX.Par.proc.rngseedmotionstates = RngSeedMotionStates; 
MRX.Par.proc.rngseednoise = RngSeedNoise; 
MRX.Par.proc.rngseedt2star = RngSeedT2star; 
MRX.Par.proc.rngseedfmap = RngSeedFMap; 

% --------------------------------------------------------------------
%   Notifications
% --------------------------------------------------------------------
if ~isempty(RngSeedNoise) 
    disp(['Note: random number generator seed selected for noise generation']); 
end
if ~isempty(RngSeedMotionStates) 
    disp(['Note: random number generator seed selected for motion state generation']); 
end
if ~isempty(RngSeedT2star) 
    disp(['Note: random number generator seed selected for T2star distribution generation']); 
end
if ~isempty(RngSeedFMap) 
    disp(['Note: random number generator seed selected for perfusion fraction distribution generation']); 
end

% --------------------------------------------------------------------
%   Error checks
% --------------------------------------------------------------------
if mod(max(MRX.Par.scan.frames),1)>0 % check if #frames is whole number
    error('Number of frames must be an integer value. Check number of segments in CDTIpar.m and number of XCAT .bin files!')
end

end