%=========================================================================
% MRXCAT_CMR_CDTI MRXCAT class for cardiac diffusion tensor (CDTI) imaging,
% and cardiac perfusion tensor imaging. 
% 
% The MRXCAT_CMR_CDTI class contains specific CDTI MRXCAT methods,
% as well as function overwrites from the MRXCAT superclass. 
% Methods common with other classes are in the MRXCAT superclass.
% 
% WEBSITES: https://www.biomed.ee.ethz.ch/mrxcat
%           https://gitlab.ethz.ch/ibt-cmr-public/mrxcat-cdti-public
% 
% VERSION HISTORY (MRXCAT-CMR-CDTI)		
%           RVG 03 May 2021        INITIAL RELEASE -  v1.0
%
% AUTHORS:  Robbert van Gorkum, Jonathan Weine, Christian Stoeck, Sebastian Kozerke. 
%           Institute for Biomedical Engineering, University and ETH Zurich
%
%=================================================================================
classdef MRXCAT_CMR_CDTI < MRXCAT
    methods
        function MRX = MRXCAT_CMR_CDTI( binfile, varargin )
            
            MRX = MRX@MRXCAT();
            MRX.Version = '1.0'; % Set version number for MRXCAT-CDTI
            
            tic; % Timer on

            % -------------------------------------------------------------
            %   Load par file contents
            % -------------------------------------------------------------
            if ~exist('binfile','var'), binfile = ''; end
            
            if nargin < 2
                MRX.CDTIpar( binfile ); % Defaults to CDTIpar file
                MRX.Par.proc.parname = 'CDTIpar';
            else
                MRX.(varargin{:})( binfile ); % User specified
                MRX.Par.proc.parname = varargin{:};
            end
            
            % -------------------------------------------------------------
            %   Initialize MRXCAT-CMR-CDTI 
            % -------------------------------------------------------------
            MRX.setDiffusionDirections;
            MRX.updatePars;
            MRX.updateArrays;
            
            % -------------------------------------------------------------
            %   Get coil sensitivities for forward model and downsampled 
            %   coil maps for (retrospective) image reconstruction
            % ------------------------------------------------------------- 
            [sen_HR, sen_DS] = MRX.calculateCoilMaps; 
            
            % -------------------------------------------------------------
            %   Compute noise standard deviation
            % ------------------------------------------------------------- 
            MRX.computeNoiseStdDev(sen_HR);
            
            % -------------------------------------------------------------
            %   Get all XCAT masks and left-ventricular masks
            % ------------------------------------------------------------- 
            [XCATMasks, LVMasks] = MRX.getXCATMasks;
            
            % -------------------------------------------------------------
            %   Get tensors prior
            %   - Computes tensors for all motion-states and slices
            %   - Debug output can be called using 
            %     [tensors, debug_output] = MRX.getTensors(LVMasks);
            % ------------------------------------------------------------- 
            [tensors] = MRX.getTensors(LVMasks);    
            
            % -------------------------------------------------------------
            %   Get field map(s) at higher object resolution
            %   and downsampled field map for reconstruction 
            % -------------------------------------------------------------
            [fieldmaps_HR,fieldmaps_DS] = MRX.getFieldMaps(XCATMasks,LVMasks);   

            % -------------------------------------------------------------
            %   Get the perfusion fraction f masks 
            %   - These are used in mapDiffusionProps to obtain the
            %     perfusion fraction f map.
            %   - Computing them here reduces simulation time in
            %     free-breathing cases
            % -------------------------------------------------------------
            fMap_masks = MRX.getLesionMasks(LVMasks);

            % -------------------------------------------------------------
            %   Produce MRXCAT data by looping over diffusion directions 
            %   and number of averages (NSA)
            %   - nsa_list parameter enables shorter simulations times for
            %     fully static or breath-held cases (simulate data once, 
            %     then replicate along the NSA direction prior to adding noise). 
            %
            %   The following respiratory motion states are supported 
            %   - Fully static 
            %   - Breath-hold 
            %   - Free-breathing
            % -------------------------------------------------------------  
            [random_motion_indices,nsa_list] = MRX.setMotionStates; 
                 
            % ------------------------------------------------------------- 
            %   Allocate memory 
            % ------------------------------------------------------------- 
            [dwimgs,masks,kspace,fieldmaps] = MRX.allocateMemory;
            
            % ------------------------------------------------------------- 
            %   Main loop 
            % -------------------------------------------------------------  
            waitbar_counter = 0;
            
            for dir_idx = 1:MRX.Par.contrast.diff.nrdiffusiondirections 
                for nsa_idx = nsa_list  
                    % -----------------------------------------------------
                    %   Select XCAT mask 
                    % -----------------------------------------------------
                    xcat_no = random_motion_indices((dir_idx-1) * MRX.Par.scan.numberofaverages + nsa_idx);
                    data = XCATMasks(:,:,:,xcat_no); 
                    
                    % -----------------------------------------------------
                    %   Map MR tissue properties
                    % -----------------------------------------------------
                    [img,msk,T2starmap] = MRX.mapTissueProps(data); 

                    % -----------------------------------------------------
                    %   Map diffusion properties
                    % -----------------------------------------------------
                    img = MRX.mapDiffusionProps(data,img,msk,dir_idx,tensors,fMap_masks,xcat_no);   
                           
                    % -----------------------------------------------------
                    %   Multiply data with coil sensitivities
                    % ----------------------------------------------------- 
                    img = MRX.multiplyCoilMaps(img,sen_HR);

                    % -----------------------------------------------------
                    %   Encode data to k-space
                    % -----------------------------------------------------
                    [img,ksp] = MRX.encodeData(img, T2starmap, fieldmaps_HR(:,:,:,:,xcat_no)); 
                    
                    % -----------------------------------------------------
                    %   Store data
                    % -----------------------------------------------------
                    dwimgs(:,:,:,:,dir_idx,nsa_idx) = permute(img,[2 1 3 4]);   
                    masks(:,:,:,dir_idx,nsa_idx)    = imresize(permute(msk,[2 1 3 4]),[size(masks,1), size(masks,2)],'nearest');  
                    kspace(:,:,:,:,dir_idx,nsa_idx) = ksp;
                    fieldmaps(:,:,:,:,dir_idx,nsa_idx) = fieldmaps_DS(:,:,:,:,xcat_no);
                    
                    %------------------------------------------------------
                    %   Update text waitbar and processed image nr 
                    %------------------------------------------------------
                    if (MRX.Par.scan.respiratorymotion==0) || (MRX.Par.scan.respiratorymotion==1)
                        MRX.Par.proc.img_nr = MRX.Par.proc.img_nr + MRX.Par.scan.numberofaverages; 
                    else
                        MRX.Par.proc.img_nr = MRX.Par.proc.img_nr + 1; 
                    end
                    waitbar_counter = MRX.textwaitbar(waitbar_counter, 'Performing forward model simulation');
                end
            end
                       
            % -------------------------------------------------------------
            %   Replicate data along the nsa dimension for static or 
            %   breathhold cases
            % -------------------------------------------------------------
            if (MRX.Par.scan.respiratorymotion==0) || (MRX.Par.scan.respiratorymotion==1)
                dwimgs = repmat(dwimgs,[1 1 1 1 1 MRX.Par.scan.numberofaverages]);
                masks  = repmat(masks, [1 1 1 1 MRX.Par.scan.numberofaverages]);
                kspace = repmat(kspace,[1 1 1 1 1 MRX.Par.scan.numberofaverages]);
            end 
  
            % -------------------------------------------------------------
            %   Add/Get noise samples
            % -------------------------------------------------------------
            [dwimgs, noisedata] = MRX.addNoise(dwimgs); 
            [kspace, noisedata_ksp] = MRX.addNoise(kspace); 
            
            % -------------------------------------------------------------
            %   Get simulation time information 
            % -------------------------------------------------------------  
            MRX.Par.proc.simul_time = toc;  
            disp(['Total simulation time (s): ', num2str(MRX.Par.proc.simul_time)]);
            
            % -------------------------------------------------------------
            %   Save data and parameters for post-processing 
            % -------------------------------------------------------------
            MRX.saveImgData(dwimgs, msk, noisedata, sen_DS, fieldmaps);
            MRX.saveReconData( kspace, sen_DS, noisedata_ksp, fieldmaps, masks); 
        end
        
        %=========================================================================
		% Set motion states and number of averages depending on respiratory
		% motion option
        function  [random_motion_indices,nsa_list] = setMotionStates(MRX) 
            % -------------------------------------------------------------
            %   Produce MRXCAT data by looping over diffusion directions 
            %   and number of averages (NSA)
            %   - nsa_list parameter enables shorter simulations times for
            %     fully static or breath-held cases (simulate data once, 
            %     then replicate along the NSA direction prior to adding noise). 
            %
            %   The following respiratory motion states are supported 
            %   - Fully static 
            %   - Breath-hold 
            %   - Free-breathing  
            %
            %   - Input                         MRX
            % 
            %   - Outputs                       Motion state indices        
            %                                   (depending on available
            %                                   XCAT masks) 
            %                                   List of averages 
            % -------------------------------------------------------------  
            if isfield(MRX.Par.proc,'rngseedmotionstates')
                % Allows the same motion states to be used for every
                % simulation 
                if ~isempty(MRX.Par.proc.rngseedmotionstates)
                    s = rng; 
                    rng(MRX.Par.proc.rngseedmotionstates)
                end
            end
            
            nsa = MRX.Par.scan.numberofaverages;
            ndirs = MRX.Par.contrast.diff.nrdiffusiondirections;
            frames_xcat = MRX.Par.scan.frames_xcat;
            
            switch MRX.Par.scan.respiratorymotion
                case 0 % Static: Use same motion state for all images
                    random_motion_indices = ones(ndirs, nsa);
                    nsa_list = 1; 
                case 1 % Breathhold: Use new motion state per diffusion direction 
                    random_motion_indices = randi([1, frames_xcat],[ndirs, 1]);
                    random_motion_indices = repmat(random_motion_indices,[1 nsa]); 
                    nsa_list = 1; 
                case 2  % Free-breathing: New motion state for each diffusion direction and average
                    random_motion_indices = randi([1, frames_xcat],[ndirs, nsa]);
                    nsa_list = 1:nsa;  
            end 
            MRX.Par.proc.random_motion_indices = random_motion_indices; 
            MRX.Par.proc.nsa_list              = nsa_list; 
            
            if isfield(MRX.Par.proc,'rngseedmotionstates')
                % Allows the same motion states to be used for every
                % simulation 
                if ~isempty(MRX.Par.proc.rngseedmotionstates)
                    rng(s); % Reset to initial values 
                end
            end
        end
        
        %=========================================================================
		% Generate diffusion and perfusion tensors based on the XCAT
		% masks
        function [tensors, debug_output] = getTensors(MRX, masks)
            %--------------------------------------------------------------
            % Code to compute the perfusion/diffusion tensors directly from 
            % the left ventricular XCAT masks 
            %
            % - Inputs              MRX object 
            %                       LV XCAT masks 
            %
            % - Outputs (tensors)   Diffusion and perfusion tensors 
            %                       Normal, and lesion cases
            % 
            %            (debug)    Transmural depth map
            %                       Helix angle map (used as input) 
            %                       Helix angle map (from computed tensors) 
            %                       Sheet angle map 
            %                       MD(D) map 
            %                       FA(D) map
            %                       MD(Dstar) map 
            %                       FA(Dstar) map
            %                       Eigenvalue map D
            %                       Eigenvector map D
            %                       Eigenvalue map Dstar
            %                       Eigenvector map Dstar
            % 
            % Notes: 
            % - Define local perfusion tensor with eval 1 pointing in the
            %   direction of the fiber
            %--------------------------------------------------------------
            if nargout == 2
                debug = true;
            else 
                debug = false; 
            end
            
            no_xcat_frames = size(masks,4); 
            
            % -------------------------------------------------------------
            % Allocate memory
            % -------------------------------------------------------------
            size_tensors = [size(masks,1),size(masks,2),size(masks,3),3,3,no_xcat_frames]; 
            tensors = struct; 
            tensors.diffusion_tensors_healthy = zeros(size_tensors);
            tensors.perfusion_tensors_healthy = zeros(size_tensors);
            tensors.diffusion_tensors_lesion = zeros(size_tensors);
            tensors.perfusion_tensors_lesion = zeros(size_tensors);

            if debug 
                debug_output = MRX.allocateTensorDebugOutput(masks); 
            end

            % -------------------------------------------------------------
            % Define local parameters 
            % -------------------------------------------------------------
            pars = MRX.Par.contrast.diff; 
            
            % -------------------------------------------------------------
            % Initialize tensors 
            % - User-defined eigenvalue maps can be loaded in here
            % -------------------------------------------------------------
            if strcmpi(MRX.Par.contrast.diff.loadeigenvaluemaps,'no')
                % Initialize eigenvalues for diffusion and perfusion tensors
                diff_tensor = pars.dtensor;
                diff_tensor = diff_tensor/(trace(diff_tensor)/3)*pars.d;
                perf_tensor = pars.dstartensor;
                perf_tensor = perf_tensor/(trace(perf_tensor)/3)*pars.dstar;

                % Lesion tensors 
                diff_tensor_lesion = pars.dtensor;
                diff_tensor_lesion = diff_tensor_lesion/(trace(diff_tensor_lesion)/3)*pars.dlesion;
                perf_tensor_lesion = pars.dstartensor;
                perf_tensor_lesion = perf_tensor_lesion/(trace(perf_tensor_lesion)/3)*pars.dstarlesion;

            elseif strcmpi(MRX.Par.contrast.diff.loadeigenvaluemaps,'yes')

                % Load the eigenvalues of the diffusion tensor
                [pathstr,~,~] = fileparts(MRX.Filename);
                EV_filepath_D = fullfile(pathstr, 'evmaps', MRX.Par.contrast.diff.evfilenamed);
                EV_filepath_Dstar = fullfile(pathstr, 'evmaps', MRX.Par.contrast.diff.evfilenamedstar);
                EV_filepath_D_lesion = fullfile(pathstr, 'evmaps', MRX.Par.contrast.diff.evfilenamedlesion);
                EV_filepath_Dstar_lesion = fullfile(pathstr, 'evmaps', MRX.Par.contrast.diff.evfilenamedstarlesion);

                try 
                    EV_D  = load(EV_filepath_D,'ev_maps'); 
                    EV_D_lesion  = load(EV_filepath_D_lesion,'ev_maps'); 
                    EV_Dstar  = load(EV_filepath_Dstar,'ev_maps'); 
                    EV_Dstar_lesion  = load(EV_filepath_Dstar_lesion,'ev_maps'); 
                catch ME
                    throw(ME)
                end
                
                % Check in-plane eigenvalue map size 
                sz_evmaps = size(EV_D.ev_maps); 
                sz_masks = size(masks); 
                
                if ~isequal(sz_evmaps(1:2), sz_masks(1:2))
                    msg = ['Loaded eigenvalue maps and XCAT masks size are not the same!'...
                           'Check the acquisition matrix size and eigenvalue maps.'];
                   error(msg); 
                end
         
                % Convert to mm�/s
                EV_D = EV_D.ev_maps * 1E-3;
                EV_D_lesion = EV_D_lesion.ev_maps * 1E-3;
                EV_Dstar = EV_Dstar.ev_maps * 1E-3;
                EV_Dstar_lesion = EV_Dstar_lesion.ev_maps * 1E-3;

                % Stored order of eigenvalues is from low to high
                % Adjust order for forward model for diffusion tensors
                EV_D = EV_D(:,:,:,:,[3 2 1]); 
                EV_D_lesion = EV_D_lesion(:,:,:,:,[3 2 1]);
                
                % Allocate for indexing later
                diff_tensor = zeros(3,3); 
                perf_tensor = zeros(3,3); 
                diff_tensor_lesion = zeros(3,3); 
                perf_tensor_lesion = zeros(3,3); 
            end
            
            % -------------------------------------------------------------
            % Define sheet angle  
            % - User-defined sheet angle maps can be loaded in here
            % -------------------------------------------------------------
            if strcmpi(MRX.Par.contrast.diff.loadsheetanglemaps,'no')
                sheet_angle_maps = masks; 
                local_sheet_angle = pars.sheetanglevalue * pi / 180; 
                sheet_angle_maps = sheet_angle_maps .* local_sheet_angle;
            elseif strcmpi(MRX.Par.contrast.diff.loadsheetanglemaps,'yes')

                [pathstr,~,~] = fileparts(MRX.Filename);
                E2Amaps_filepath = fullfile(pathstr, 'evmaps', MRX.Par.contrast.diff.e2afilename);
                
                try 
                    sheet_angle_maps  = load(E2Amaps_filepath,'e2a_maps'); 
                catch ME
                    throw(ME)
                end        
                sheet_angle_maps = sheet_angle_maps.e2a_maps * pi / 180; 
            end
            
            % -------------------------------------------------------------
            % Loop over all slices and masks to generate 
            % elevation and azimuthal angle maps 
            % -------------------------------------------------------------
            for xcat_idx  = 1:no_xcat_frames
                for slice_idx = 1:size(masks,3)
                    % Compute the edges of the image plane using the mask
                    [~,L] = bwboundaries(masks(:,:,slice_idx, xcat_idx));
                    values = unique(L(:));
                    values(values==0)=[];
                    edge_epi = edge(L>=values(1));
                    edge_endo = edge(L>=values(2));

                    % Calculate Center of gravity of endocardium edge
                    endo_filled = imfill(edge_endo,'holes');
                    stat = regionprops(endo_filled,'centroid');
                    LV_endo_center = stat.Centroid;
                    LV_endo_center = floor(LV_endo_center); 

                    % Determine the shift towards the center points
                    % of the LV endocardial mask for the cart/polar
                    % coordinate systems.
                    mat_size = size(masks); 
                    shift_AP = LV_endo_center(2) - (mat_size(1)/2); 
                    shift_LR = LV_endo_center(1) - (mat_size(2)/2);

                    % Initialize normal vector.
                    normal_vector = [0;0;1];
                    
                    % Define cartesian grid and polar coordinates 
                    [Y,X] = meshgrid(1:1:(size(masks,2)),1:1:(size(masks,1)));
                    Y = Y - (size(masks,2)/2) - shift_LR;
                    X = X - (size(masks,1)/2) - shift_AP; 
                    [thetaIm,rhoIm] = cart2pol(X , Y );

                    % Loop over the mask, and setup each 
                    % diffusion/perfusion tensors based on the
                    % transmural depth. 
                    for ind_i = 1:mat_size(1)
                        for ind_j = 1:mat_size(2)
                            if masks(ind_i,ind_j,slice_idx,xcat_idx)

                                rad_pos = sqrt((ind_i - (mat_size(1)/2) - shift_AP)^2+(ind_j - (mat_size(2)/2) - shift_LR)^2);

                                % Find local polar coordinates    
                                [theta_pos, ~] = cart2pol((ind_i - (mat_size(1)/2) - shift_AP),(ind_j - (mat_size(2)/2) - shift_LR));

                                % Determine local transmural depth 
                                tmp_endo = (abs(thetaIm - theta_pos).*edge_endo);
                                tmp_endo(tmp_endo == 0) = inf; % Set to INF so MIN can still be called for linear indexing
                                tmp_epi = (abs(thetaIm - theta_pos).*edge_epi);
                                tmp_epi(tmp_epi == 0) = inf; % Set to INF so MIN can still be called for linear indexing

                                % Find smallest value, then find idx
                                % belonging to smallest value, then
                                % call the corresponding endo/epi radius
                                [~, idxEndo] = min(tmp_endo(:)); 
                                [~, idxEpi] = min(tmp_epi(:)); 
                                r_endo = rhoIm(idxEndo);
                                r_epi = rhoIm(idxEpi);

                                % Threshold radial positions
                                % exceeding the minimum
                                % endocardial or the 
                                % maximum epicardial 
                                % borders 
                                if rad_pos < r_endo 
                                    rad_pos = r_endo; 
                                end 
                                if rad_pos > r_epi 
                                    rad_pos = r_epi; 
                                end 

                                if((rad_pos >= r_endo) && (rad_pos <= r_epi))

                                    local_tmd = (rad_pos - r_endo) ./ (r_epi - r_endo);

                                    % Define local radial and
                                    % circumferential vectors 
                                    local_rad_vector = [ind_i - mat_size(1)/2 - shift_AP; ind_j - mat_size(2)/2 - shift_LR; 0] / norm([ind_i - mat_size(1)/2 - shift_AP; ind_j - mat_size(2)/2 - shift_LR; 0], 2);
                                    local_circ_vector = cross(normal_vector, local_rad_vector);

                                    % Compute helix angle depending
                                    % on local transmural depth 
                                    local_helix_angle = -1*(pars.helixanglerange / (r_epi - r_endo) * (rad_pos - r_endo) - pars.helixanglerange / 2);
                                    local_helix_angle = local_helix_angle / 180 * pi;

                                    % Set helix angle of the
                                    % eigenvectors
                                    vec_1 = cos(local_helix_angle) * local_circ_vector + sin(local_helix_angle) * normal_vector;
                                    vec_2 = -sin(local_helix_angle) * local_circ_vector + cos(local_helix_angle) * normal_vector; 
                                    vec_3 = local_rad_vector; 

                                    vec_2_orig = vec_2; % Local long axis
                                    vec_3_orig = vec_3; % Local rad axis

                                    % Set E2 sheetlet angle                                             
                                    vec_2 = -sin(sheet_angle_maps(ind_i,ind_j,slice_idx,xcat_idx)) * vec_3_orig + cos(sheet_angle_maps(ind_i,ind_j,slice_idx,xcat_idx)) * vec_2_orig;
                                    vec_3 = cos(sheet_angle_maps(ind_i,ind_j,slice_idx,xcat_idx)) * vec_3_orig + sin(sheet_angle_maps(ind_i,ind_j,slice_idx,xcat_idx)) * vec_2_orig;

                                    % Normalize vectors 
                                    vec_1 = vec_1 / norm(vec_1,1); 
                                    vec_2 = vec_2 / norm(vec_2,2); 
                                    vec_3 = vec_3 / norm(vec_3,2); 

                                    local_transform = [vec_1, vec_2, vec_3];

                                    if strcmpi(MRX.Par.contrast.diff.loadeigenvaluemaps,'yes')
                                        % Initialize the local tensor
                                        diff_tensor(eye(3)==1) = EV_D(ind_i,ind_j,slice_idx,xcat_idx,:);
                                        perf_tensor(eye(3)==1) = EV_Dstar(ind_i,ind_j,slice_idx,xcat_idx,:);
                                        diff_tensor_lesion(eye(3)==1) = EV_D_lesion(ind_i,ind_j,slice_idx,xcat_idx,:);
                                        perf_tensor_lesion(eye(3)==1) = EV_Dstar_lesion(ind_i,ind_j,slice_idx,xcat_idx,:);
                                    end
                                    
                                    local_diff_tensor = local_transform * diff_tensor / local_transform;
                                    local_perf_tensor = local_transform * perf_tensor / local_transform;
                                    local_diff_tensor_lesion = local_transform * diff_tensor_lesion / local_transform;
                                    local_perf_tensor_lesion = local_transform * perf_tensor_lesion / local_transform;

                                    tensors.diffusion_tensors_healthy(ind_i,ind_j,slice_idx,:,:,xcat_idx) = local_diff_tensor;
                                    tensors.perfusion_tensors_healthy(ind_i,ind_j,slice_idx,:,:,xcat_idx) = local_perf_tensor;
                                    tensors.diffusion_tensors_lesion(ind_i,ind_j,slice_idx,:,:,xcat_idx) = local_diff_tensor_lesion;
                                    tensors.perfusion_tensors_lesion(ind_i,ind_j,slice_idx,:,:,xcat_idx) = local_perf_tensor_lesion;

                                    %--------------------------------------------------
                                    % Verify the output of the tensors, by computing the
                                    % diffusion tensor, perfusion tensor, corresponding
                                    % eigenvectors, and the corresponding projections.
                                    % - Only use when necessary...
                                    %--------------------------------------------------
                                    if debug
                                        local_pars = struct; 
                                        local_pars.ind_i = ind_i;
                                        local_pars.ind_j = ind_j;
                                        local_pars.slice_idx = slice_idx;
                                        local_pars.xcat_idx = xcat_idx; 
                                        local_pars.local_helix_angle = local_helix_angle; 
                                        local_pars.local_tmd = local_tmd; 
                                        local_pars.normal_vector = normal_vector; 

                                        local_vectors = struct; 
                                        local_vectors.normal_vector = normal_vector; 
                                        local_vectors.local_rad_vector = local_rad_vector; 
                                        local_vectors.local_circ_vector = local_circ_vector; 

                                        debug_output = MRX.updateTensorDebugStruct(debug_output, local_diff_tensor, local_perf_tensor, local_vectors, local_pars);
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
        
        %=========================================================================
		% Update the tensor debug struct 
        function output = updateTensorDebugStruct(MRX, output, local_diff_tensor, local_perf_tensor, local_vectors, local_pars)
            % Input:    MRX                         MRXCAT object
            %           output                      output struct
            %           local_diff_tensor           Local diffusion tensor 
            %           local_perf_tensor           Local perfusion tensor
            %           local_vectors               Struct with local 
            %                                       normal/circ/rad vectors 
            %           local_pars                  Struct with parameters
            % 
            % Output:   output                      Updated struct
            %
            
            % -------------------------------------------------------------
            % Initialize the local parameters 
            % -------------------------------------------------------------
            ind_i = local_pars.ind_i;
            ind_j = local_pars.ind_j; 
            local_helix_angle = local_pars.local_helix_angle; 
            local_tmd = local_pars.local_tmd; 
            normal_vector = local_vectors.normal_vector; 
            local_circ_vector = local_vectors.local_circ_vector; 
            local_rad_vector = local_vectors.local_rad_vector; 
            xcat_idx = local_pars.xcat_idx; 
            slice_idx = local_pars.slice_idx;
            
            MDDstar = MRX.Par.contrast.diff.dstar;
           
            % -------------------------------------------------------------
            % Fill output struct 
            % -------------------------------------------------------------
            output.helix_map_pre(ind_i,ind_j,slice_idx,xcat_idx) = local_helix_angle;
            output.transmural_depth_map(ind_i, ind_j, slice_idx,xcat_idx) = local_tmd;

            [D_vec,D_val] = eig(local_diff_tensor);

            % Check for positive definite tensor
            flag = 0;
            for i = 1:rank(local_diff_tensor)
              if D_val(i,i) <= 0 
              flag = 1;
              end
            end
            if flag == 1
              warning on; 
              warning('Diffusion tensor is not positive definite');
            end

            % Check if trace of eigenvalues is 1,
            % this implies the eigenvalues are setup
            % incorrectly 
            if trace(local_diff_tensor) == 1
               error('Local diffusion tensor is identity matrix.');  
            end

            [~,IX] = sort(diag(D_val),'descend');
            D_vec = D_vec(:,IX);
            D_val = diag(D_val(IX,IX));

            output.EigVal_map_D(ind_i,ind_j,slice_idx,:,xcat_idx) = D_val;
            output.EigVec_map_D(ind_i,ind_j,slice_idx,:,:,xcat_idx) = D_vec;

            % Compute the local helix
            % angle 
            signum =  sign(local_circ_vector'*D_vec(:,1)) * sign(normal_vector'*D_vec(:,1));
            angle_post = asin(norm(cross(local_circ_vector,D_vec(:,1)),2))*signum/pi*180;
            output.helix_map_post(ind_i,ind_j,slice_idx) = angle_post;

            % Compute the E2 sheet angle
            eig_1 = D_vec(:,1);
            eig_2 = D_vec(:,2);
            eig_1_epi_plane = eig_1 - dot(eig_1,local_rad_vector)*local_rad_vector;
            fibre_direction_proj = eig_1_epi_plane/norm(eig_1_epi_plane,2);
            cross_fiber_dir = cross(fibre_direction_proj, local_rad_vector);
            cross_fiber_dir = cross_fiber_dir / norm(cross_fiber_dir, 2);
            proj_e2 = eig_2 - fibre_direction_proj * dot(fibre_direction_proj, eig_2);
            proj_e2 = proj_e2 / norm(proj_e2, 2);

            try 
                sheet_angle = vrrotvec(cross_fiber_dir, proj_e2);
            catch 
                error('E2 Angle cannot be computed. Assess the local diffusion tensor.');
            end
            rot_vec = sheet_angle(1:3)';
            sheet_angle = sign(dot(rot_vec, fibre_direction_proj)) * sheet_angle(4) * 180 / pi;

            if(sheet_angle < -90)
                sheet_angle = sheet_angle + 180;
            elseif(sheet_angle > 90)
                sheet_angle = sheet_angle - 180;
            end

            % Store output 
            output.sheet_map(ind_i,ind_j,slice_idx,xcat_idx)  = sheet_angle;
            output.MD_D_map(ind_i, ind_j,slice_idx,xcat_idx)  = trace(local_diff_tensor)/3;
            output.FA_D_map(ind_i,ind_j,slice_idx,xcat_idx)   = sqrt(3/2)* ...
                                                      sqrt( ( (D_val(1)-mean(D_val))^2 +...
                                                      (D_val(2)-mean(D_val))^2 +...
                                                      (D_val(3)-mean(D_val))^2  ...
                                                       ) / ...
                                                      (D_val(1)^2+D_val(2)^2+D_val(3)^2));
            %----------------------------------------------
            % Verify the output of the perfusion tensors
            %----------------------------------------------
            if MDDstar > 0 

                [Dstar_vec,Dstar_val] = eig(local_perf_tensor);

                % Check for positive definite tensor
                flag = 0;
                for i = 1:rank(local_diff_tensor)
                  if Dstar_val(i,i) <= 0 
                  flag = 1;
                  end
                end
                if flag == 1
                  warning on; 
                  warning('Perfusion tensor is not positive definite');
                end

                [~,IX] = sort(diag(Dstar_val),'descend');
                Dstar_vec = Dstar_vec(:,IX);
                Dstar_val = diag(Dstar_val(IX,IX));

                output.EigVal_map_Dstar(ind_i,ind_j,slice_idx,:,xcat_idx) = Dstar_val;
                output.EigVec_map_Dstar(ind_i,ind_j,slice_idx,:,:,xcat_idx) = Dstar_vec;

                output.MD_Dstar_map(ind_i, ind_j,slice_idx,xcat_idx)  = trace(local_perf_tensor)/3;
                output.FA_Dstar_map(ind_i,ind_j,slice_idx,xcat_idx)   = sqrt(3/2)* ...
                                                              sqrt( ( (Dstar_val(1)-mean(Dstar_val))^2 +...
                                                              (Dstar_val(2)-mean(Dstar_val))^2 +...
                                                              (Dstar_val(3)-mean(Dstar_val))^2  ...
                                                               ) / ...
                                                              (Dstar_val(1)^2+Dstar_val(2)^2+Dstar_val(3)^2));
            end
        end
        
        %=========================================================================
		% Downsample field map to user-defined resolution 
        function FieldMapDS = downsampleFieldMap(MRX, FieldMap, varargin)
            % Input:    MRX                         MRXCAT object
            %           Fieldmap                    Field map [rad/s]
            %           varargin                    Downsampling factors
            % 
            % Output:   FieldMapDS                  Downsampled field map
            %                                       [rad/s]

            % Define echo-time(s) in [ms] for downsampling
            % 
            % Note: 
            % To avoid issues with wrapping, these echo-times are
            % chosen for convenience. 
            % 
            % In reality, Field maps are acquired with the
            % following dTEs for dual-GE sequences 
            % - 2.25 ms @3T, 4.5 ms @ 1.5T. 
            % 
            te_1 = 1E-4; 
            dte = 1.1E-4; 
            te_2 = te_1 + dte; 
                        
            if nargin == 3
                DownsamplingFactors = varargin{1};
                DownsampledF0Matrix = size(FieldMap) ./ DownsamplingFactors; 
                FieldMapDS = zeros(DownsampledF0Matrix(1), DownsampledF0Matrix(2),size(FieldMap,3),size(FieldMap,4),size(FieldMap,5)); 
            else
                DownsamplingFactors = [MRX.Par.enc.nx_OVS, MRX.Par.enc.ny_OVS] ./ MRX.Par.f0.downsampledf0matrix;
                FieldMapDS = zeros(MRX.Par.f0.downsampledf0matrix(1), MRX.Par.f0.downsampledf0matrix(2),size(FieldMap,3),size(FieldMap,4),size(FieldMap,5)); 
            end
            
            switch lower(MRX.Par.enc.preparationdirection)
                case 'lr'
                    DownsamplingFactors = fliplr(DownsamplingFactors); 
                    FieldMapDS = permute(FieldMapDS,[2 1 3 4 5]); 
            end
            
            for xcat_idx = 1:size(FieldMap,5)
                for dz_idx = 1:size(FieldMap,4)
                    for slice_idx = 1:size(FieldMap,3)
                        % Decompose field map into two phase images [rad/s]
                        omega = FieldMap(:,:,slice_idx,dz_idx,xcat_idx); % [Rad/s]
                        phi_1 = omega .* te_1; 
                        phi_2 = omega .* te_2; 

                        % Define complex valued phase images
                        cpx_phi_1 = exp(1i*phi_1); 
                        cpx_phi_2 = exp(1i*phi_2); 

                        % Downsample data for forward model simulation
                        % using sinc-kernel
                        cpx_phi_1_ds_forw = MRX.sincDownsample(cpx_phi_1, [DownsamplingFactors(1), DownsamplingFactors(2)],[1 2]); 
                        cpx_phi_2_ds_forw = MRX.sincDownsample(cpx_phi_2, [DownsamplingFactors(1), DownsamplingFactors(2)],[1 2]); 
                        FieldMapDS(:,:,slice_idx,dz_idx,xcat_idx) = (unwrap(angle(cpx_phi_2_ds_forw) - unwrap(angle(cpx_phi_1_ds_forw)) )) ./ dte; % Keep in rad/s
                    end
                end
            end
        end
        
        %=========================================================================
		% Save data for MRXCAT_CMR_CDTI
        function saveImgData( MRX, img, msk, nois, sen, fieldmap)
            % --------------------------------------------------------------------
            %   Generate filename and append extensions
            % --------------------------------------------------------------------
            fname = MRX.generateFilename( img );
            fimg = [fname,'.cpx'];
            fmsk = [fname,'.msk'];
            fnoi = [fname,'.noi'];
            fsen = [fname,'.sen'];
            ffmap = [fname,'.fmap'];
            ftxt = [fname,'.txt'];  % save MRXCAT parameters to text file
                                    
            % --------------------------------------------------------------------
            %   Write/append data file
            % --------------------------------------------------------------------
            if MRX.Par.proc.img_nr == 1
                fprintf('\nOutput file information:\n');
                fprintf('  matrix :%4d x%4d x%4d\n',size(img,1),size(img,2),size(img,3));
                fprintf('  frames :%4d\n',MRX.Par.scan.frames);
                fprintf('  slices :%4d\n',MRX.Par.scan.slices);

                fidimg = fopen(fimg,'w');
                fidmsk = fopen(fmsk,'w');
                fidnoi = fopen(fnoi,'w');
                fidsen = fopen(fsen,'w');
                fidfmap = fopen(ffmap,'w');

                % write parameters to text file
                fidtxt = fopen(ftxt,'w');

                parf = which(MRX.Par.proc.parname);  
                fidpar = fopen(parf,'r');

                parContent = textscan(fidpar,'%s','delimiter','\n','whitespace','');
                endPars    = strfind(parContent{1}, 'Display title');
                endPars    = find(~cellfun('isempty', endPars), 1);
                fprintf(fidtxt, ['%% Content of parameter file (', MRX.Par.proc.parname ,'.m)'], ' \n\n' );

                for k=3:endPars-2
                    % % copy par file directly
                    % fprintf(fidtxt,'%s\n',parContent{1}{k});

                    % % copy par file, but update parameters with actual MRXCAT object values
                    percentIdx = strfind( parContent{1}{k},'%');
                    if ~isempty( percentIdx )
                        if percentIdx(1) == 1  % commentary line
                            fprintf( fidtxt, '%s\n', parContent{1}{k} );
                        elseif percentIdx(1)>1 % value line
                            strw   = MRX.getFieldName( parContent{1}{k} );
                            if ~isempty( strw )
                                fprintf( fidtxt, '%s\n', strw );
                            else
                                fprintf( fidtxt, '%s\n', parContent{1}{k} );
                            end
                        end
                    else % other line (e.g. empty line)
                        fprintf( fidtxt, '%s\n', parContent{1}{k} );
                    end
                end
                fclose(fidpar);
                fclose(fidtxt);
            else
                fidimg = fopen(fimg,'a');
                fidmsk = fopen(fmsk,'a');
                fidnoi = fopen(fnoi,'a');
                fidsen = fopen(fsen,'a'); 
                fidfmap = fopen(ffmap,'a'); 
            end
            
            if ( fidimg ~= -1 )
                dim = size(img);
                
                if length(dim) < 3, dim(3) = 1; end
                if length(dim) < 5, dim(5) = 1; end
                if length(dim) < 6, dim(6) = 1; end
                
                img  = [real(img),imag(img)];
                img  = reshape(img,dim(1),dim(2),2,dim(3),MRX.Par.scan.coils,dim(5),dim(6));
                img  = permute(img,[3 1 2 4 5 6 7]);
                nois = [real(nois),imag(nois)];
                nois = reshape(nois,dim(1),dim(2),2,dim(3),MRX.Par.scan.coils,dim(5),dim(6));
                nois = permute(nois,[3 1 2 4 5 6 7]);
                sen  = [real(sen),imag(sen)];
                sen  = reshape(sen,dim(1),dim(2),2,dim(3),MRX.Par.scan.coils);
                sen  = permute(sen,[3 1 2 4 5]);
                
                dim = size(fieldmap);
                if length(dim) < 3, dim(3) = 1; end
                if length(dim) < 5, dim(5) = 1; end
                if length(dim) < 6, dim(6) = 1; end
                
                fieldmap = reshape(fieldmap,dim(1),dim(2),1,dim(3),1,dim(5),dim(6));
                fieldmap  = permute(fieldmap,[3 1 2 4 5 6 7]);
                
                fwrite(fidimg,single(img),'single');
                fwrite(fidmsk,uint8(msk),'uint8');             
                fwrite(fidnoi,single(nois),'single');
                fwrite(fidsen,single(sen),'single');
                fwrite(fidfmap,single(fieldmap),'single');
                
                fclose(fidimg);
                fclose(fidmsk);
                fclose(fidnoi);
                fclose(fidsen);
                fclose(fidfmap);
            else
                fprintf([fimg,' cannot be written\n']);
            end
        end
        
        %=========================================================================
		% Save MRXCAT data for recon as mat files
        function saveReconData( MRX, ksp, sen, noi, fieldmaps, msk)
            
            fname   = MRX.generateFilename( ksp );
            
            % Save pars as default
            Par = MRX.Par;
            fpar = [fname,'_par.mat'];
            save( fpar, 'Par');

            if strcmpi(MRX.Par.proc.exportdata2mat,'yes')

                fksp = [fname,'_ksp.mat'];
                save( fksp, 'ksp');

                fsen = [fname,'_sen.mat'];
                save( fsen, 'sen');

                fsen = [fname,'_noi.mat'];
                save( fsen, 'noi');

                ffieldmaps = [fname,'_fieldmaps.mat'];
                save( ffieldmaps, 'fieldmaps');

                fmsk = [fname,'_masks_for_recon.mat'];
                save( fmsk, 'msk');
            end
        end
                        
        %=========================================================================
		% Get field map(s) for simulating off-resonance effects
        function [Fieldmaps,Fieldmaps_DS] = getFieldMaps( MRX, XCATMasks, LVMasks, varargin ) 
            % Input:    MRX                         MRXCAT object
            %           varargin                    debug_flag
            % 
            % Output:   Fieldmaps                    Field map [rad/s]
            % 
            %           Fieldmaps_DS                 Field map [rad/s] 
            %                                        (downsampled)
            
            % Parse user input 
            p = inputParser;
            addOptional(p,'debug_flag','false');
            parse(p,varargin{:});  
            debug_flag = p.Results.debug_flag;
                
            % Get MRXCAT parameters 
            EncPars  = MRX.Par.enc; 
            ScanPars = MRX.Par.scan; 
            act = cell2mat(struct2cell(MRX.Par.act));
            tis = fieldnames(MRX.Par.act);      
            
            % Define the matrix size for the preparation direction 
            no_slices      = size(XCATMasks,3); 
            no_xcat_frames = size(XCATMasks,4); 
            
            FieldMap_OVS_matrix = [EncPars.nx_OVS, EncPars.ny_OVS, no_slices, MRX.Par.f0.nz_ts, no_xcat_frames];
            FieldMap_OVS_pixel_size = [EncPars.dx,EncPars.dy]; 
            
            switch lower(EncPars.preparationdirection)
                case 'ap'
                    FieldMap_OVS_matrix(1:2) = flip(FieldMap_OVS_matrix(1:2),2);
                    FieldMap_OVS_pixel_size(1:2) = flip(FieldMap_OVS_pixel_size(1:2),2); 
            end
            
            % Allocate memory 
            Fieldmaps = zeros(FieldMap_OVS_matrix);    
                     
            switch lower(MRX.Par.f0.simulatef0)
                case 'yes'
                    img_size = size(XCATMasks); 
                    
                    if length(img_size) < 3
                        img_size(3) = 1; 
                    end                               
                    for xcat_idx = 1:no_xcat_frames
                        % ----------------------------------------------------------------
                        % Load field map from matfile 
                        % ----------------------------------------------------------------
                        if strcmpi(MRX.Par.f0.loadf0maps,'yes')    
                            try 
                                fieldmap_path = MRX.Par.f0.fieldmappath; 
                                [binfile_path,~,~] = fileparts(MRX.Filename);
                                tmp = load(fullfile(binfile_path, fieldmap_path)); 
                                F0maps = tmp.fieldmaps;
                            catch ME
                                throw(ME); 
                            end 

                            % Perform a size check 
                            f0map_size_loaded = size(F0maps); 
                            if ndims(F0maps) == 2 
                                f0map_size_loaded(3) = 1; 
                            end 
                            
                            size_ratios = f0map_size_loaded ./ FieldMap_OVS_matrix(1:3); 
                            if sum(size_ratios) ~= 3
                               error('Size of loaded field map does not match forward model data size!') 
                            end
                        else
                            % ------------------------------------------------------------
                            % Generate field map analytically 
                            % ------------------------------------------------------------
                            F0maps = zeros(size(XCATMasks,1),size(XCATMasks,2),size(XCATMasks,3));
                            
                            if MRX.Par.f0.targetgradient ~=0
                                for slice_idx = 1:ScanPars.slices
                                    
                                    current_XCATmask = XCATMasks(:,:,slice_idx,xcat_idx); 
                                    current_LVMask   = LVMasks(:,:,slice_idx,xcat_idx); 
                                    
                                    LVMask_filled = imfill(current_LVMask,'holes');
                                                                    
                                    % Define the transmural depth and sector angle
                                    % map for the current slices  
                                    [transmural_depth_map_LV_filled,sector_angle_map,LV_endo_center] = MRX.getLocalAngleAndDepthMaps(LVMask_filled);
                                    [transmural_depth_map_LV] = MRX.getLocalAngleAndDepthMaps(current_LVMask,LV_endo_center);
                                    
                                    %----------------------------------------------
                                    % Define the posterior vein effect as
                                    % an ellipse
                                    %----------------------------------------------
                                    sizeX = size(LVMask_filled,2); 
                                    sizeY = size(LVMask_filled,1); 

                                    centerX = fix(sizeX/2)+1;
                                    centerY = fix(sizeY/2)+1;
                                    img_center = [centerX,centerY]; 
                                    
                                    sector_angle = mean(MRX.Par.f0.ellipsesectoranglerange); 
                                    sector_angle_map_local = single(sector_angle_map); 
                                    sector_angle_map_local(sector_angle_map < sector_angle-1) = 0; 
                                    sector_angle_map_local(sector_angle_map > sector_angle+1) = 0; 

                                    tmd_sector_angle = transmural_depth_map_LV_filled .* logical(sector_angle_map_local); 
                                    [~,Imax] = max(tmd_sector_angle(:)); 
                                    [y_max, x_max] = ind2sub([size(LVMask_filled,1),size(LVMask_filled,2)],Imax); 

                                    % Assess displacement to center the
                                    % ellipse coordinates at the position
                                    % of the vein 
                                    vein_origin = [x_max, y_max];
                                    disp_vein_origin = vein_origin - img_center;

                                    % Define the ellipse grid parameters 
                                    [Y, X] = meshgrid(-fix(sizeX/2):ceil(sizeX/2)-1,-fix(sizeY/2):ceil(sizeY/2)-1); 
                                    coords = [Y(:) - disp_vein_origin(1), X(:) - disp_vein_origin(2)];
                                    
                                    % Define rotation angle to align the
                                    % ellipse in the direction of the
                                    % centerpoint of the mask
                                    theta = sector_angle - 180; % (+) = CCW; (-) = CW

                                    R = [cosd(theta) -sind(theta); sind(theta) cosd(theta)];
                                    rotXY=coords*R'; 
                                    Y = reshape(rotXY(:,1), size(X,1), []);
                                    X = reshape(rotXY(:,2), size(Y,1), []);

                                    % Define the global ellipse grid coordinates
                                    ellipse_x = MRX.Par.f0.ellipsescalex; 
                                    ellipse_y = MRX.Par.f0.ellipsescaley; 
                                    shape = ( (((X).^2) ./ ellipse_x)  + (((Y).^2) ./ ellipse_y));

                                    % Define the ellipse mask  
                                    radius = MRX.Par.f0.ellipseradius * sqrt( (1/ellipse_x).^2 + (1/ellipse_y).^2); 
                                    ellipseMask = (((X).^2) ./ (ellipse_x)) + (((Y  ).^2) ./ (ellipse_y)) <= radius.^2;

                                    local_vein_shape = shape .*ellipseMask;
                                    local_vein_mask = logical(local_vein_shape); 
                                    local_vein_shape(local_vein_mask) = local_vein_shape(local_vein_mask) - ( max(local_vein_shape(:)) * 0.75); 

                                    vein_effect_init = double(LVMask_filled); 
                                    vein_effect_init(vein_effect_init==1) = max(local_vein_shape(:)); 
                                    vein_effect_init(local_vein_mask) = local_vein_shape(local_vein_mask); 
                                    vein_effect_init = vein_effect_init.*LVMask_filled; 
                                    
                                    local_vein_mask = local_vein_mask .* LVMask_filled; 
                                   
                                    transmural_depth_map_grad = transmural_depth_map_LV;  
                                    transmural_depth_map_grad(transmural_depth_map_grad > 0.9) = 0; 
                                    
                                    LV_ROI = logical(transmural_depth_map_grad) .* local_vein_mask;                                     
                                    LV_ROI = logical(LV_ROI);
                                    
                                    % - Fx = gradient in horizontal
                                    %        direction
                                    % - Fy = gradient in vertical direction
                                    [Fx,Fy] = gradient(vein_effect_init,FieldMap_OVS_pixel_size(1),FieldMap_OVS_pixel_size(2)); 
                                   
                                    switch lower(EncPars.preparationdirection)
                                        case 'ap'
                                            preparation_direction_gradient_map = Fy; 
                                        case 'lr'
                                            preparation_direction_gradient_map = Fx; 
                                    end
                                   
                                    roi_gradient_data = preparation_direction_gradient_map(LV_ROI);

                                    % Set the maximum gradient in the vein ROI 
                                    % to the target gradient strength.
                                    scale_factor = quantile(abs(roi_gradient_data),0.99)/ abs(MRX.Par.f0.targetgradient); 
                                    vein_effect = vein_effect_init ./ scale_factor;  
                                                                        
                                    %----------------------------------------------
                                    % Prepare the background of the field map 
                                    %----------------------------------------------
                                    f0map_background = LVMask_filled; 
                                    dF0_fat = MRX.Par.f0.f0_fat * MRX.Par.f0.fatfraction; 

                                    % Apply offset to get an "in vivo" like
                                    % ramp from - to + from the posterior
                                    % vein to the anterior wall. 
                                    offset = max(vein_effect(:)); 

                                    f0map_background(f0map_background==1) = 0; 
                                    
                                    for i=1:length(act)
                                        switch char(tis(i))
                                            case {'body_activity', 'muscle_activity','rib_activity','cortical_bone_activity','bone_marrow_activity','cartilage_activity','spine_activity'}
                                                f0map_background(current_XCATmask==act(i)) = offset; 

                                            case {'liver_activity'}
                                                f0map_background(current_XCATmask==act(i)) = dF0_fat + offset; 
                                        end
                                    end
                            
                                    %----------------------------------------------
                                    % Dilate the LV contour to create a
                                    % gap between the LV and lung tissue. 
                                    % - Otherwise an unnatural gradient can
                                    %   occur in apical slices. 
                                    %----------------------------------------------
                                    LV_pericard_mask_dilated = LVMask_filled;
                                    
                                    for i=1:length(act)
                                        switch char(tis(i))
                                            case {'myoRV_act','pericardium_activity'}
                                                LV_pericard_mask_dilated(current_XCATmask==act(i)) = 1; 
                                        end
                                    end
                                    LV_pericard_mask_dilated = imfill(LV_pericard_mask_dilated,'holes');
                                    SE = strel('disk',25); 

                                    LV_pericard_mask_dilated = imdilate(LV_pericard_mask_dilated,SE);
                                    f0map_background(LV_pericard_mask_dilated==1) = 0; 
                                    
                                    % Smooth the background of the field map 
                                    mask = logical(f0map_background); 
                                    f0map_background(f0map_background==0) = NaN; 
                                    f0map_background = smoothn(f0map_background,1e3);
                                    f0map_background = f0map_background .* mask; 
 
                                    %----------------------------------------------
                                    % Fill in the LV/RV area, smooth and
                                    % extrapolate 
                                    %----------------------------------------------
                                    f0map_raw = f0map_background;
                                    f0map_raw(LVMask_filled==1) = vein_effect(LVMask_filled==1);
                                    
                                    % Dilate the stomatch wall mask to
                                    % remove any body activity signal
                                    % between stomach and liver signals 
                                    stomach_mask = zeros(size(current_XCATmask)); 
                                    stomach_mask(current_XCATmask==20) = 1; 
                                    SE = strel('disk',14); 
                                    stomach_mask_dilated = imdilate(stomach_mask,SE);
                                    stomach_mask_dilated(current_XCATmask==20);
                                    
                                    for i=1:length(act)
                                        switch char(tis(i))
                                            case {'body_activity','myoLV_act','pericardium_activity'}
                                                stomach_mask_dilated(current_XCATmask==act(i)) = 0; 
                                        end
                                    end
                                    
                                    SE = strel('disk',10); 
                                    stomach_mask_dilated = imclose(stomach_mask_dilated,SE);
                                    stomach_mask_dilated = logical(stomach_mask_dilated); 
                                    f0map_raw(stomach_mask_dilated) = 0;
                                                         
                                    for i=1:length(act)
                                        switch char(tis(i))
                                            case {'bldplRV_act', 'art_activity'}
                                                f0map_raw(current_XCATmask==act(i)) = offset; 
                                        end
                                    end
                                    
                                    % Define mask for smoothing and extrapolation  
                                    mask = logical(f0map_raw); 
                                    
                                    for i=1:length(act)
                                        switch char(tis(i))
                                            case {'pericardium_activity'}
                                                mask(current_XCATmask==act(i)) = 1; 
                                        end
                                    end
                                    
                                    % Remove small components that affect
                                    % the extrapolation and smoothing
                                    % process 
                                    mask_closed = MRX.thresholdMask(logical(f0map_raw), 0.02); 
                                    f0map_raw = f0map_raw .* mask_closed; 
                                    
                                    % Smooth and extrapolate field map 
                                    f0map_raw(f0map_raw==0) = NaN; 
                                    f0map_init = smoothn(f0map_raw,mask,1e3);
                                    
                                    % Apply dampening mask to suppress
                                    % background 
                                    foreground = ones(size(current_XCATmask)); 
                                    foreground(current_XCATmask==20) = 0; % Stomach (not listed in MRX.Par.act)
                                    foreground(current_XCATmask==16) = 0; % Left lung (not listed in MRX.Par.act)
                                    foreground(current_XCATmask==0) = 0; 
                                    foreground_mask = MRX.thresholdMask(foreground, 0.1); 

                                    % Dilate the mask to prevent gradients
                                    % directly between the object and background 
                                    SE = strel('disk',15); 
                                    foreground_mask = imdilate(foreground_mask,SE); 
                                    
                                    dampening_mask = bwdist(foreground_mask); 
                                    dampening_mask(dampening_mask==0) = 1; 
                                    dampening_mask = nthroot(dampening_mask,2);                                    
                                    f0map = f0map_init ./ dampening_mask;  
                                      
                                    %----------------------------------------------
                                    % Assess the gradient the LV sector. 
                                    % Scale field map if necessary 
                                    %----------------------------------------------                                    
                                    % - Fx = gradient in horizontal
                                    %        direction
                                    % - Fy = gradient in vertical direction
                                    [Fx,Fy] = gradient(f0map,FieldMap_OVS_pixel_size(1),FieldMap_OVS_pixel_size(2)); 
                                   
                                    switch lower(EncPars.preparationdirection)
                                        case 'ap'
                                            preparation_direction_gradient_map = Fy; 
                                        case 'lr'
                                            preparation_direction_gradient_map = Fx; 
                                    end
                                    
                                    roi_gradient_data = preparation_direction_gradient_map(LV_ROI);

                                    % This ensures the maximum gradient in the sector 
                                    % is set to the target gradient strength.
                                    scale_factor = quantile(abs(roi_gradient_data),0.99)/ abs(MRX.Par.f0.targetgradient); 
                                    F0maps(:,:,slice_idx) = f0map ./ scale_factor; 
                                end                                
                            end
                        end

                        % Assign through-slice field effect 
                        % - Project a linear slope along the kz_slice axis 
                        dz_ts = 1 / MRX.Par.f0.nz_ts;

                        % Define the gradient across the slice 
                        % Choice between
                        % - 'linear' - A linear ramp centered around zero
                        % - 'Quadratic' - Impose a quadratic frequency profile across slice. 
                        %                 Mean gradient is MRX.Par.enc.gradienttroughslice 
                        switch lower(MRX.Par.f0.throughsliceprofile)
                            case 'linear'
                                kz_grad_ts = (-floor(MRX.Par.f0.nz_ts/2):ceil(MRX.Par.f0.nz_ts/2)-1) * (MRX.Par.f0.throughslicegradient * dz_ts);
                            case 'quadratic'
                                kz_grad_ts = ((-floor(MRX.Par.f0.nz_ts/2):ceil(MRX.Par.f0.nz_ts/2)-1).^2);

                                if (MRX.Par.f0.throughslicegradient ~= 0 && MRX.Par.f0.nz_ts > 1)
                                    kz_grad_ts = kz_grad_ts * (MRX.Par.f0.throughslicegradient * dz_ts);
                                    kz_grad_ts = kz_grad_ts ./ (mean(abs(gradient(kz_grad_ts,dz_ts))) /  MRX.Par.f0.throughslicegradient);
                                else
                                    kz_grad_ts = zeros(size(kz_grad_ts)); 
                                end 
                        end  

                        % Add through-slice gradient dFdz onto the fieldmap(s)
                        F0maps = repmat(F0maps,[1 1 1 MRX.Par.f0.nz_ts 1]); 

                        for slice_idx = 1:img_size(3)
                            for z_ts_idx = 1:MRX.Par.f0.nz_ts  
                                F0maps(:,:,slice_idx,z_ts_idx) = F0maps(:,:,slice_idx,z_ts_idx) + (kz_grad_ts(z_ts_idx) * ones(size(F0maps,1),size(F0maps,2)));
                            end
                        end
                        % -----------------------------------------------------
                        % Optional: Compute and display gradient maps 
                        % - Computes the average gradient through-slice 
                        % -----------------------------------------------------
                        if strcmpi(debug_flag,'true')
                            figure, hold on; 
                            for ix = 1:img_size(3)
                                subplot(1,img_size(3),ix);                            
                                plot(-floor(MRX.Par.f0.nz_ts  /2):ceil(MRX.Par.f0.nz_ts  /2)-1,kz_grad_ts); axis square; xlabel('Kz'); ylabel('\DeltaF_{z}'); 
                                title(['\nablaF_{through slice,mean} profile: ', num2str(MRX.Par.f0.throughslicegradient) ,'Hz/slice. Slice: ', num2str(ix)]); 
                                set(gca,'FontWeight', 'Bold', 'FontSize', 10); 
                            end 
                            for ix = 1:img_size(3)
                                figure; 
                                
                                [~,Fy,Fz] = gradient(squeeze(F0maps(:,:,slice_idx,:,1)),FieldMap_OVS_pixel_size(1),FieldMap_OVS_pixel_size(2),dz_ts); 

                                subplot(211); 
                                imagesc(abs(Fy(:,:,slice_idx))); colormap hot; daspect([1 1 1]); colorbar; axis off; caxis([0 35]);
                                title(['|\nablaF_{y}|. Slice: ', num2str(ix)]); 

                                subplot(212); 
                                Fz_mean = mean(abs(Fz),3); 
                                imagesc(Fz_mean); colormap hot; daspect([1 1 1]); colorbar; axis off; caxis([0 MRX.Par.f0.throughslicegradient*5]);
                                title(['|\nablaF_{z,mean}|. Slice: ', num2str(ix)]); 
                            end
                        end 
                        % Assign field map for forward model [rad/s]
                        Fieldmaps(:,:,:,:,xcat_idx) = F0maps * 2 * pi; 
                    end
            end   
            Fieldmaps = single(Fieldmaps); 
            
            % Perform downsampling of field map for retrospective reconstruction 
            % - Take the mean across the through-slice direction for
            %   data reconstruction.
            Fieldmaps_DS = MRX.downsampleFieldMap(permute(Fieldmaps,[2 1 3 4 5]));  
            Fieldmaps_DS = mean(Fieldmaps_DS,4);  
        end 

        %=========================================================================
		% Get XCAT masks (for all tissues) and left-ventricular XCAT masks 
        function [XCATMasks, LVMasks] = getXCATMasks( MRX )
            % Input:    MRX                         MRXCAT object
            % 
            % Output:   XCAT masks 
            %           LV masks                    
            %
            
            if (MRX.Par.scan.respiratorymotion==0)
                no_xcat_frames = 1; 
            elseif (MRX.Par.scan.respiratorymotion==1) || (MRX.Par.scan.respiratorymotion==2)
                no_xcat_frames = MRX.Par.scan.frames_xcat; 
            end
            
            % -------------------------------------------------------------
            % Load all available XCAT masks and crop to desired object
            % matrix size 
            % - Select the LV myocardium masks from these XCAT masks  
            % -------------------------------------------------------------
            XCATMasks = zeros(MRX.Par.enc.nx_OVS, MRX.Par.enc.ny_OVS,MRX.Par.scan.slices,no_xcat_frames);

            switch lower(MRX.Par.enc.preparationdirection)
                case 'ap'
                    XCATMasks = permute(XCATMasks,[2 1 3 4]); 
            end

            for xcat_idx = 1:no_xcat_frames
                msk = MRX.readImgData(xcat_idx);
                msk = MRX.cropImage(msk); 
                XCATMasks(:,:,:,xcat_idx) = msk; 
            end
            
            if isfield(MRX.Par.act,'myoLV_act')
                LVMasks = (XCATMasks==MRX.Par.act.myoLV_act); 
            else
                error('Could not identify left-ventricular myocardial XCAT mask in MRX.Par.act');  
            end
            % Cast to double for later indexing methods
            LVMasks = double(LVMasks); 
            
            % Optionally: Export the LV masks 
            if strcmpi(MRX.Par.proc.exportmasks,'yes')
                [pathstr,~,~] = fileparts(MRX.Filename);
                filepath = fullfile(pathstr,'export'); 
                if ~isfolder(filepath)
                    mkdir(filepath)
                end
                
                try 
                    fname_XCAT = strcat('XCATMasks_',MRX.Par.enc.preparationdirection,'.mat'); 
                    fname_LV = strcat('LVMasks_',MRX.Par.enc.preparationdirection,'.mat'); 
                    save(fullfile(filepath,fname_XCAT),'XCATMasks'); 
                    save(fullfile(filepath,fname_LV),'LVMasks');    
                catch ME
                   throw(ME);  
                end
            end
        end 
        
        %=========================================================================
		% Encode the data to k-space 
        function [img_out, kspace_out] = encodeData( MRX, img, T2starmaps, fieldmaps ) 
            % Input:    MRX                         MRXCAT object
            %           img                         Object image w/contrast
            %                                       (multi-coil data)
            %           T2starmap                   T2star map 
            %           fieldmaps                   Field map(s) [rad/s]
            %   
            % Output:   img_out          			Encoded image
			%           kspace_out					K-space 
            %
            %
            %   This 2D encoder allows inclusion of 
            %   - Off-resonance effects (in-plane + through-slice)
            %   - T2star decay during encoding 
            % 
            % Notes 
            % -> Be aware that the term 2*pi in both exponential functions
            %    is applied in previous steps.

            % Assess input parameters 
            img_size = size(img); 
            img_size(3) = MRX.Par.scan.slices;
            img_size(4) = size(img,4); 
            
            % Get encoding parameters
            pars = MRX.Par.enc; 
            nx = pars.nx; 
            ny = pars.ny; 
            
            if nargin < 3 % No F0map and T2star map
                T2starmaps = inf(img_size(1:3)); 
                fieldmaps = zeros(img_size(1:3)); 
                fieldmaps = repmat(fieldmaps,[1 1 1 MRX.Par.f0.nz_ts]);
            end 
            if nargin < 4 % No F0 map 
                fieldmaps = zeros(img_size(1:3)); 
                fieldmaps = repmat(fieldmaps,[1 1 1 MRX.Par.f0.nz_ts]);
            end
            
            switch lower(MRX.Par.enc.preparationdirection)
                case 'ap'
                    % Transpose data to frequency/phase encoding axes 
                    img = permute(img,[2 1 3 4]); 
                    fieldmaps = permute(fieldmaps,[2 1 3 4]); 
                    T2starmaps = permute(T2starmaps,[2 1 3 4 5]); 
            end
            
            % Get sampling times [s]
            sampling_times = MRX.getSamplingTimes; 
            
            % Get voxel coordinates [rad*mm] 
            [x_coord, y_coord] = MRX.getVoxelCoordinates; 
            
            % Get kspace coordinates [1/mm]
            [kx_coord,ky_coord,FT,no_samples,no_profiles] = MRX.getKspaceCoordinates;
            
            % Allocate memory
            img_out     = zeros(nx,ny,img_size(3),img_size(4)); 
            kspace_out  = zeros(no_samples,no_profiles,1);
            
            % Define volume element for integration. 
            % To get correct scaling post-encoding, use the following
            % volume element definition. 
            volume_element = [1 1] ./ pars.OVS;
            
            % Initialize through-slice volume element
            % - Required to ensure the volume integral is correct.
            dz = 1/MRX.Par.f0.nz_ts; 
            volume_element(3) = dz; 
            nz_ts = MRX.Par.f0.nz_ts;
                       
            %-------------------------------------------------%
            % Loop over all slices
            %-------------------------------------------------%
            for slice_idx = 1:img_size(3)
                
                % Select current slice data
                F0Maps_slice = squeeze(fieldmaps(:,:,slice_idx,:));
                T2starmap_slice = squeeze(T2starmaps(:,:,slice_idx,:));
                
                % Vectorize 
                F0Maps_slice = reshape(F0Maps_slice,[size(F0Maps_slice,1)*size(F0Maps_slice,2),size(F0Maps_slice,3)]); 
                T2starmap_slice = reshape(T2starmap_slice,[size(T2starmap_slice,1)*size(T2starmap_slice,2),size(T2starmap_slice,3)]); 

                % Assess non-zero elements of current slice 
                % - Use first coil image as default
                img_coil = reshape(img(:,:,slice_idx,1),[],1); 
                nz_idx = find(abs(img_coil)>0);

                % Flatten arrays 
                x_coord_local = x_coord(nz_idx);
                y_coord_local = y_coord(nz_idx);
                F0Maps_slice = F0Maps_slice(nz_idx,:); 
                T2starmap_slice = T2starmap_slice(nz_idx,:);
                
                %-------------------------------------------------%
                % Loop over every coil image to encode the data 
                %-------------------------------------------------%
                for coil_idx = 1:img_size(4)

                    % Vectorize and flatten current coil image
                    img_coil = reshape(img(:,:,slice_idx,coil_idx),[],1); 
                    img_coil = img_coil(nz_idx); 
                    
                    %-----------------------------------------------------%
                    % Loop over every profile and through-slice component
                    %-----------------------------------------------------%
                    out = zeros(no_samples,no_profiles,nz_ts);
                    
                    for prof_idx = 1:no_profiles
                        
                        for dz_idx = 1:nz_ts
                            % Select current sub-section of the slice for
                            % through-slice simulation
                            F0Maps_slice_dz  = F0Maps_slice(:,dz_idx);
                            T2starmap_slice_dz = T2starmap_slice(:,dz_idx);
                            
                            parfor ksp_idx = 1:length(kx_coord)
                                %-------------------------------------------------%
                                % Generate phase of Fourier encoding
                                %-------------------------------------------------%
                                % Units: 
                                %   k_x/k_y-coord   = [1/mm]
                                %   x_coord/y_coord = [rad*mm]
                                %-------------------------------------------------%
                                phase = kx_coord(ksp_idx,prof_idx,slice_idx).*x_coord_local  + ky_coord(ksp_idx,prof_idx,slice_idx).*y_coord_local; 
                                    
                                %-------------------------------------------------%
                                % Generate dB0 phase and add to encoding phase
                                % [rad]
                                %-------------------------------------------------%
                                phase = phase(:) + (F0Maps_slice_dz .* sampling_times(ksp_idx) ) ...
                                                 + (-1i*abs(sampling_times(ksp_idx))./ T2starmap_slice_dz);
                                
                                %-------------------------------------------------%
                                % Evaluate integral using the summation over all 
                                % image pixels. 
                                % - Sums over the through-slice index
                                %-------------------------------------------------% 
                                out(ksp_idx,prof_idx,dz_idx) = (exp(-1i * phase)' * img_coil) * prod(volume_element); % Correct for dx * dy * dz_ts of volume integral 
                            end
                        end
                    end
                    % Finalize integral for through-slice components
                    out = sum(out,3); 
                    ksp = out./sqrt(prod([nx,ny]));
                    kspace_out(:,:,slice_idx,coil_idx) = ksp;                   
                    img_out(:,:,slice_idx,coil_idx) = FT{slice_idx}'*ksp(:); 
                end
            end
            % Transpose to original orientation 
            switch lower(MRX.Par.enc.preparationdirection)
                case 'ap'
                    img_out = permute(img_out,[2 1 3 4]); 
            end
        end

        %=========================================================================
        % Compute noise standard deviation based on desired SNR and signal
        % intensity in ROI
        function stdev = computeNoiseStdDev( MRX, sen )
            %   Input:  MRX         MRXCAT instance
            %           sen         coil sensitivity maps
            % 
            %   Output: stdev       Std. deviation of noise
            %           SNRmap      SNR map 
            % 
            
            if (MRX.Par.scan.snr ~= inf)
                % -------------------------------------------------------------
                % We only use the first time series to get an assessment of the 
                % noise levels, since we do not expect a contrast difference across
                % different b=0 s/mm2 images. 
                % -------------------------------------------------------------
                data = MRX.readImgData(1);
                data = MRX.cropImage(data); 
                [img,msk,T2starmap] = MRX.mapTissueProps(data); 
                img = MRX.multiplyCoilMaps(img,sen);

                % Produce the initial encoded image (b = 0 s/mm�)
                % Not affected by F0 (no F0 input argument) 
                img = MRX.encodeData(img,T2starmap);

                % Resize the mask, and sensitivites to the target 
                % acquisition resolution
                imgsize = size(img);
                msk = imresize(msk,imgsize(1:2),'nearest'); msk = floor(msk); 
                sen = imresize(sen,imgsize(1:2)); 
                
                roi = msk==MRX.Par.act.myoLA_act|msk==MRX.Par.act.myoLV_act;
                
                % Only select inner 75% transmural depth for computing
                % left ventricular SNR. 
                [transmural_depth_map] = MRX.getLocalAngleAndDepthMaps(roi);
                transmural_depth_map(transmural_depth_map>0.875) = 0; 
                transmural_depth_map(transmural_depth_map<0.125) = 0;
                transmural_depth_map = logical(transmural_depth_map); 
                roi = roi .* transmural_depth_map; 
                idx = find(roi>0);
                if idx==0 
                    roi = ones(size(roi));
                end     
                
                % find biggest connected mask = heart (if > 1 connected region)
                conn = bwconncomp(roi);
                if conn.NumObjects > 1
                    for k=1:conn.NumObjects
                        len(k)  = length(conn.PixelIdxList{k});
                    end
                    [~,maxk]    = max(len);
                    idxs        = conn.PixelIdxList{maxk};
                    roi         = zeros(size(roi));
                    roi(idxs)   = 1;
                end
                roiall(:,:,:,:) = repmat(roi,[1,1,1,size(sen,4)]).*img;
            
                % -------------------------------------------------------------
                % Determine the noise standard deviation, based on the work of 
                % Larsson et al, JMRI 2003. 
                % - This serves as an initial estimate for the standard
                %   deviation.
                % -------------------------------------------------------------
                for k=1:MRX.Par.scan.coils, roik = roiall(:,:,:,k); smean(k) = mean(roik(roik~=0)); end
                sosmean     = sqrt(sum(abs(smean).^2));
                stdev       = (1/MRX.Par.scan.snr)*sosmean;
                fprintf('initial noise std dev : %f\n',stdev);

                % Assign initial noise standard deviation
                stdev_init = stdev;
                
                % -------------------------------------------------------------
                % Iteratively refine noise standard deviation
                % -------------------------------------------------------------
                MRX.Par.scan.noisestd = MRX.refineNoiseStd(img, sen, roi, stdev_init);   
            else
                % In case of noiseless data, set noise stdev to 0 
                MRX.Par.scan.noisestd = 0;           
                fprintf('Myocardial SNR set to inf. Setting noise standard deviation to 0! \n');
            end
        end
        
        %=========================================================================
		% Calculate signal intensities using tissue and sequence pars
        function [img,msk,T2starmap,T2map] = mapTissueProps( MRX, data )
            % Input:    MRX                         MRXCAT object
            %           data                        *.bin file contents
            %
            % Output:   img                         Image with mapped tissue properties
			%           msk                         Mask image
            %           T2starmap                   T2star map
            %           T2map                       T2 map
            %                
            act = cell2mat(struct2cell(MRX.Par.act));
            tis = fieldnames(MRX.Par.act);
            img = single(zeros(size(data)));
            msk = uint8(zeros(size(data)));
            T2map = single(zeros(size(data)));
            T2starmap = single(zeros(size(data,1),size(data,2),MRX.Par.scan.slices,MRX.Par.f0.nz_ts)); % Prepare for through-slice simulation 

            nsa = MRX.Par.scan.numberofaverages;
            
            if isfield(MRX.Par.proc,'rngseedt2star')
                % Use same rng as this ensures the T2star tissue map is
                % generated identically for every slice. 
                if ~isempty(MRX.Par.proc.rngseedt2star)
                    s = rng; 
                    rng(MRX.Par.proc.rngseedt2star)
                end
            end
            % ----------------------------------------------------------------
            %   Loop over slices. Assign a TR per slice (if requested).
            % ----------------------------------------------------------------  
            for slice_idx = 1:MRX.Par.scan.slices     
                current_slice = data(:,:,slice_idx);
                current_img = zeros(size(img,1), size(img,2)); 
                current_T2map = zeros(size(img,1), size(img,2));
                current_msk  =  uint8(zeros(size(img,1), size(img,2)));
                current_T2starmap = single(zeros(size(data,1),size(data,2),MRX.Par.f0.nz_ts));
                
                for i=1:length(act)
                    % ----------------------------------------------------------------
                    %   Select tissue type
                    % ----------------------------------------------------------------
                    switch char(tis(i))
                            % ------------------------------------------------------------
                            %   Myocardium
                            % ------------------------------------------------------------
                        case {'myoLV_act','myoRV_act','myoLA_act','myoRA_act'}
                            rho     = MRX.Par.tissue.rhomuscle;
                            t1      = MRX.Par.tissue.t1muscle;
                            t2      = MRX.Par.tissue.t2muscle;
                            t2star  = MRX.Par.tissue.t2starmuscle;
                            
                            % ------------------------------------------------------------
                            %   Blood
                            % ------------------------------------------------------------
                        case {'bldplLV_act','bldplRV_act','art_activity','vein_activity','bldplLA_act','bldplRA_act'}
                                                        
                            switch lower(MRX.Par.contrast.diff.sequence)
                                case {'se','spin-echo'}
                                    rho = MRX.Par.tissue.rhoblood;
                                case 'steam'
                                    rho = 0; % Removes blood signal for STEAM 
                            end
                            
                            t1      = MRX.Par.tissue.t1blood;
                            t2      = MRX.Par.tissue.t2blood;
                            t2star  = MRX.Par.tissue.t2starblood;
                            
                            % ------------------------------------------------------------
                            %   Body
                            % ------------------------------------------------------------
                        case {'body_activity','pericardium_activity'}
                            rho     = MRX.Par.tissue.rhofat;
                            t1      = MRX.Par.tissue.t1fat;
                            t2      = MRX.Par.tissue.t2fat;
                            t2star  = MRX.Par.tissue.t2starfat;
                             
                        case 'muscle_activity'
                            rho     = MRX.Par.tissue.rhomuscle;
                            t1      = MRX.Par.tissue.t1muscle;
                            t2      = MRX.Par.tissue.t2muscle;
                            t2star  = MRX.Par.tissue.t2starmuscle;
                            
                        case 'liver_activity'
                            rho     = MRX.Par.tissue.rholiver;
                            t1      = MRX.Par.tissue.t1liver;
                            t2      = MRX.Par.tissue.t2liver;
                            t2star  = MRX.Par.tissue.t2starliver;
                            
                        case {'rib_activity','cortical_bone_activity','spine_activity','bone_marrow_activity'}
                            rho     = MRX.Par.tissue.rhobone;
                            t1      = MRX.Par.tissue.t1bone;
                            t2      = MRX.Par.tissue.t2bone;
                            t2star  = MRX.Par.tissue.t2starbone;
                        otherwise
                            rho = 0;
                    end
                    % ----------------------------------------------------------------
                    %   Signal models 
                    %   - Spin-echo
                    %   - STEAM 
                    % ----------------------------------------------------------------      
                    te  = MRX.Par.scan.te;
                    fa  = MRX.Par.scan.flip;
                    tr  = MRX.Par.scan.tr_array(slice_idx,MRX.Par.proc.img_nr); 
                    
                    if rho > 0 
                        switch lower(MRX.Par.contrast.diff.sequence)
                            case {'spin-echo','se'}
                                % Simplified model (only valid for flip angle of 90 degrees)
                                % sig = rho .* (1 - exp(-tr./t1)).*exp(-te./t2);

                                % Full signal model (valid for arbitrary flip angles)
                                mz_n = 1.*(1-exp(-tr/t1));
                                
                                if (MRX.Par.proc.img_nr == 1)
                                    sig = rho * (mz_n + (1 * cos(fa) * exp(-tr/t1))) * sin(fa) * exp(-te/t2); 
                                else
                                    mz_n_prev = MRX.Par.scan.mz_array(slice_idx, MRX.Par.proc.img_nr-1);
                                    sig = rho * (mz_n + (mz_n_prev * cos(fa) * exp(-tr/t1))) * sin(fa) * exp(-te/t2); 
                                end

                            case 'steam'

                                % Simplified model (only valid for flip angle of 90 degrees)
                                % - Assumes the TR is the same between the 2R-R period  
                                % - Assumes that TE << T1
                                % sig = rho .* 0.5 .* (1 - exp(-tr./t1)).*exp(-tr./t1).*exp(-te./t2);  
                                
                                % Assume that the 2R-R interval has
                                % similar consecutive heart rates 
                                tr = tr / 2; 

                                % Full signal model (valid for arbitrary flip angles)
                                % - Assumes the TR is the same between the 2R-R period 
                                % - Assumes that TE << T1
                                mz_n = 1.*(1-exp(-tr/t1));

                                if (MRX.Par.proc.img_nr == 1)
                                    sig = 0.5 * rho * (mz_n + (1 * cos(fa)* exp(-tr/t1))) * exp(-tr/t1) * sin(fa) * exp(-te/t2); 
                                else
                                    mz_n_prev = MRX.Par.scan.mz_array(slice_idx, MRX.Par.proc.img_nr-1);
                                    sig = 0.5 * rho * (mz_n + (mz_n_prev * cos(fa) * exp(-tr/t1))) * exp(-tr/t1) * sin(fa) * exp(-te/t2); 
                                end
                        end 
                        % Store Mz value to recall Mz history for next
                        % 'excitation'. 
                        % Take into account the respiratory motion case.
                        switch MRX.Par.scan.respiratorymotion
                            case {0,1} % Static / breath hold 
                                MRX.Par.scan.mz_array( slice_idx, (MRX.Par.proc.img_nr:MRX.Par.proc.img_nr+nsa-1)) = mz_n; 
                            case 2 % Free-breathing 
                                MRX.Par.scan.mz_array( slice_idx, MRX.Par.proc.img_nr) = mz_n;  
                        end
                    else
                        % Set signal to 0, such that 'empty' voxels can be 
                        % selected for flattening of the image array in EncodeData 
                        sig = 0;
                    end
                    % ----------------------------------------------------------------
                    %   Update tissue compartments
                    % ----------------------------------------------------------------
                    current_img(current_slice==act(i)) = sig;
                    current_T2map(current_slice==act(i)) = t2/1000; %[s]
                    
                    % ----------------------------------------------------------------
                    %   Generate T2star maps for the forward model 
                    %   - Include the number of through-slice components 
                    %   - Generate unique T2star map per dz section
                    %
                    %   - Define distribution for t2star
                    %       - Setup as a percentage of the parameter value  
                    % ----------------------------------------------------------------
                    nsamples = max([length(find(current_slice(:)==act(i)))*5; 10000]);
                    t2star_std  = MRX.Par.tissue.t2star_std;
                    
                    for iz = 1:MRX.Par.f0.nz_ts
                        tmp = zeros(size(img,1),size(img,2)); 
                        random_indices = randi(nsamples,length(find(current_slice(:)==act(i))),1);
                        t2star_dist = normrnd(t2star,t2star_std*(t2star/100),[1,nsamples]);
                        tmp(current_slice==act(i)) = t2star_dist(random_indices)/1000; %[s]
                        current_T2starmap(:,:,iz) = current_T2starmap(:,:,iz) + tmp;
                    end
                    % ----------------------------------------------------------------
                    %   Update tissue masks
                    % ----------------------------------------------------------------
                    current_msk(current_slice==act(i)) = act(i);
                end 
                img(:,:,slice_idx) = current_img;
                T2map(:,:,slice_idx) = current_T2map; 
                T2starmap(:,:,slice_idx,:) = current_T2starmap; 
                msk(:,:,slice_idx) = current_msk; 
            end
            T2map(T2map == 0) = eps;
            T2starmap(T2starmap == 0) = eps;  
            img = single(img); msk = single(msk); % Cast to single precision.
            
            if isfield(MRX.Par.proc,'rngseedt2star')
                % Reset to initial RNG setting 
                if ~isempty(MRX.Par.proc.rngseedt2star)
                    rng(s); % Reset to initial values 
                end
            end
        end
                
        %=========================================================================
		% Map the model-based diffusion and perfusion contrast to the data
        function img = mapDiffusionProps( MRX, data, img, msk, d, tensors, fmap_masks, xcat_no) 
            % Input:    MRX                         MRXCAT object
            %           data                        *.bin file contents
            %           img                         Data with tissue properties
            %           msk                         Mask
            %           d                           Diffusion direction
            %           tensors                     Struct with all tensors  
            %           fmap_masks                  Perfusion fraction f
            %                                       LV masks
            %           xcat_no                     Current XCAT mask
            %                                       number
            %
            % Output:   img                         Image with mapped diffusion contrast
            %                       
            tissue_numbers = cell2mat(struct2cell(MRX.Par.act));
            tissue_types = fieldnames(MRX.Par.act);   
            
            dimX = size(img,1); dimY = size(img,2); dimZ = size(img,3);
            
            for i=1:length(tissue_numbers)
                % ---------------------------------------------------------
                %   Select tissue type
                % ---------------------------------------------------------
                switch char(tissue_types(i))
                    
                    % Select the diffusion directions and b-values here 
                    % -----------------------------------------------------
                    %   Myocardium
                    % -----------------------------------------------------
                    case {'myoLV_act'}     
                        
                        curr_diff_dirs = MRX.Par.contrast.diff.directions(d,:);                       
                        curr_bvalues = MRX.Par.scan.bvalue_array(:,MRX.Par.proc.img_nr); 

                        if sum(curr_bvalues) ~= 0
                            % -------------------------------------------------
                            %   Find MRI signal and mask of corresponding 
                            %   tissue compartment
                            % -------------------------------------------------             
                            sig = img.*(data==tissue_numbers(i));
                            msk = msk.*(data==tissue_numbers(i));

                            % -------------------------------------------------
                            %   Get fmaps
                            % -------------------------------------------------
                            current_fmap_mask = fmap_masks(:,:,:,xcat_no); 
                            fMap = MRX.getFMap(current_fmap_mask); 
                            clear fmap_masks; 

                            % -------------------------------------------------
                            %   Compute the signal attenuation for D and Dstar 
                            % -------------------------------------------------
                            diff_tensors_healthy = tensors.diffusion_tensors_healthy(:,:,:,:,:,xcat_no); 
                            perf_tensors_healthy = tensors.perfusion_tensors_healthy(:,:,:,:,:,xcat_no);  
                            diff_tensors_lesion = tensors.diffusion_tensors_lesion(:,:,:,:,:,xcat_no); 
                            perf_tensors_lesion = tensors.perfusion_tensors_lesion(:,:,:,:,:,xcat_no);  
                            clear tensors;           
                            
                            for iZ=1:dimZ
                                local_bvalue = curr_bvalues(iZ,1); 
                                for iY=1:dimY
                                    parfor iX=1:dimX
                                        if current_fmap_mask(iX,iY,iZ)==1 % healthy 
                                            D = dot( curr_diff_dirs * squeeze(diff_tensors_healthy(iX,iY,iZ,:,:)), curr_diff_dirs, 2); 
                                            MD_Dstar = dot( curr_diff_dirs * squeeze(perf_tensors_healthy(iX,iY,iZ,:,:)), curr_diff_dirs, 2); 
                                            
                                            img(iX,iY,iZ,1) =  sig(iX,iY,iZ) * ( (((1 - fMap(iX,iY,iZ) ) * ( exp(-local_bvalue * D)))) + ...
                                                                (fMap(iX,iY,iZ) * ( exp(-local_bvalue * MD_Dstar))) );  

                                        elseif current_fmap_mask(iX,iY,iZ)==2 % lesion 
                                            D = dot( curr_diff_dirs * squeeze(diff_tensors_lesion(iX,iY,iZ,:,:)), curr_diff_dirs, 2); 
                                            MD_Dstar = dot( curr_diff_dirs * squeeze(perf_tensors_lesion(iX,iY,iZ,:,:)), curr_diff_dirs, 2); 
                                            
                                            img(iX,iY,iZ,1) =  sig(iX,iY,iZ) * ( (((1 - fMap(iX,iY,iZ) ) * ( exp(-local_bvalue * D)))) + ...
                                                                (fMap(iX,iY,iZ) * ( exp(-local_bvalue * MD_Dstar))) );  
                                        end  
                                    end
                                end
                            end
                        end 
                        
                    case {'myoRV_act','pericardium_activity'}
                        % Apply global signal attenuation in mask area
                        % using MD value 
                        msk = abs((data==tissue_numbers(i)));
                        MD_D = MRX.Par.contrast.diff.d; 
                        curr_bvalues = MRX.Par.scan.bvalue_array(:,MRX.Par.proc.img_nr); 
                       
                        if sum(curr_bvalues) ~= 0
                            for iZ=1:dimZ
                                local_bvalue = curr_bvalues(iZ,1); 
                                for iY=1:dimY
                                    parfor iX=1:dimX
                                        if msk(iX,iY,iZ)
                                            img(iX,iY,iZ,1) = img(iX,iY,iZ,1) * ( exp(-local_bvalue * MD_D ));                 
                                        end
                                    end
                                end
                            end
                        end
                        
                    case {'bldplLV_act','bldplRV_act','bldplLA_act','bldplRA_act','art_activity','vein_activity'}
                        % Apply global signal attenuation in mask area using fixed
                        % b-value 
                        msk = abs((data==tissue_numbers(i)));
                        MD_D = MRX.Par.contrast.diff.d;

                        if (MRX.Par.contrast.diff.bvaluearray(d) ~= 0) || strcmpi(MRX.Par.contrast.diff.sequence,'steam')
                            for iZ=1:dimZ
                                for iY=1:dimY
                                    parfor iX=1:dimX
                                        if msk(iX,iY,iZ)
                                            img(iX,iY,iZ,1) = img(iX,iY,iZ,1) * ( exp(-10000 * MD_D ));                 
                                        end
                                    end
                                end
                            end
                        end
                        
                    case {'muscle_activity','liver_activity', 'bone_marrow_activity'}
                        
                        typ = char(tissue_types(i));
                        
                        switch typ 
                            case 'muscle_activity'
                                ADC_val = 1.5*1E-3; 
                            case 'liver_activity'
                                ADC_val = 1*1E-3;
                            case 'bone_marrow_activity'
                                ADC_val = 0.5*1E-3;
                        end
                        
                        % Perform global diffusion weighting for all other components 
                        msk = abs((data==tissue_numbers(i)));
                        curr_bvalues = MRX.Par.scan.bvalue_array(:,MRX.Par.proc.img_nr); 
                        
                        if sum(curr_bvalues)~= 0
                            for iZ=1:dimZ
                                local_bvalue = curr_bvalues(iZ,1); 
                                for iY=1:dimY
                                    parfor iX=1:dimX
                                        if msk(iX,iY,iZ)
                                            img(iX,iY,iZ,1) = img(iX,iY,iZ,1) * ( exp(-local_bvalue * ADC_val));                        
                                        end
                                    end
                                end
                            end
                        end 
                end
            end
        end
        
        %=========================================================================
        % Compute spatially-varying map for the perfusion fraction in the
        % presence of lesions        
        function  fMap = getFMap( MRX, fMap_masks )
            % Input:    MRX                         MRXCAT object
            %           fMap_masks                  Lesion masks 
            % 
            % Output:   fMap                        Perfusion fraction f
            %                                       map
            % 
           
            if isfield(MRX.Par.proc,'rngseedfmap')
                % Use same rng as this ensures the perfusion fraction map is
                % generated identically for every slice. 
                if ~isempty(MRX.Par.proc.rngseedfmap)
                    s = rng; 
                    rng(MRX.Par.proc.rngseedfmap)
                end
            end

            % Make a spatially distributed map for f 
            fstd = MRX.Par.contrast.diff.fstd; 
            f = MRX.Par.contrast.diff.f; 
            lesion_value = MRX.Par.lesion.lesionvalue;

            % Define fMap with values for normal and lesion
            % areas 
            msk_indices_myo = (fMap_masks==1);
            fMap = zeros(size(fMap_masks));
            fMap(msk_indices_myo) = f; 
            
            if (fstd > 0)  && (f > 0) 
                % Introduce distribution for f to the healthy myocardium
                pd_f = makedist('Normal','sigma',(fstd/100)*f);
                 % Ensure minimum value is never lower than the lowest value of f
                trunc_dist_f = truncate(pd_f,-f*0.99,Inf);
                f_noise = reshape(random(trunc_dist_f,numel(fMap),1),size(fMap));
                nz_indices = find(fMap_masks==1);
                fMap(nz_indices) = fMap(nz_indices) + f_noise(nz_indices);
            end
            if strcmpi(MRX.Par.lesion.enable,'yes')
                
                fMap(fMap_masks==2) = lesion_value; 
                if (fstd > 0)  && (f > 0) 
                    % Introduce distribution for f to the lesion 
                    pd_f = makedist('Normal','sigma',(fstd/100)*lesion_value); 
                    % Ensure minimum value is never lower than the lowest lesion value
                    trunc_dist_f = truncate(pd_f,-lesion_value*0.99,Inf); 
                    f_noise = reshape(random(trunc_dist_f,numel(fMap),1),size(fMap));
                    nz_indices = find(fMap_masks==2);
                    fMap(nz_indices) = fMap(nz_indices) + f_noise(nz_indices);
                end
            end
            fMap(isnan(fMap)) = 0;
 
            if isfield(MRX.Par.proc,'rngseedfmap')
                % Reset to initial RNG setting 
                if ~isempty(MRX.Par.proc.rngseedfmap)
                    rng(s);
                end
            end
        end
                     
        %=========================================================================
        % Allocate memory for data handling
        function [dwimgs, masks, kspace, fieldmaps] = allocateMemory( MRX )
            % Input:    MRX                         MRXCAT object
            % 
            % Output: (Allocated matrices for) 
            %           dwimgs                      Matrix for DWImages
            %           masks                       Matrix for Masks
            %           kspace                      Matrix for kspace
            %           fieldmaps                   Matrix for fieldmaps
            
            % Get pars
            EncPars = MRX.Par.enc; 
            
            % Get encoding parameters 
            nx = EncPars.nx;
            ny = EncPars.ny;                     
            nz    = MRX.Par.scan.slices;
            nc    = MRX.Par.scan.coils;
            ndirs = MRX.Par.contrast.diff.nrdiffusiondirections;
            
            % In static case, data is replicated prior to adding noise
            if (MRX.Par.scan.respiratorymotion==0)
                nsa   = 1;
            elseif (MRX.Par.scan.respiratorymotion==1) || (MRX.Par.scan.respiratorymotion==2)
                nsa   = MRX.Par.scan.numberofaverages;
            end

            % Define matrix sizes
            imageSize   = [nx, ny, nz, nc, ndirs, nsa];
            kspaceSize  = [EncPars.no_samples, EncPars.no_profiles, nz, nc, ndirs, nsa];
            maskSize    = [nx, ny, nz, ndirs, nsa];
            
            % Define Field map matrix size 
            fieldMapSize  = [MRX.Par.f0.downsampledf0matrix(1), MRX.Par.f0.downsampledf0matrix(2), nz, 1, ndirs, nsa];

            % Allocate matrices 
            dwimgs      = zeros(imageSize); 
            masks       = zeros(maskSize); 
            kspace      = zeros(kspaceSize);
            fieldmaps   = zeros(fieldMapSize); 
            
            switch lower(MRX.Par.enc.preparationdirection)
                case 'lr'
                    dwimgs = permute(dwimgs,[2 1 3 4 5 6]); 
                    masks = permute(masks,[2 1 3 4 5 6]); 
                    fieldmaps = permute(fieldmaps,[2 1 3 4 5 6]); 
            end
        end

        %=========================================================================
        % Set diffusion directions and b-value array for simulating diffusion contrast
        function [directions,bValues] = setDiffusionDirections( MRX )
        %=========================================================================
        % - Provided schemes are non-overplus (i.e. defined on a spherical surface),
        %   except for (1) which is defined on a cube. 
        %   
        % - Diffusion direction schemes (2 to 6) computed using QMRITools of Martijn
        %   Froeling 
        %   https://www.opensourceimaging.org/project/qmritools-mathematica-toolbox-for-quantitative-mri-data/
        %    
        % Available schemes 
        %   - (1) Custom  - Overplus - Suppresses blood pool signal
        %         (Default) 
        %         - 3 x user-defined b-value(1) s/mm� (3 orthogonal directions)
        %         - 9 x user-defined b-value(2) s/mm�
        %         - Used in von Deuster, et al (2016); DOI: 10.1002/mrm.25998 
        %         - This scheme is recommended for cDTI use only.
        %   - (2) DTI 6   - Non-overplus 
        %   - (3) DTI 9   - Non-overplus 
        %   - (4) DTI 12  - Non-overplus 
        %   - (5) DTI 15  - Non-overplus
        %   - (6) DTI 30  - Non-overplus
        %   
        % Input:    MRX                         MRXCAT object 
        %
        % Output:   directions                  Diffusion directions
        %           bValues                     list of b-values
        % 
        % Note 
        %  - All schemes are normalized to norm 1 for each diffusion
        %    direction.
        % 
            maxBfactor = max(MRX.Par.contrast.diff.bvalues(:)); 
            bValue_list = MRX.Par.contrast.diff.bvalues; 

            switch MRX.Par.contrast.diff.encodingscheme
                case 1 
                    % ----------------------------------- %
                    % Bloodpool suppressive scheme 
                    % ----------------------------------- %
                    directions_base = [ 0.0000,  0.0000,  0.0000; 
                                    -1.0000, -1.0000,  0.0000; 
                                     0.0000, -1.0000,  1.0000; 
                                     1.0000,  0.0000,  1.0000; 
                                     0.7869, -0.0100,  0.6170; 
                                    -0.6296,  0.2223,  0.7445; 
                                     0.6907, -0.6853,  0.2307; 
                                     0.6117,  0.7535,  0.2409; 
                                    -0.3516, -0.7930,  0.4975; 
                                     0.1050,  0.4587,  0.8823;
                                     0.0845, -0.3949,  0.9148; 
                                    -0.2219,  0.9406,  0.2570; 
                                    -0.9708, -0.1207,  0.2072];

                case 2 
                    % ----------------------------------- %
                    % 6 directions 
                    % ----------------------------------- %
                    directions_base = [0.0000,      0.0000,     0.0000;
                                 0.1455,      0.2714,      0.9514; 
                                -0.0548,     -0.7259,     0.6856; 
                                 0.6774,      0.7176,      0.1617; 
                                -0.3533,      0.8962,      0.2684; 
                                 0.8619,     -0.2849,      0.4195; 
                                -0.8058,     0.0040,      0.5922];

                case 3 
                    % ----------------------------------- %
                    % 9 directions 
                    % ----------------------------------- %
                    directions_base = [0.0000,      0.0000,     0.0000;        
                                 0.7869,     -0.0100,      0.6170;
                                -0.6296,      0.2223,      0.7445;
                                 0.6907,     -0.6853,      0.2307;
                                 0.6117,      0.7535,      0.2409;
                                -0.3516,     -0.7930,      0.4975;
                                 0.1050,      0.4587,      0.8823;
                                 0.0845,     -0.3949,      0.9148;
                                -0.2219,      0.9406,      0.2570;
                                -0.9708,     -0.1207,      0.2072];

                case 4 
                    % ----------------------------------- %
                    % 9 directions 
                    % ----------------------------------- %
                    directions_base = [0.0000,      0.0000,      0.0000;   
                                -0.6188,     -0.3529,      0.7018;
                                -0.0281,      0.9588,      0.2827;
                                -0.1514,      0.5084,      0.8477;
                                -0.8413,     -0.5398,      0.0287;
                                 0.6393,     -0.7689,      0.0023;
                                 0.6745,     -0.0690,      0.7350;
                                -0.2826,     -0.8962,      0.3421;
                                -0.0076,     -0.1630,      0.9866;
                                 0.9852,     -0.1316,      0.1102;
                                 0.5307,      0.6025,      0.5961;
                                -0.7627,      0.3185,      0.5629;
                                 0.3286,     -0.7063,      0.6271];

                case 5 
                    % ----------------------------------- %
                    % 15 directions 
                    % ----------------------------------- %
                    directions_base = [0.0000,      0.0000,      0.0000;   
                                -0.3799,     -0.3286,      0.8647;
                                 0.2194,     -0.6332,      0.7422;
                                -0.2277,      0.8665,      0.4442;
                                -0.5298,     -0.7460,      0.4034;
                                 0.3727,      0.5419,      0.7533;
                                 0.7355,     -0.0948,      0.6708;
                                -0.7576,      0.5095,      0.4081;
                                 0.1906,     -0.0186,      0.9815;
                                -0.3431,      0.3538,      0.8701;
                                 0.6677,     -0.7045,      0.2406;
                                 0.9851,     -0.1449,      0.0924;
                                 0.8613,      0.4642,      0.2065;
                                 0.0871,     -0.9725,      0.2160;
                                -0.8627,     -0.1620,      0.4791;
                                 0.3951,      0.8903,      0.2266];

                case 6 
                    % ----------------------------------- %
                    % 30 directions 
                    % ----------------------------------- %
                    directions_base = [0.0000,      0.0000,      0.0000; 
                                 0.7754,      0.4845,      0.4050;
                                 0.3915,     -0.8469,      0.3597;
                                 0.4975,      0.8603,      0.1108;
                                 0.9058,     -0.2906,      0.3083;
                                -0.7067,      0.5837,      0.3998;
                                -0.4176,      0.3814,      0.8247;
                                 0.4590,      0.3227,      0.8278;
                                 0.0094,      0.1910,      0.9815;
                                 0.0379,      0.6075,      0.7934;
                                 0.0610,      0.9292,      0.3644;
                                -0.4584,     -0.0862,      0.8846;
                                -0.0593,     -0.8585,      0.5094;
                                -0.5034,     -0.7870,      0.3566;
                                 0.7347,     -0.6731,      0.0843;
                                -0.7725,      0.1464,      0.6180;
                                 0.7808,      0.0424,      0.6234;
                                -0.7000,     -0.4004,      0.5913;
                                 0.6362,     -0.4899,      0.5960;
                                -0.9386,     -0.1671,      0.3019;
                                -0.9474,      0.2784,      0.1579;
                                 0.9749,      0.1497,      0.1646;
                                 0.4366,     -0.1461,      0.8877;
                                -0.8185,     -0.5691,      0.0789;
                                -0.3091,     -0.5595,      0.7690;
                                -0.3334,      0.7627,      0.5541;
                                 0.4283,      0.7117,      0.5568;
                                -0.3791,      0.9190,      0.1078;
                                -0.0669,     -0.9938,      0.0889;
                                -0.0201,     -0.2495,      0.9682;
                                 0.2264,     -0.5954,      0.7709];

                otherwise
                    error('Unknown diffusion encoding scheme');
            end

            % ----------------------------------- %
            % Prepare the directions
            % - Setup for diffusion or perfusion imaging
            % ----------------------------------- %
            if MRX.Par.contrast.diff.encodingscheme > 1

                number_of_b_values = length(nonzeros(bValue_list)); 

                if number_of_b_values > 1 
                    % ----------------------------------- %
                    % IVIM
                    % ----------------------------------- %
                    tmp = directions_base(2:end,:);
                    tmp = repmat(tmp,[(number_of_b_values-1) 1]);
                    directions = vertcat(directions_base,tmp); 

                    tmp = bValue_list(2:end); 
                    tmp = repmat(tmp,[size(directions_base,1)-1 1]);
                    tmp = tmp(:); 
                    bValues = vertcat(0,tmp); 
                else 
                    % ----------------------------------- %
                    % DTI 
                    % ----------------------------------- %
                    bValues = zeros(1,size(directions_base,1));
                    bValues(2:end) = maxBfactor;   
                    directions = directions_base; 
                end 
            else
                % ----------------------------------- %
                % For the blood pool suppressive scheme only
                % ----------------------------------- %
                directions = directions_base; 
                bValues = zeros(1,size(directions_base,1)); 
                bValues(2:4) = MRX.Par.contrast.diff.bvalues(2); 
                bValues(5:end) = maxBfactor; 
            end
            % ----------------------------------- %
            % Normalize directions 
            % ----------------------------------- %
            for id = 1:length(directions)
                if ~isempty(nonzeros(directions(id,:)))
                    directions(id,:) = directions(id,:) / norm(directions(id,:)); 
                end
            end

            % ----------------------------------- %
            % Assign parameters to MRX object 
            % ----------------------------------- %
            MRX.Par.contrast.diff.directions             = directions; 
            MRX.Par.contrast.diff.bvaluearray            = reshape(bValues,1,[]); 
            MRX.Par.contrast.diff.nrdiffusiondirections  = length(bValues); 
            MRX.Par.contrast.diff.numberofbvalues        = length(unique(bValues)); % Includes b=0s/mm2 image
        end 
        
        %=========================================================================
        % Update arrays for repetition times, excitation history (Mz), and
        % for STEAM the b-value array
        function MRX = updateArrays( MRX )
            % Input:    MRX                         MRXCAT object
            %           
            % Output:   MRX                         MRXCAT object (updated)
            %  
            % Array dimensions 
            % [slices, DWI(nsa x diffusion directions)] (TR array)
            % 
            % [slices, DWI(nsa x diffusion directions)] (Mz array)
            %
            % [slices, DWI(nsa x diffusion directions)] (b-value array)
            
            % Initialize the TR array 
            tr_std = MRX.Par.scan.tr_std / 100; % Convert to [%]
            tr = MRX.Par.scan.tr;
            ndirs = MRX.Par.contrast.diff.nrdiffusiondirections;
            nslices = MRX.Par.scan.slices; 
            nsa = MRX.Par.scan.numberofaverages; 
            
            switch MRX.Par.scan.respiratorymotion
                case {0,1} % Static or breath-hold: Use same TR for each NSA of a single diffusion direction 
                    TR_array = normrnd(tr,tr_std*tr,1, ndirs * nslices ); 
                    
                    % Set the first NSA of each slice to 'perfect' user-defined TR w/o std
                    TR_array(1:nslices) = tr; 
                    TR_array = reshape(TR_array,[nslices, 1, ndirs]);
                    TR_array = repmat(TR_array,[1 nsa 1]);
                    MRX.Par.scan.tr_array = reshape(TR_array,nslices,[]); 
                    
                case 2  % Free-breathing: Unique TR per acquired DWI 
                    nimg = ndirs * nsa* nslices;
                    TR_array = normrnd(tr,tr_std*tr,1,nimg);
                    
                    % Set the first NSA of each slice to 'perfect' user-defined TR w/o std
                    TR_array(1:nslices) = tr; 
                    MRX.Par.scan.tr_array = reshape(TR_array,[nslices, ( ndirs * nsa)]); 
            end
            
            % Initialize Mz array to record history of Mz variations 
            MRX.Par.scan.mz_array = ones(nslices, ndirs * nsa);
            
            % Initialize the b-value array
            MRX.Par.scan.bvalue_array = repmat(MRX.Par.contrast.diff.bvaluearray.',[1 nslices nsa]); 
            MRX.Par.scan.bvalue_array = permute(MRX.Par.scan.bvalue_array,[2 3 1]); 
            MRX.Par.scan.bvalue_array = reshape(MRX.Par.scan.bvalue_array,nslices,[]); 
            
            % Update the b-value array for the STEAM sequence. 
            % Scale the b-value under the assumption of instantaneous
            % diffusion gradients. 
            if strcmpi(MRX.Par.contrast.diff.sequence,'steam')
                MRX.Par.scan.bvalue_array = MRX.Par.scan.bvalue_array .* ((MRX.Par.scan.tr_array) ./ (MRX.Par.scan.tr_array(:,1))); 
            end
        end  
        
        %=========================================================================
        % Update parameters using the current *.bin data 
        function MRX = updatePars( MRX )
            % Input:    MRX                         MRXCAT object
            %
            % Output:   MRX                         MRXCAT object (updated)
            %
            %--------------------------------------------------------------
            % Scan parameters 
            %--------------------------------------------------------------  
            ScanPars = MRX.Par.scan; 
            F0Pars   = MRX.Par.f0; 
            
            %--------------------------------------------------------------
            % Define offcenters for multi-slice data
            % - z-axis is defined as the local long axis 
            % - Define the offcenters from one slice-center to the next
            %--------------------------------------------------------------  
            offcenters = zeros(ScanPars.slices,ScanPars.slices); 
            offcenters_z_axis_mm = ScanPars.rz_cm*10; 
            
            for slice_idx = 1:ScanPars.slices
                offcenters(slice_idx,3) = 0 + ((offcenters_z_axis_mm./2) * (slice_idx - 1));
            end
                        
            ScanPars.offcenters = offcenters; 
            MRX.Par.scan = ScanPars; 
                        
            if isfield(MRX.Par.scan,'selectedslices')
                if ~isempty(MRX.Par.scan.selectedslices)
                    MRX.Par.scan.slices = length(MRX.Par.scan.selectedslices); 
                end
            end

            %--------------------------------------------------------------
            % Get parameters 
            %--------------------------------------------------------------
            EncodePars = MRX.Par.enc; 
            DiffPars   = MRX.Par.contrast.diff; 
            LesionPars = MRX.Par.lesion; 
                       
            %--------------------------------------------------------------
            % Compute oversampling (OVS) factor, which indicates how much
            % higher the bandwidth of the object is with respect to the
            % target resolution
            %--------------------------------------------------------------
            rx_cm = MRX.Par.scan.rx_cm;
            OVS   = EncodePars.acqres(1) / rx_cm;
                       
            %--------------------------------------------------------------
            % Set parameters 
            %--------------------------------------------------------------
            EncodePars.nx                     = EncodePars.acqresmatrix(1);
            EncodePars.ny                     = EncodePars.acqresmatrix(2);
            EncodePars.nz                     = 1;
            EncodePars.OVS                    = OVS; 
            EncodePars.nx_OVS                 = fix(EncodePars.nx * OVS);
            EncodePars.ny_OVS                 = fix(EncodePars.ny * OVS);
            EncodePars.dx                     = rx_cm*10; % mm
            EncodePars.dy                     = rx_cm*10; % mm
            
            %--------------------------------------------------------------
            % Compute FOV of 'excited' image 
            %--------------------------------------------------------------
            x_fov = EncodePars.nx_OVS * rx_cm; %[cm]
            y_fov = EncodePars.ny_OVS * rx_cm; %[cm]
            EncodePars.fov = [x_fov, y_fov];
            
            %--------------------------------------------------------------
            % Store updated encoding parameters
            %--------------------------------------------------------------
            MRX.Par.enc = EncodePars; 
            
            % Call getKspaceCoordinates here to define the number of 
            % samples and profiles. 
            [~,~,~,no_samples,no_profiles] = MRX.getKspaceCoordinates;    
            MRX.Par.enc.no_samples = no_samples; 
            MRX.Par.enc.no_profiles = no_profiles; 
            
            %--------------------------------------------------------------
            % Print parameter file name 
            %--------------------------------------------------------------
            fprintf('Loaded parameters from          : %s \n',MRX.Par.proc.parname);
            fprintf('\n');
            
            %--------------------------------------------------------------
            % Print parameter list for encoding parameters
            %--------------------------------------------------------------
            fprintf('Encoding information: \n');
            fprintf('  Pixel size MRXCAT object [mm] : %.2f\n',rx_cm*10);
            fprintf('  Oversampling factor       [-] : %.2f\n',OVS);
            fprintf('  Acquisition pixels - x    [-] : %i\n',EncodePars.nx );
            fprintf('  Acquisition pixels - y    [-] : %i\n',EncodePars.ny );
            fprintf('  Acquisition pixels - z    [-] : %i\n',EncodePars.nz );
            fprintf('  Object pixels - x         [-] : %i\n',EncodePars.nx_OVS);
            fprintf('  Object pixels - y         [-] : %i\n',EncodePars.ny_OVS);
            fprintf('  Object resolution - x    [mm] : %.2f\n',EncodePars.dx);
            fprintf('  Object resolution - y    [mm] : %.2f\n',EncodePars.dy);
            fprintf('  Field-of-view - x        [mm] : %.2f\n',x_fov*10);
            fprintf('  Field-of-view - y        [mm] : %.2f\n',y_fov*10);
            fprintf('  Readout bandwidth    [Hz/pix] : %.2f\n',EncodePars.readoutbandwidth);
            fprintf('  Flip EPI blip direction       : %s\n',EncodePars.flipepiblipdirection);
            fprintf('\n');
            
            %--------------------------------------------------------------
            % Print parameter list for diffusion parameters 
            %--------------------------------------------------------------
            fprintf('Diffusion information: \n');
            fprintf('  Selected encoding scheme      : %i\n',DiffPars.encodingscheme);
            fprintf('  Load eigenvalue maps          : %s\n',DiffPars.loadeigenvaluemaps);
            
            if strcmpi(DiffPars.loadeigenvaluemaps,'No')
            	fprintf('  Helix angle range             : %.2f\n',DiffPars.helixanglerange);
                fprintf('  D               [1E-3 mm2/s]  : %.2f\n',DiffPars.d*1e3);
                fprintf('  Dstar           [1E-3 mm2/s]  : %.2f\n',DiffPars.dstar*1e3);
            end
            fprintf('  Load sheet angle maps         : %s\n',DiffPars.loadsheetanglemaps);
            
            if strcmpi(DiffPars.loadsheetanglemaps,'No')
                fprintf('  Sheet angle                   : %.2f\n',DiffPars.sheetanglevalue);
            end
            
            fprintf('  f               [-]           : %.2f\n',DiffPars.f);
            fprintf('  fstd            [%%-f]         : %.2f\n',DiffPars.fstd);
            fprintf('  Number of b-values            : %i\n',DiffPars.numberofbvalues);
            fprintf('  Number of diffusion directions: %i\n',DiffPars.nrdiffusiondirections);
            fprintf('  Sequence type                 : %s\n',DiffPars.sequence);
            fprintf('\n');
            
            %--------------------------------------------------------------
            % Print parameter list for F0 parameters 
            %--------------------------------------------------------------
            fprintf('F0 information: \n');
            fprintf('  Simulate F0                   : %s\n',F0Pars.simulatef0);
            fprintf('  Load F0 map(s)                : %s\n',F0Pars.loadf0maps);
            
            if strcmpi(F0Pars.loadf0maps,'No')
                fprintf('  Target gradient    [Hz/pix]   : %.2f\n',F0Pars.targetgradient);
                fprintf('  F0 value fat       [Hz]       : %.2f\n',F0Pars.f0_fat);
                fprintf('  Fat fraction       [-]        : %.2f\n',F0Pars.fatfraction);
            end
            
            fprintf('  Through-slice points              : %.2f\n',F0Pars.nz_ts);
            fprintf('  Gradient through-slice [Hz/slice] : %.2f\n',F0Pars.throughslicegradient);
            fprintf('  Through-slice profile             : %s\n',F0Pars.throughsliceprofile);
            fprintf('\n');
            
            %--------------------------------------------------------------
            % Print parameter list for lesion parameters 
            %--------------------------------------------------------------
            fprintf('Lesion information: \n');
            fprintf('  Enable lesions                : %s\n',LesionPars.enable);
            fprintf('  Lesion value                  : %.2f\n',LesionPars.lesionvalue);
            
            nlesions = size(MRX.Par.lesion.sectorangles,1); 
            fprintf('  Number of lesions             : %i\n',nlesions);
            fprintf('\n');
        end 
    end 
        methods ( Hidden )
            %=====================================================================
            % Text waitbar (replaces graphical waitbar)
            function strlen = textwaitbar( MRX, str_len_in, title )
            % MRX:			MRXCAT instance
            %
            % STR_LEN_IN:   length of previous waitbar
            % TITLE:        optional
            % 
            % HOWTO USE:    E.g. in a for loop, write 
            %               
            %               strlen = 0;
            %               for k=1:N 
            %                   strlen = textwaitbar(MRX, k, strlen, 'Calculating task name');
            %                   .... (tasks WITHOUT command line output)
            %               end 

            nr_imgs = MRX.Par.scan.numberofaverages * MRX.Par.contrast.diff.nrdiffusiondirections;
            frac    = MRX.Par.proc.img_nr/nr_imgs;

            len     = 40;
            numblk  = round(frac*len);
            strlen  = 0;

            if nargin > 2 && ~isempty(str_len_in) && str_len_in > 0
                for k=1:str_len_in
                    fprintf('\b');
                end
            else
                fprintf('\n------------------------------------------\n');
            end
            if nargin > 3
                fprintf('%s\n\n',title);
                strlen = strlen + length(title)+2; 
            else
                stdstr = 'Computing ...\n\n'; 
                fprintf(stdstr);
                strlen = strlen + length(stdstr)-2;
            end
            fprintf('[');
            strlen = strlen + 1;
            for k=1:numblk
                fprintf('o');
                strlen = strlen+1;
            end
            for k=1:len-numblk
                fprintf(' ');
                strlen = strlen+1;
            end
            fprintf(']');
            strlen = strlen + 1;

            if frac >= 1
                fprintf('\n\nDone.\n------------------------------------------\n');
            end
            end

            %=====================================================================
            % Function to downsample data using sinc kernel (downsample in k-space)
            function img_downsampled = sincDownsample(MRX, img, downsample_factor, dims)
            % Input: 
            %      - MRX:                   MRXCAT instance
            %      - img:                   Input images 
            %      - downsample_factor:     Factor to downsample data with
            %      - dims:                  Which dimensions to downsample
            %                               across
            % 
            % Output: 
            %      - img_downsampled:       Downsampled images
            % 
            if nargin == 3
                dims = [1,2];   
            end
            
            data_size = size(img); 
            img_center = fix(data_size ./2) + 1; 
            
            new_data_size = zeros(size(dims)); 
            new_img_center = zeros(size(dims)); 
            
            for ix = dims 
                new_data_size(ix) = fix(data_size(ix) ./ downsample_factor(ix)); 
                new_img_center(ix) = fix(new_data_size(ix) ./2) + 1; 
            end
            for ix = dims
                if ix == 1 
                    img_coord_x = img_center(1) + (1:new_data_size(1)) - new_img_center(1); 
                elseif ix ~= 2
                    img_coord_x = 1:data_size(1);
                end
                if ix == 2 
                    img_coord_y = img_center(2) + (1:new_data_size(2)) - new_img_center(2); 
                elseif ix ~= 2
                    img_coord_y = 1:data_size(2);
                end
            end
            kspace = MRX.i2k( img, [1 2]); 
            img_downsampled = MRX.k2i(kspace(img_coord_x, img_coord_y),[1 2]); 
            end
            
            %=====================================================================
            % Allocate debug output struct elements 
            function output = allocateTensorDebugOutput(MRX, masks)
            % Input: 
            %      - MRX                    MRXCAT instance
            %      - masks                  XCAT masks
            % 
            % Output: 
            %      - output                 Updated struct 
            % 
                if (MRX.Par.scan.respiratorymotion==0)
                    no_xcat_frames = 1; 
                elseif (MRX.Par.scan.respiratorymotion==1) || (MRX.Par.scan.respiratorymotion==2)
                    no_xcat_frames = MRX.Par.scan.frames_xcat; 
                end
                
                map_size = [size(masks,1),size(masks,2),size(masks,3),no_xcat_frames];
                eigval_map_size = [size(masks,1),size(masks,2),size(masks,3),3,no_xcat_frames];
                tensor_map_size = [size(masks,1),size(masks,2),size(masks,3),3,3,no_xcat_frames];
                
                output = struct;
                output.transmural_depth_map = zeros(map_size);
                output.helix_map_pre = zeros(map_size);
                output.helix_map_post = zeros(map_size);
                output.sheet_map = zeros(map_size);
                output.MD_D_map = zeros(map_size);
                output.FA_D_map = zeros(map_size);
                output.MD_Dstar_map = zeros(map_size);
                output.FA_Dstar_map = zeros(map_size);
                output.EigVal_map_D = zeros(eigval_map_size);
                output.EigVec_map_D = zeros(tensor_map_size);
                output.EigVal_map_Dstar = zeros(eigval_map_size);
                output.EigVec_map_Dstar = zeros(tensor_map_size);
            end
            
            %=====================================================================
            % Threshold mask to remove small components
            function mask_thresholded = thresholdMask(MRX, mask, threshold)
            % Input: 
            %      - mask                   binary mask
            %      - threshold              threshold value [0-1]
            % 
            % Output: 
            %      - mask_thresholded       Mask with components removed 
            % 
                L = bwlabel(mask);

                % Determine the amount of pixels per label
                max_labels = max(L(:))+1; 
                bin_counts = histcounts(L,max_labels); 
                bin_counts_norm = bin_counts ./ numel(L); 
                selected_bins = (bin_counts_norm  > threshold); 

                selected_labels = nonzeros((selected_bins .* (0:max_labels-1))); 
                
                mask_thresholded = zeros(size(L)); 
                
                for ix = 1:length(selected_labels)
                    mask_thresholded(L == selected_labels(ix)) = 1; 
                end
                mask_thresholded = logical(mask_thresholded); 
            end
        end
end