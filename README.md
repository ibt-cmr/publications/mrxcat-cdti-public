# MRXCAT-CMR-CDTI
__A Matlab software for numerical simulation of cardiac diffusion and perfusion tensor imaging__

MRXCAT-CMR-CDTI is a Matlab toolbox for realistic numerical simulation of 
Magnetic Resonance (MR)cardiac diffusion and perfusion tensor imaging.
Based on the original MRXCAT implementation by Lukas Wissmann, it employs 
the XCAT phantom to obtain realistic anatomical masks including cardiac 
contraction and respiratorymotion options. MRXCAT-CMR-CDTI simulates 
diffusion and perfusion tissue contrast, MR signal models, 
multiple receiver coils, object noise,off-resonance effects 
(in-plane and through-plane), lesions, and supports
non-Cartesian trajectories.

The original MRXCAT version for cardiac cine and myocardial perfusion MRI 
simulation are still available at: www.biomed.ee.ethz.ch/mrxcat 


How to start
=========================	
1. 	Download the MRXCAT-CMR-CDTI repository in *.zip file or clone from 
    https://gitlab.ethz.ch/ibt-cmr-public/mrxcat-cdti-public. 
	Add the main folder (after unpacking the zip file) to your Matlab path 
    (only the MRXCAT-CMR-CDTI folder, not the @MRXCAT_CMR_CDTI, @MRXCAT_CMR_PERF and @MRXCAT_CMR_CINE classfolders).

2.	Run MRXCAT-CMR-CDTI using the 'example_run_MRXCAT.m' script.
    Full example scripts used for the publication are provided. 

3. 	Adapt the MRXCAT-CMR-CDTI parameters in CDTIpar.m in @MRXCAT_CMR_CDTI to your 
    needs. For a first try, go with the predefined parameters.

4.1 MRXCAT-CMR-CDTI can be called inside the example scripts to additionally start 
    a parallel pool. 

    MRXCAT-CMR-CDTI output data will be stored in a folder named after the parfile.   
    The data output folder will be located in Data\dti_breathhold or Data\dti_freebreathing
    depending on the selected XCAT *.bin file path. 

4.2 Alternatively, one can start MRXCAT-CMR-CDTI by typing
	MRXCAT_CMR_CDTI into the command line.

    Then, select the first XCAT .bin file in the file selection dialog
	(DIFF_act_1.bin). 

	Once the simulation is done, you will get the following files:
	*.cpx		MRXCAT phantom data
	*.msk		XCAT mask data
	*.sen		MRXCAT coil sensitivity maps
	*.noi		MRXCAT noise only
	*_par.mat	MRXCAT parameters

    Note: the XCAT *.bin files can be found under Data\dti_breathhold or
    Data\dti_freebreathing. 

    The output can additionally be stored to *.mat files.  

5.	To display the produced phantom data, run DisplayMRXCAT; and select
	the *.cpx file in the file selection dialog.


Dependencies
============
This code was tested in Matlab R2019b

Some methods require the IRT toolbox by Jeffrey Fessler and the NUFFT wrapper 
by Miki Lustig for non-uniform FFT. If needed, download and extract them 
into your Matlab path from:
http://web.eecs.umich.edu/~fessler/
http://www.eecs.berkeley.edu/~mlustig/Software.html

MRXCAT-CMR-CDTI uses 'smoothn' for smoothing/extrapolation of analytical field maps
- Damien Garcia (2020). smoothn (https://www.mathworks.com/matlabcentral/fileexchange/25634-smoothn), MATLAB Central File Exchange. Retrieved November 17, 2020. 
- Garcia D. Robust smoothing of gridded data in one and higher dimensions with missing values. Computational Statistics & Data Analysis 2010;54:1167-1178.
- Garcia D. A fast all-in-one method for automated post-processing of PIV data. Exp Fluids 2011;50:1247-1259.

The rejection sampling method was written using
- Python version 3.6.9 (Python Software Foundation, https://www.python.org/)
- TensorFlow version 2.1 (https://www.tensorflow.org/). 


Distribution of MRXCAT-CMR-CDTI
==========================================
MRXCAT-CMR-CDTI is distributed in Matlab source code format to support its 
propagation for research purposes. Anyone is allowed to use the MRXCAT-CMR-CDTI
original and/or modified software for non-commercial purposes. If you 
publish research using MRXCAT-CMR-CDTI and MRXCAT, please cite the MRXCAT 
and MRXCAT-CMR-CDTI publications. 

Please do not share code derived from MRXCAT or MRXCAT-CMR-CDTI without permission of 
the MRXCAT and MRXCAT-CMR-CDTI authors.

Original MRXCAT CINE and PERFUSION implementations are available from www.biomed.ee.ethz.ch/mrxcat

MRXCAT-CMR-CDTI is available from https://gitlab.ethz.ch/ibt-cmr-public/mrxcat-cdti-public

(c) Copyright University and ETH Zurich; 2014 Institute for Biomedical Engineering | MRXCAT 
(c) Copyright University and ETH Zurich; 2021 Institute for Biomedical Engineering | MRXCAT-CMR-CDTI extension
	

Contact details
===============
For anything related to MRXCAT:

Sebastian Kozerke
Institute for Biomedical Engineering
University and ETH Zurich
kozerke(at)biomed.ee.ethz.ch


Note about XCAT
===============
XCAT is a separate software, which offers a lot more than the
example *.bin datasets included in MRXCAT. The XCAT software is 
available here: https://olv.duke.edu/xcat


History
=======
yymmdd  au  version	description
----------------------------------------------------------
210503  RVG v1.0    MRXCAT-CMR-CDTI: RELEASE VERSION
