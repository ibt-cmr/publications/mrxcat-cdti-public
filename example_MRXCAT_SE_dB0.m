%=========================================================================%
% Main file to run MRXCAT-CDTI on a local computer. 
% User could open parallel pool if multiple workers are available.
% 
% WEBSITES: https://www.biomed.ee.ethz.ch/mrxcat
%           https://gitlab.ethz.ch/ibt-cmr-public/mrxcat-cdti-public
%
% AUTHORS:  Robbert van Gorkum, Jonathan Weine, Christian Stoeck, Sebastian Kozerke. 
%           Institute for Biomedical Engineering, University and ETH
%           Zurich, 2021
%
%=========================================================================%

% Init all paths 
MRXCAT_CDTI_init_paths;

% Define which par file to use. 
% - These can be found in the @MRXCAT_CMR_CDTI folder.
% - Single-slice off-resonance examples from paper 
parfile = 'CDTIpar_SS_B0_TS_IP'; 
% parfile = 'CDTIpar_SS_B0_TS'; 
% parfile = 'CDTIpar_SS_B0_GT'; 

% Use this binfile path for single-slice static data 
% - Example path Data\dti_breathhold\
binfile_path = fullfile('Data','dti_breathhold');

% Select *.bin file
% - Bin file (Output of XCAT) 
binfile = fullfile(binfile_path,'DIFF_act_1.bin');

% Open local parallel pool (optional)
cluster_profile = 'local';
nWorkers = 1; % Parallel pool is only started when nWorkers > 1 
open_parallel_workers(cluster_profile, nWorkers); 

% Run MRXCAT-CDTI
MRXCAT_CMR_CDTI(binfile, parfile);

% Close parallel pool
close_parallel_workers;