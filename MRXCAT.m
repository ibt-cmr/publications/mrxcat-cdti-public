%=========================================================================
% MRXCAT superclass for all MRXCAT classes
% 
% The MRXCAT superclass contains all common properties and methods of
% MRXCAT_CMR_PERF, MRXCAT_CMR_CINE, and MRXCAT_CMR_CDTI
% 
% NOTE:     Do not call MRXCAT directly, but use MRXCAT_CMR_PERF, MRXCAT_CMR_CINE, or MRXCAT_CMR_CDTI instead.
% 
% WEBSITE: 	http://www.biomed.ee.ethz.ch/mrxcat
%           https://gitlab.ethz.ch/ibt-cmr-public/mrxcat-cdti-public
% 
%=========================================================================

%=========================================================================
% VERSION HISTORY (general MRXCAT; more history in PERF/CINE/CDTI subclasses):
%           140130LW OO IMPLEMENTATION - v1.0
%           140321LW VERSION W/ COMMENTS - v1.2
%           150930LW ADDED MRXCAT_Showcase GUI - v1.3
%           151123LW SAVE ALL PARAMETERS IN .TXT FILE - v1.4alpha
%           170111LW BUGFIX coil calculus - v1.4
%           210420RVG UPDATES FOR CDTI EXTENSION - v1.5
%
% AUTHORS:	Lukas Wissmann, Robbert van Gorkum, Claudio Santelli, Sebastian Kozerke
% 			Institute for Biomedical Engineering, University and ETH Zurich
%
%=========================================================================
classdef MRXCAT < handle
    properties
        Data;
        Sensitivities;
        Mask;
        Par;
        Ksp;
        Filename;
        Version;
    end
    methods
        function MRX = MRXCAT()
            MRX.Version = '1.5';
        end
        %=========================================================================
		% Read XCAT log file generated during .bin file production
        function readLogFile( MRX )
            fname = MRX.Filename;
            % --------------------------------------------------------------------
            %   Update number of frames
            % --------------------------------------------------------------------
            MRX.Par.scan.frames_xcat = length(dir([fname(1:end-5),'*.bin']));
            
            % --------------------------------------------------------------------
            %   Trim file name
            % --------------------------------------------------------------------
            fname = [fname(1:end-9),'log'];
            
            % --------------------------------------------------------------------
            %   Open and read log file
            % --------------------------------------------------------------------
            fid = fopen(fname);
            if ( fid ~= -1 )
                while ( ~feof( fid ) )
                    str = fscanf( fid, '%s', 1 );
                    
                    if     strcmp(str, 'array_size'                     ) fscanf(fid,'%s',1); MRX.Par.scan.matrix               = fscanf( fid,'%d',1);
                    elseif strcmp(str, 'myoLV_act'                      ) fscanf(fid,'%s',1); MRX.Par.act.myoLV_act             = fscanf( fid,'%f',1);
                    elseif strcmp(str, 'myoRV_act'                      ) fscanf(fid,'%s',1); MRX.Par.act.myoRV_act             = fscanf( fid,'%f',1);
                    elseif strcmp(str, 'myoLA_act'                      ) fscanf(fid,'%s',1); MRX.Par.act.myoLA_act             = fscanf( fid,'%f',1);
                    elseif strcmp(str, 'myoRA_act'                      ) fscanf(fid,'%s',1); MRX.Par.act.myoRA_act             = fscanf( fid,'%f',1);
                    elseif strcmp(str, 'bldplLV_act'                    ) fscanf(fid,'%s',1); MRX.Par.act.bldplLV_act           = fscanf( fid,'%f',1);
                    elseif strcmp(str, 'bldplRV_act'                    ) fscanf(fid,'%s',1); MRX.Par.act.bldplRV_act           = fscanf( fid,'%f',1);
                    elseif strcmp(str, 'bldplLA_act'                    ) fscanf(fid,'%s',1); MRX.Par.act.bldplLA_act           = fscanf( fid,'%f',1);
                    elseif strcmp(str, 'bldplRA_act'                    ) fscanf(fid,'%s',1); MRX.Par.act.bldplRA_act           = fscanf( fid,'%f',1);
                    elseif strcmp(str, 'body_activity'                  ) fscanf(fid,'%s',1); MRX.Par.act.body_activity         = fscanf( fid,'%f',1);
                    elseif strcmp(str, 'muscle_activity'                ) fscanf(fid,'%s',1); MRX.Par.act.muscle_activity       = fscanf( fid,'%f',1);
                    elseif strcmp(str, 'liver_activity'                 ) fscanf(fid,'%s',1); MRX.Par.act.liver_activity        = fscanf( fid,'%f',1);
                    elseif strcmp(str, 'rib_activity'                   ) fscanf(fid,'%s',1); MRX.Par.act.rib_activity          = fscanf( fid,'%f',1);
                    elseif strcmp(str, 'cortical_bone_activity'         ) fscanf(fid,'%s',1); MRX.Par.act.cortical_bone_activity= fscanf( fid,'%f',1);
                    elseif strcmp(str, 'spine_activity'                 ) fscanf(fid,'%s',1); MRX.Par.act.spine_activity        = fscanf( fid,'%f',1);
                    elseif strcmp(str, 'bone_marrow_activity'           ) fscanf(fid,'%s',1); MRX.Par.act.bone_marrow_activity  = fscanf( fid,'%f',1);
                    elseif strcmp(str, 'art_activity'                   ) fscanf(fid,'%s',1); MRX.Par.act.art_activity          = fscanf( fid,'%f',1);
                    elseif strcmp(str, 'vein_activity'                  ) fscanf(fid,'%s',1); MRX.Par.act.vein_activity         = fscanf( fid,'%f',1);
                    elseif strcmp(str, 'pericardium_activity'           ) fscanf(fid,'%s',1); MRX.Par.act.pericardium_activity  = fscanf( fid,'%f',1);
                    elseif strcmp(str, 'pixel'                          ) str = fscanf( fid, '%s', 1 );
                        if strcmp(str, 'width'                          ) fscanf(fid,'%s',1); MRX.Par.scan.rx_cm                = fscanf( fid,'%f',1); end
                    elseif strcmp(str, 'slice'                          ) str = fscanf( fid, '%s', 1 );
                        if strcmp(str, 'width'                          ) fscanf(fid,'%s',1); MRX.Par.scan.rz_cm                = fscanf( fid,'%f',1); end
                    elseif strcmp(str, 'starting'                       ) str = fscanf( fid, '%s', 1 ); str1 = fscanf( fid, '%s', 1 );
                        if strcmp(str, 'slice') && strcmp(str1, 'number') fscanf(fid,'%s',1); sl_start                          = fscanf( fid,'%f',1); end
                    elseif strcmp(str, 'ending'                         ) str = fscanf( fid, '%s', 1 ); str1 = fscanf( fid, '%s', 1 );
                        if strcmp(str, 'slice') && strcmp(str1, 'number') fscanf(fid,'%s',1); sl_end                            = fscanf( fid,'%f',1); end
                    elseif strcmp(str, '==>Total'                        ) str = fscanf( fid, '%s', 1 ); str1 = fscanf( fid, '%s', 1 );
                        if strcmp(str, 'Output') && strcmp(str1, 'Period') fscanf(fid,'%s',1); MRX.Par.scan.scan_dur            = fscanf( fid,'%f',1); end
                    elseif strcmp(str, 'beating'                        ) str = fscanf( fid, '%s', 1 ); str1 = fscanf( fid, '%s', 1 );
                        if strcmp(str, 'heart') && strcmp(str1, 'period') fscanf(fid,'%s',1); MRX.Par.scan.heartbeat_length     = fscanf( fid,'%f',1); end
                    elseif strcmp(str, 'Respiration motion and beating heart motions included');              MRX.Par.scan.resp = 1; %will be overwritten if Resp option set outside function
                    elseif strcmp(str, 'Respiration motion included only'                     );              MRX.Par.scan.resp = 1; %will be overwritten if Resp option set outside function
                    elseif strcmp(str, 'Beating heart motion included only'                   );              MRX.Par.scan.resp = 0; %will be overwritten if Resp option set outside function
                    end
                end
                fclose( fid );
                if( exist('sl_start', 'var') && exist('sl_end', 'var') ) % number of slices
                    MRX.Par.scan.slices = sl_end-sl_start+1; 
                end
                
                fid = fopen(fname);
                if ( fid ~= -1 )
                    while ( ~feof( fid ) )
                        str = fgets(fid);
                        if     strfind(str, 'Respiration motion and beating heart motions included'); MRX.Par.scan.resp = 1; %will be overwritten if Resp option set outside function
                        elseif strfind(str, 'Respiration motion included only'                     ); MRX.Par.scan.resp = 1; %will be overwritten if Resp option set outside function
                        elseif strfind(str, 'Beating heart motion included only'                   ); MRX.Par.scan.resp = 0; %will be overwritten if Resp option set outside function
                        end
                    end
                end
                fclose( fid );
                
                fprintf('\nPhantom information:\n');
                fprintf('  matrix     :%4d\n',MRX.Par.scan.matrix);
                fprintf('  frames     :%4d\n',MRX.Par.scan.frames_xcat);
                fprintf('  slices     :%4d\n',MRX.Par.scan.slices); 
                fprintf('  resolution :   %1.2f x %1.2f x %1.2f cm3\n\n', MRX.Par.scan.rx_cm, MRX.Par.scan.rx_cm, MRX.Par.scan.rz_cm);
            else
                error('Cannot read XCAT log file. Aborting ... ');
            end
        end
        
        %=========================================================================
		% Read data from XCAT .bin file
        function img = readImgData(MRX,t)
            
            fname = MRX.Filename;
            
            % --------------------------------------------------------------------
            %   in case of more output than input frames, restart at first XCAT frame
            %   after last frame is reached.
            %   !!! Make sure breathing/heart rate fits with number of frames in XCAT parameter file !!!
            %   --> Only done for perfusion, but probably the 2nd condition <-- 
            %   --> (strcmpi(class(MRX), 'MRXCAT_CMR_PERF')) can be removed <--
            % --------------------------------------------------------------------
            if t > MRX.Par.scan.frames_xcat && strcmpi(class(MRX), 'MRXCAT_CMR_PERF')
                t = mod(t,MRX.Par.scan.frames_xcat);
            end
            
            % --------------------------------------------------------------------
            %   Trim file name
            %   Use only the 1st .bin file for breathhold perfusion (1 heart phase)
            % --------------------------------------------------------------------
            if MRX.Par.scan.resp || strcmpi(class(MRX), 'MRXCAT_CMR_CINE')
                fname = [fname(1:end-5),sprintf('%d',t),'.bin'];
            end
            
            % --------------------------------------------------------------------
            %   Trim file name
            % --------------------------------------------------------------------
            if MRX.Par.scan.resp && strcmpi(class(MRX), 'MRXCAT_CMR_CDTI')
                keyWord = 'DIFF_act_'; 
                k = strfind(fname, keyWord);
                fname = [fname(1:(k+length(keyWord)-1)),sprintf('%d',t),'.bin'];
            end
            
            % --------------------------------------------------------------------
            %   Open data file
            % --------------------------------------------------------------------
            fid = fopen(fname);
            
            if ( fid ~= -1 )
                img = fread(fid,'single');
                img = reshape(img,MRX.Par.scan.matrix,MRX.Par.scan.matrix,[]);
                
                % If active parameter, only load selected slices 
                if isfield(MRX.Par.scan,'selectedslices')
                    if ~isempty(MRX.Par.scan.selectedslices)
                        img = img(:,:,MRX.Par.scan.selectedslices);
                    end
                end
                
                [xdim,ydim,zdim] = MRX.computeBoundingBox;
                
                % crop image to bounding box
                img  = img(xdim,ydim,zdim);
                
                fclose( fid );
            else
                fprintf([fname,' cannot be read\n']);
            end
            img = single(img);
        end
        
        %=========================================================================
		% Add Gaussian noise to image
        function [img,nois] = addNoise(MRX, img)
            % -------------------------------------------------------------
            %   Set random number generator 
            %   - If rng(0), the noise that is added will always be
            %     identical for every simulated dataset. 
            %   - Set MRX.Par.proc.rngseednoise = []; to generate 
            %     unique noise for every simulation.  
            % -------------------------------------------------------------
            if isfield(MRX.Par.proc,'rngseednoise')
                if ~isempty(MRX.Par.proc.rngseednoise)
                    s = rng; 
                    rng(MRX.Par.proc.rngseednoise);
                end
            end
            
            stdev = MRX.Par.scan.noisestd;
            nois  = stdev*complex(randn(size(img)),randn(size(img)));
            img   = img + nois;
            
            if isfield(MRX.Par.proc,'rngseednoise')
                if ~isempty(MRX.Par.proc.rngseednoise)
                    rng(s); % Reset to initial value
                end
            end 
        end
        
        %=========================================================================
		% Compute bounding box based on MRX.Par.scan.bbox (0 < parameters < 1)
        function [xdim,ydim,zdim] = computeBoundingBox( MRX )
            
            xdim = fix((MRX.Par.scan.bbox(1)*MRX.Par.scan.matrix+1):MRX.Par.scan.bbox(4)*MRX.Par.scan.matrix);
            ydim = fix((MRX.Par.scan.bbox(2)*MRX.Par.scan.matrix+1):MRX.Par.scan.bbox(5)*MRX.Par.scan.matrix);
            zdim = fix((MRX.Par.scan.bbox(3)*MRX.Par.scan.slices+1):MRX.Par.scan.bbox(6)*MRX.Par.scan.slices);
            
            if( isempty(xdim) || isempty(ydim) || isempty(zdim) )
                error('Bounding box dimension empty! Increase BoundingBox size or XCAT size');
            end
            
            % adapt ydim to multiple of possible k-t factors
            f = factor(MRX.Par.scan.frames);
            if (length(f) < 2 || f(1)~=2 ) && ~strcmpi(class(MRX), 'MRXCAT_CMR_CDTI')
                warning('Number of time points should be multiple of 2 and no prime number for k-t studies!');
            end
            if isfield(MRX.Par.scan, 'adaptPhaseEncDim') && MRX.Par.scan.adaptPhaseEncDim
                rdy   = rem(length(ydim),MRX.Par.scan.frames);
                ydim = ydim(1)+ceil(rdy/2):ydim(end)-floor(rdy/2);
            end
            
            if( length(xdim) ~= length(ydim) && ~strcmpi(MRX.Par.scan.trajectory, 'Cartesian') )
                % square FOV for radial resampling
                dly  = length(ydim)-length(xdim);
                xdim = xdim(1)-floor(dly/2):xdim(end)+ceil(dly/2);
                % adjust FOV for radial (radial Nyquist requirement)
                samp=round(length(xdim)*pi/2);
                xdimplus = (samp-length(xdim))/2;
                xdim = xdim(1)-floor(xdimplus):xdim(end)+floor(xdimplus);
                ydim = ydim(1)-floor(xdimplus):ydim(end)+floor(xdimplus);
                % !!! add check for negative indices here !!!
            end
        end
        
        %=========================================================================
		% Calc voxelwise coil maps
        function [sen,sen_DS] = calculateCoilMaps( MRX )
            % -----------------------------------------------------------------
            % Bounding box indices
            % -----------------------------------------------------------------
            [xdim,ydim,zdim] = MRX.computeBoundingBox;
            
            if MRX.Par.scan.coils>1
                
                nc = MRX.Par.scan.coils;
                rx = MRX.Par.scan.rx_cm*10; % [mm] voxel size
                rz = MRX.Par.scan.rz_cm*10; % [mm] slice width
                
                % -----------------------------------------------------------------
                % Coil centre locations, coil radius
                % -----------------------------------------------------------------
                [cc,R] = MRX.coilCentres; %nc coils, 450mm radius (??), 600 mm coil array length => replace/remove
                
                % -----------------------------------------------------------------
                % Define rotation
                % -----------------------------------------------------------------
                a = 0; %MRX.Par.scan.rotation(1); % (needs debugging for correct rotation!)
                b = 0; %MRX.Par.scan.rotation(2); % (needs debugging for correct rotation!)
                c = 0; %MRX.Par.scan.rotation(3); % verified!
                rotx = [1,0,0; 0,cos(a),-sin(a); 0,sin(a),cos(a)];
                roty = [cos(b),0,sin(b); 0,1,0; -sin(b),0,cos(b)];
                rotz = [cos(c),-sin(c),0; sin(c),cos(c),0; 0,0,1];
                invr = inv(rotx*rotz*roty);
                
                % -----------------------------------------------------------------
                % angles for integration
                % -----------------------------------------------------------------
                angles = 60;
                dtheta = 2*pi/angles;
                theta = -pi:dtheta:pi-dtheta;
                
                % -----------------------------------------------------------------
                % voxel coordinates with origin in image centre
                % -----------------------------------------------------------------
                x = 0:1:length(xdim)-1;
                x = x-x(end)/2;
                y = 0:1:length(ydim)-1;
                y = y-y(end)/2;
                z = 0:1:length(zdim)-1;
                z = z-z(end)/2;

                % -----------------------------------------------------------------
                % convert voxel coordinats to mm
                % -----------------------------------------------------------------
                x = x*rx;
                y = y*rx;
                z = z*rz;
                
                % -----------------------------------------------------------------
                % calculate sensitivity for each coil
                % -----------------------------------------------------------------
                sen = zeros(numel(x),numel(y),numel(z),nc);
                [~,~,~,T] = ndgrid(x,y,z,theta); % T is the same for all coils
                t = 0; % tictoc counter
                sinT = sin(T);
                cosT = cos(T);
                for i=1:nc
                    tic; fprintf('Calculating coil sensitivities for coil %2d / %2d (time elapsed: %6.1f s)\n',i,nc,t);
                    ci = cc(i,:);
                    ang = cart2pol(cc(i,2),cc(i,1),cc(i,3));
                    
                    % vector from all voxels to coil centre
                    % (equivalent to [Y,X,Z,T] = ndgrid(x-c(1),x-c(2),z-c(3),theta), but more efficient)
                    [X,Y,Z] = ndgrid(x,y,z);
                    imrot   = invr*[X(:),Y(:),Z(:)]';
                    X = reshape(imrot(1,:)-ci(2),[length(x),length(y),length(z)]);
                    Y = reshape(imrot(2,:)-ci(1),[length(x),length(y),length(z)]);
                    Z = reshape(imrot(3,:)-ci(3),[length(x),length(y),length(z)]);
                    X = repmat(X,[1,1,1,angles]);
                    Y = repmat(Y,[1,1,1,angles]);
                    Z = repmat(Z,[1,1,1,angles]);
                    
                    % calculate x,y,z components and integrate them
                    sina = sin(ang);
                    cosa = cos(ang);
                    % denominator
                    denom = (R^2) + X.^2+Y.^2+Z.^2;
                    denom = denom - 2*R*(-X.*cosT.*sina+Y.*cosT.*cosa+Z.*sinT);
                    denom = abs(denom).^(3/2);
                    % nominators (xyz)
                    nomx = R.*( Y.*cosT+Z.*sinT.*cosa-R*cosa);
                    nomy = R.*(-X.*cosT+Z.*sinT.*sina-R*sina);
                    nomz = R.*(-Y.*sinT.*sina-X.*sinT.*cosa);   % v1.4: sign bugfix Javier Royuela del Val
                    % sensitivity components (xyz)
                    sx = nomx./denom;
                    sx = dtheta*sum(sx,4); % integration over theta
                    sy = nomy./denom;
                    sy = dtheta*sum(sy,4);
                    sz = nomz./denom;
                    sz = dtheta*sum(sz,4);
                    
                    % angle in yz plane
                    angy = sina.*(X(:,:,:,1)+ci(2))-cosa.*(Y(:,:,:,1)+ci(1));
                    angz = Z(:,:,:,1);
                    yz = angle(angy+1i.*angz);
                    
                    % calculate sensitivity
                    sen(:,:,:,i) = cosa*sx+sina*sy+1i.*( (-sina.*sx+cosa.*sy).*cos(yz) + sz.*sin(yz));
                    clear nomx nomy nomz denom sx sy sz;
                    if i<nc, for k=1:73, fprintf('\b'); end; end
                    t = t+toc;
                end
                % reset contrast concentrations
                MRX.Par.contrast.cm.lv=0;
                MRX.Par.contrast.cm.rv=0;
                MRX.Par.contrast.cm.la=0;
                MRX.Par.contrast.cm.ra=0;
                MRX.Par.contrast.ca.lv=0;
                MRX.Par.contrast.ca.rv=0;
                MRX.Par.contrast.ca.la=0;
                MRX.Par.contrast.ca.ra=0;
                % normalize sens
                msen    = mean(sen(sen~=0));
                sen     = 1/msen*(real(sen)+1i*imag(sen));
            else
                sen = ones(length(xdim),length(ydim),length(zdim),1);
            end
            
            % Normalization
            % - Sum of max abs sen value should be 1 
            for ci = 1:size(sen,4)
                tmp = sen(:,:,:,ci);       
                sen(:,:,:,ci) = sen(:,:,:,ci) ./ max(abs(tmp(:))) ./ size(sen,4);       %SK Bugfix
            end
            
            % Adjust FOV of sensitivities
            sen = MRX.cropImage( sen ); 
            
            % Downsample the coil sensitivities for retrospective reconstruction
            switch lower(MRX.Par.enc.preparationdirection)
                case 'ap'
                    sen_DS = imresize(permute(sen,[2 1 3 4]),[MRX.Par.enc.nx, MRX.Par.enc.ny]);         

                case 'lr'
                    sen_DS = imresize(sen,[MRX.Par.enc.nx, MRX.Par.enc.ny]);         
            end
        end
        
        %=========================================================================
        % Calc coil centres on circles around body
        function [cc, rcoil] = coilCentres( MRX )
            % Generate coil centres on cylinder surface for sensitivity-encoded MR simulation
            %
            % Input:    MRX                         MRXCAT object
            %
            % Parameters used:
            %           MRX.Par.scan.coils          number of coils
            %           MRX.Par.scan.coildist       body radius = radius of coil array in ('AP','RL') plane
            %           MRX.Par.scan.coilsperrow    max. number of coils per coil array row / "ring" / circle
            %
            % Output:   cc [mm]         			coil centre xyz coordinates as [ncoils, 3] matrix
			%           rcoil [mm]					coil radius
            %

            nc        = MRX.Par.scan.coils;                    % No of coils
            rbody_mm  = MRX.Par.scan.coildist;                 % distance of coil centres from image centre
            c_per_row = MRX.Par.scan.coilsperrow;              % No of coils per full circle on cylinder surface
                                                                % Set this parameter to max. 8

            % Define specific angular distributions (low #coils)
            % For >6 coils, equal distribution from 0 to 2 PI is assumed
            angles2 = [150, 210] * pi/180;                   % [rad] 2 anterior coil elements
            angles3 = [130, 180, 230] * pi/180;              % [rad] 3 anterior coil elements
            angles4 = [150, 210, 330, 30] * pi/180;          % [rad] 2 anterior, 2 posterior
            angles5 = [130, 180, 230, 330, 30] * pi/180;     % [rad] 3 anterior, 2 posterior
            angles6 = [130, 180, 230, 310, 0, 50] * pi/180;  % [rad] 3 anterior, 3 posterior
            
            % Determine number of coil rows ("rings") and number of coil elements per ring
            if rem(nc,c_per_row)
                compl_rows = max(floor((nc-c_per_row)/c_per_row),0);
                remc  = nc-compl_rows*c_per_row;
                % divide into 2 rows with as equal partition as possible
                if remc>c_per_row
                    remc = [floor(remc/2),ceil(remc/2)];
                end
            else
                compl_rows = nc/c_per_row;
                remc  = [];
            end
            rings = compl_rows + numel(remc);
            switch numel(remc)
                case 0
                    nc_ring(1:rings)    = c_per_row;
                case 1
                    nc_ring(1)          = remc;
                    nc_ring(2:rings)    = c_per_row;
                case 2
                    nc_ring(1)          = remc(1);
                    nc_ring(2:rings-1)  = c_per_row;
                    nc_ring(rings)        = remc(2);
            end
            
            % z coordinate based on coil element radius
            if any(nc_ring>6)
                minang = 2*pi/max(nc_ring);
            else 
                minang = 50*pi/180;
            end
            rcoil = 1/2*rbody_mm*minang;
            z     = -(rings-1)*rcoil:2*rcoil:(rings-1)*rcoil;
            
            % x-y coordinates
            cc    = zeros(nc,3);
            lctr  = 0; % index counter
            for k=1:rings
                switch nc_ring(k)
                    case 1
                        [x{k}, y{k}] = pol2cart(0, rbody_mm);
                    case 2
                        [x{k}, y{k}] = pol2cart(angles2, rbody_mm);
                    case 3
                        [x{k}, y{k}] = pol2cart(angles3, rbody_mm);
                    case 4
                        [x{k}, y{k}] = pol2cart(angles4, rbody_mm);
                    case 5
                        [x{k}, y{k}] = pol2cart(angles5, rbody_mm);
                    case 6
                        [x{k}, y{k}] = pol2cart(angles6, rbody_mm);
                    otherwise % equally distribute on circle
                        c = nc_ring(k);
                        [x{k}, y{k}] = pol2cart(linspace(0,2*pi*(c-1)/c,c),rbody_mm);
                end
                % assign coil centre positions (x,y,z)
                cc(lctr+1:lctr+length(x{k}),1:3) = [x{k}', y{k}', z(k)*ones(length(x{k}),1)];
                lctr = lctr + length(x{k});
            end
            
        end
             
        %=========================================================================
		% Multiply data w/ coil sensitivity maps
        function img = multiplyCoilMaps( MRX, img, sen)
            img = bsxfun(@times,img,sen); 
        end
       
        %=========================================================================
		% Create file name for saving MRXCAT phantom data
        function filename = generateFilename(MRX, img)
            % determine short filename
            [p,f,~] =   fileparts(MRX.Filename);
            
            % Create folder based on par-file name 
            p = fullfile(p,MRX.Par.proc.parname); 
            
            if exist(p, 'dir') ~= 7 % Check if folder already exists
                mkdir(p); 
            end
            
            fname   =   [p,filesep,f(1:4)];
            
            % create different strings to be concatenated in a filename string
            if strcmpi(class(MRX), 'MRXCAT_CMR_CDTI') % Cardiac diffusion
                if MRX.Par.scan.respiratorymotion == 0 % Static 
                    bhstr = '_st';
                elseif MRX.Par.scan.respiratorymotion == 1 % Breath-hold 
                    bhstr = '_bh';
                elseif MRX.Par.scan.respiratorymotion == 2 % Free-breathing 
                    bhstr = '_fb';
                end 
            else
                if MRX.Par.scan.resp==0
                    bhstr = '_bh';
                else
                    bhstr = '_fb';
                end
            end
            
            resolstr = ['_',num2str(MRX.Par.scan.rx_cm*10),'x',num2str(MRX.Par.scan.rx_cm*10),'x',num2str(MRX.Par.scan.rz_cm*10),'mm'];
            matstr = ['_',num2str(size(img,1)),'x',num2str(size(img,2)),'x',num2str(size(img,3)),'x',num2str(MRX.Par.scan.frames)];
            if MRX.Par.scan.coils>1
                matstr = [matstr,'x',num2str(MRX.Par.scan.coils)];
            end
            snrstr      = ['_snr',num2str(MRX.Par.scan.snr)];
            fastr       = ['_fa',num2str(round(MRX.Par.scan.flip*180/pi))];
            % case separation between perf and other phantom types
            if strcmpi(class(MRX), 'MRXCAT_CMR_PERF') % perfusion
                dosestr     = ['_dose',num2str(MRX.Par.contrast.dose)];
                tshiftstr   = ['_tshift',num2str(MRX.Par.contrast.tshift)];
                if MRX.Par.contrast.rs == 2
                    restStress = 'Stress';
                elseif MRX.Par.contrast.rs == 1
                    restStress = 'Rest';
                else
                    restStress = '';
                end
                filename = [fname,restStress,resolstr,matstr,snrstr,dosestr,fastr,tshiftstr,bhstr];
            else % cine, ...
                filename = [fname,resolstr,matstr,snrstr,fastr,bhstr];                
            end
        end
        
        %=========================================================================
		% Save image data (cpx), mask (msk), noise (noi) and sensitivities (sen)
        function saveImgData( MRX, img, msk, nois, sen, t)
            % Note: MRXCAT-CMR-CDTI has a version of saveImgData in its own
            % class
            
            % --------------------------------------------------------------------
            %   Generate filename and append extensions
            % --------------------------------------------------------------------
            fname = MRX.generateFilename( img );
            fimg = [fname,'.cpx'];
            fmsk = [fname,'.msk'];
            fnoi = [fname,'.noi'];
            fsen = [fname,'.sen'];
            ftxt = [fname,'.txt'];  % save MRXCAT parameters to text file 
                        
            % --------------------------------------------------------------------
            %   Write/append data file
            % --------------------------------------------------------------------
            if t==1
                fprintf('\nOutput file information:\n');
                fprintf('  matrix :%4d x%4d x%4d\n',size(img,1),size(img,2),size(img,3));
                fprintf('  frames :%4d\n',MRX.Par.scan.frames);
                fprintf('  coils  :%4d\n',MRX.Par.scan.coils);
                
                fidimg = fopen(fimg,'w');
                fidmsk = fopen(fmsk,'w');
                fidnoi = fopen(fnoi,'w');
                fidsen = fopen(fsen,'w');
                
                % write parameters to text file (PERFpar/CINEpar)
                fidtxt = fopen(ftxt,'w');
                if strcmpi( class(MRX), 'MRXCAT_CMR_PERF' );
                    parf = which('PERFpar');
                    fidpar = fopen(parf,'r');
                elseif strcmpi( class(MRX), 'MRXCAT_CMR_CINE' );
                    parf = which('CINEpar');
                    fidpar = fopen(parf,'r');
                end
                parContent = textscan(fidpar,'%s','delimiter','\n','whitespace','');
                endPars    = strfind(parContent{1}, 'Display title');
                endPars    = find(~cellfun('isempty', endPars), 1);
                fprintf(fidtxt, '%% Content of parameter file (CINEpar.m/PERFpar.m)\n\n' );
                for k=3:endPars-2
                    % % copy par file directly
                    % fprintf(fidtxt,'%s\n',parContent{1}{k});
                    
                    % % copy par file, but update parameters with actual MRXCAT object values
                    percentIdx = strfind( parContent{1}{k},'%');
                    if ~isempty( percentIdx )
                        if percentIdx(1) == 1  % commentary line
                            fprintf( fidtxt, '%s\n', parContent{1}{k} );
                        elseif percentIdx(1)>1 % value line
                            strw   = MRX.getFieldName( parContent{1}{k} );
                            if ~isempty( strw )
                                fprintf( fidtxt, '%s\n', strw );
                            else
                                fprintf( fidtxt, '%s\n', parContent{1}{k} );
                            end
                        end
                    else % other line (e.g. empty line)
                        fprintf( fidtxt, '%s\n', parContent{1}{k} );
                    end
                    
                end
                fclose(fidpar);
                fclose(fidtxt);
            else
                fidimg = fopen(fimg,'a');
                fidmsk = fopen(fmsk,'a');
                fidnoi = fopen(fnoi,'a');
                fidsen = fopen(fsen,'a');
            end
            
            if ( fidimg ~= -1 )
                
                dim = size(img);
                
                if length(dim) < 3, dim(3) = 1; end
                img  = [real(img),imag(img)];
                img  = reshape(img,dim(1),dim(2),2,dim(3),MRX.Par.scan.coils);
                img  = permute(img,[3 1 2 4 5]);
                nois = [real(nois),imag(nois)];
                nois = reshape(nois,dim(1),dim(2),2,dim(3),MRX.Par.scan.coils);
                nois = permute(nois,[3 1 2 4 5]);
                sen  = [real(sen),imag(sen)];
                sen  = reshape(sen,dim(1),dim(2),2,dim(3),MRX.Par.scan.coils);
                sen  = permute(sen,[3 1 2 4 5]);
                
                fwrite(fidimg,single(img),'single');
                fwrite(fidmsk,uint8(msk),'uint8');
                fwrite(fidnoi,single(nois),'single');
                fwrite(fidsen,single(sen),'single');
                
                fclose(fidimg);
                fclose(fidmsk);
                fclose(fidnoi);
                fclose(fidsen);
            else
                fprintf([fimg,' cannot be written\n']);
            end
        end
         
        %=========================================================================
		% Save MRXCAT parameters for recon
        function saveParsForRecon( MRX, img)
            % append recon parameters to parameter struct and save it to file
            
            % --------------------------------------------------------------------
            %   Generate filename
            % --------------------------------------------------------------------
            fname   = MRX.generateFilename( img );
            
            fpar = [fname,'_par.mat'];
            xdim = size(img,1);
            ydim = size(img,2);
            zdim = size(img,3);
             
            MRX.Par.acq_ovs        = ones(1,4);
            MRX.Par.acq_matrix     = [xdim ydim zdim 1];        % ACQ matrix
            MRX.Par.acq_ne0        = xdim;
            MRX.Par.acq_ne1        = ydim;
            MRX.Par.acq_ne2        = zdim;
            MRX.Par.rec_matrix     = MRX.Par.acq_matrix(1:3);   % RECON marix
            MRX.Par.rec_file_name  = [fname,'.cpx'];
            MRX.Par.proto_array    = [10 11 7 1 0 0 100 0];     % [kt-Factor, ky_train, kz_train, kt-PCA flag, plug in 
                                                                %  training, phase recon, #PCA components, comp-based flag]
            MRX.Par.ncoils         = MRX.Par.scan.coils;        % duplicate for ReconKtData.m
            
            % save par struct
            Par = MRX.Par;
            save( fpar, 'Par');
        end

        %=========================================================================
        % Get MRXCAT object field name corresponding to parameter name
        function strw = getFieldName( MRX, parContent )
            % get MRXCAT object field name for specific parameter name
            % field names are assigned to parameter names in PERFpar.m/CINEpar.m/CDTIpar.m
            % this method performs "the opposite" and is used for saving parameters to text files
            percentIdx = strfind( parContent,'%');
            strFull = strsplit(sscanf(parContent,'%s'),'=');
            parName = strFull{1};
            
            switch parName
                case 'Relaxivity'
                    fieldName = 'contrast.ry';
                case 'MBFrest'
                    fieldName = 'contrast.qr';
                case 'MBFstress'
                    fieldName = 'contrast.qs';
                case 'RestStress'
                    fieldName = 'contrast.rs';
                case 'Fermi_alpha'
                    fieldName = 'contrast.falpha';
                case 'Fermi_beta'
                    fieldName = 'contrast.fbeta';
                case 'ImpulseResponse'
                    fieldName = 'contrast.impulseresp';
                case 'Flip'
                    fieldName = 'scan.flip';  % check for this case separately, as it needs conversion from [rad]->[deg]
                case 'BoundingBox'
                    fieldName = 'scan.bbox';
                case 'CropProfs'
                    fieldName = 'scan.crop';
                case 'LowPassFilt'
                    fieldName = 'scan.lowpass';
                case 'FilterStr'
                    fieldName = 'scan.lowpass_str';
                case 'RespMotion'
                    fieldName = 'scan.resp';
                case 'CropPhaseEnc'
                    fieldName = 'scan.adaptPhaseEncDim';
                case 'RotationXYZ'
                    fieldName = 'scan.rotation'; % check for this case separately, as it needs conversion from [rad]->[deg]
                otherwise
                    if find( strcmpi(fieldnames(MRX.Par.tissue), parName) )
                        groupName = 'tissue';
                    elseif find( strcmpi(fieldnames(MRX.Par.scan), parName) )
                        groupName = 'scan';
                    elseif find( strcmpi(fieldnames(MRX.Par.contrast), parName) )
                        groupName = 'contrast';
                    elseif find( strcmpi(fieldnames(MRX.Par.act), parName) )
                        groupName = 'act';
                    end 
                    if exist('groupName','var')
                        fieldName = [groupName,'.',lower( parName )];
                    else
                        fieldName = '';
                    end
            end
            if ~isempty( fieldName )
                pVal = eval(['MRX.Par.',fieldName]);
                
                % rad->deg for angles
                if strcmpi(fieldName,'scan.rotation') || strcmpi(fieldName,'scan.flip')
                    pVal = rad2deg( pVal );
                end
                if isscalar(pVal)
                    pStr = num2str( pVal );
                elseif ischar(pVal)
                    pStr = ['''',pVal,''''];
                elseif numel(pVal) == 6 % BoundingBox
                    pStr = ['[',num2str(pVal(1,1)),',',num2str(pVal(1,2)),';',num2str(pVal(2,1)),',',num2str(pVal(2,2)),';',num2str(pVal(3,1)),',',num2str(pVal(3,2)),']'];
                elseif numel(pVal) == 3 % RotationXYZ or GoldStdLambda
                    pStr = ['[',num2str(pVal(1)),';',num2str(pVal(2)),';',num2str(pVal(3)),']'];
                end
                strw = [parName, blanks(16-length(parName)), '= ', pStr, ';' ...
                    blanks(30-length(pStr-1)), parContent(percentIdx(1):end)];
            else
                strw = '';
            end
        end
                
        %=========================================================================
        % Crop data to preferred FOV for encoding  
        function croppedImage = cropImage( MRX, data )
            % Input:    MRX                         MRXCAT object
            %           data                        Data to crop
            % 
            % Output:   data_cropped                Cropped data
            %
            data_size = size(data); 
            nx_OVS = MRX.Par.enc.nx_OVS; 
            ny_OVS = MRX.Par.enc.ny_OVS; 
                                    
            % Crop out data 
            AcqResMatrix = [nx_OVS, ny_OVS]; 
            XRange = ceil(-AcqResMatrix(1)/2):ceil((AcqResMatrix(1)/2))-1;
            YRange = ceil(-AcqResMatrix(2)/2):ceil((AcqResMatrix(2)/2))-1;
            
            centerPoints = ceil( (data_size(1:2)+1 )/2);
            
            switch lower(MRX.Par.enc.preparationdirection)
                case 'ap'
                    croppedImage = data(centerPoints(2) + YRange, centerPoints(1) + XRange,:,:);                   
                case 'lr'
                    croppedImage = data(centerPoints(1) + XRange, centerPoints(2) + YRange,:,:);
            end
        end
        
        %=========================================================================
        % Encode the data to k-space (2D encoder)
        function [img_out, kspace_out] = encodeData( MRX, img ) 
            % Input:    MRX                         MRXCAT object
            %           img                         Object image w/contrast
            %                                       multi-coil data 
            %   
            % Output:   img_out          			Encoded image
            %           kspace_out					K-space 
            %
            % Notes 
            % -> Be aware that the term 2*pi in both exponential functions
            %    is applied in previous steps.
            % 
            % Note: Encode data assumes each profile has the same number of samples.

            % Get encoding parameters
            pars = MRX.Par.enc; 
            nx = pars.nx;
            ny = pars.ny; 
            nz = MRX.Par.scan.slices; 

            switch lower(MRX.Par.enc.preparationdirection)
                case 'ap'
                    % Transpose data to frequency-phase encoding axes 
                    img = permute(img,[2 1 3 4]); 
            end
            
            % Get voxel coordinates [rad*mm] 
            [x_coord, y_coord] = MRX.getVoxelCoordinates; 
            
            % Get kspace coordinates [1/mm]
            [kx_coord,ky_coord,FT] = MRX.getKspaceCoordinates;
            no_profiles = size(kx_coord,2); 
            no_samples  = size(kx_coord,1);
            
            % Allocate memory
            out         = zeros(no_samples,no_profiles);
            img_out     = zeros(nx,ny,nz,size(img,4)); 
            kspace_out  = zeros(no_samples,no_profiles,nz,size(img,4)); 

            % Define volume element for integration. 
            % To get correct scaling post-encoding, use the following
            % volume element definition. 
            volume_element = [1 1] ./ pars.OVS;

            %-------------------------------------------------%
            % Loop over all slices
            %-------------------------------------------------%
            for slice_idx = 1:nz
                
                % Assess non-zero elements of current slice 
                % - Use first coil image as default
                img_coil = reshape(img(:,:,slice_idx,1),[],1); 
                nz_idx = find(abs(img_coil)>0);

                % Flatten arrays 
                x_coord_local = x_coord(nz_idx);
                y_coord_local = y_coord(nz_idx);
                
                % Loop over every coil image to encode the data 
                for coil_idx = 1:size(img,4) 
                    
                    out = reshape(out*0,[],1); % Reset
                    
                    % Vectorize and flatten current coil image
                    img_coil = reshape(img(:,:,slice_idx,coil_idx),[],1); 
                    img_coil = img_coil(nz_idx); 

                    for prof_idx = 1:no_profiles
                        parfor ksp_idx = 1:length(kx_coord)
                            %-------------------------------------------------%
                            % Generate phase of Fourier encoding
                            %-------------------------------------------------%
                            % Units: 
                            %   k_x/k_y-coord   = [1/mm]
                            %   x_coord/y_coord = [rad*mm]
                            %-------------------------------------------------%
                            phase = kx_coord(ksp_idx,prof_idx,slice_idx).*x_coord_local  + ky_coord(ksp_idx,prof_idx,slice_idx).*y_coord_local; 

                            %-------------------------------------------------%
                            % Evaluate integral using the summation over all 
                            % image pixels. 
                            %-------------------------------------------------%
                            out(ksp_idx,prof_idx) = (exp(-1i * phase)' * img_coil) * prod(volume_element); % Correct for dx * dy * dz_ts of volume integral 
                        end
                    end
                    ksp = out./sqrt(prod([nx,ny]));
                    kspace_out(:,:,slice_idx,coil_idx) = ksp;
                    img_out(:,:,slice_idx,coil_idx) = FT{slice_idx}'*ksp(:); 
                end
            end
           % Transpose to original orientation 
            switch lower(MRX.Par.enc.preparationdirection)
                case 'ap'
                    img_out = permute(img_out,[2 1 3 4]); 
            end
        end
        
        %=========================================================================
		% Low-pass filter image and mask
        function [img, msk] = lowPassFilter( MRX, img, msk )
        % Input:    MRX                         MRXCAT object
        %           img                         Object image w/contrast
        %                                       (multi-coil data) 
        %           msk                         XCAT masks
        %   
        % Output:   img                         Object image (filtered)
        %           msk                         Masks (filtered)
        %-------------------------------------------------------------%
            if MRX.Par.scan.lowpass
                H    = fspecial('disk',MRX.Par.scan.lowpass_str);
                % filter image
                img  = imfilter(img,H,'replicate');
                % filter mask
                msk1 = imfilter(msk,H,'replicate');
                msk2 = zeros(size(msk));
                for j=[1,5,6,7,8] %myo=1, lv,la,rv,ra=5:8
                    idxr1   = find(round(msk1)==j); %myo
                    [x,y,z] = ind2sub(size(msk),idxr1);
                    for k=1:length(x)
                        msk2(x(k),y(k),z(k)) = j*(msk(x(k),y(k),z(k))==j);
                    end
                end
                msk  = msk2;
                clear msk1 msk2 x y z;
            end
        end
      
        %=========================================================================
        % Iteratively loop over noise standard deviation to refine the SNR 
        function refined_noise_std = refineNoiseStd(MRX, img, sen, roi, stdev_init)
            % Input:    MRX                         MRXCAT object
            %           img                         Object image w/contrast
            %                                       (multi-coil data) 
            %           sen                         Coil-sensitivity maps 
            %           roi                         region-of-interest
            %           stdev_init                  Initial standard
            %                                       deviation
            %   
            % Output:   refined_noise_std        	Updated noise standard
            %                                       deviation 
            %-------------------------------------------------------------%
            it = 1;
            refine_noi_std = true; 
            target_SNR = MRX.Par.scan.snr; 

            fprintf('Iteratively refining noise standard deviation to target SNR...\n'); 
            % -------------------------------------------------------------
            % Perform a check to evaluate the SNR in the myocardium
            % by computing an SNR map at each iteration
            % -------------------------------------------------------------
            while ((refine_noi_std) && it < 100)

                nois  = stdev_init*complex(randn(size(img)),randn(size(img)));
                noisy_img = img + nois;  

                roemer_noisy_img = MRX.combineCoils( noisy_img, sen, 'roe');
                roemer_noise = MRX.combineCoils( nois, sen, 'roe');

                SNRmap = abs(roemer_noisy_img) ./ std(real(roemer_noise(:))); 

                SNRmap_roi = SNRmap .* roi; 
                SNR_roi_mean = mean(nonzeros(SNRmap_roi(:)));
                SNR_roi_std = std(nonzeros(SNRmap_roi(:)));

                fprintf('Myocardial SNR (it: %i) : %.2f +/- %.2f. Target SNR : %.2f)\n', it, SNR_roi_mean, SNR_roi_std, MRX.Par.scan.snr);

                delta_SNR = (SNR_roi_mean - target_SNR); 

                if abs(delta_SNR) < (0.01*target_SNR)
                    refine_noi_std = false; 
                else 
                    if delta_SNR < 0 
                        stdev_init = stdev_init * 0.99;

                    elseif delta_SNR > 0 
                        stdev_init = stdev_init * 1.01;
                    end
                end
                it = it + 1; 
            end

            if refine_noi_std
                error('Could not determine target noise standard deviation');  
            else
                fprintf('Found noise standard deviation to reach target SNR. \n'); 
            end
            
            fprintf('Adding noise w/ std dev : %f\n',stdev_init);
            refined_noise_std = stdev_init;
        end
                
        %=========================================================================
		% Compute the local transmural depth and sector angle map 
        function [transmural_depth_map, sector_angle_map, LV_endo_center] = getLocalAngleAndDepthMaps(MRX, LVmasks, varargin)
            % Input:    MRX                         MRXCAT object
            %           LVmasks                     Left-ventricular masks
            %           varargin                    LV_endo_center 
            %        
            % Output:   transmural_depth_map        Transmural depth map
            %                                       (normalized from 0 to 1)
            %           sector_angle_map            Sector angle map 
            %                                       (Range: [0,2pi>)
            % 
            
            transmural_depth_map = zeros(size(LVmasks,1),size(LVmasks,2),size(LVmasks,3),size(LVmasks,4));
            sector_angle_map = zeros(size(LVmasks,1),size(LVmasks,2),size(LVmasks,3),size(LVmasks,4));  

            for xcat_idx = 1:size(LVmasks,4)
                msk = LVmasks(:,:,:,xcat_idx);

                for slice_idx = 1:size(LVmasks,3)
                    % Compute the edges of the image plane using the mask
                    [~,L] = bwboundaries(msk(:,:,slice_idx));
                    values = unique(L(:));
                    values(values==0)=[];
                    edge_epi = edge(L>=values(1));
                                        
                    if length(values) == 2
                        edge_endo = edge(L>=values(2));
                    else
                        edge_endo = edge_epi*0;
                    end
                    
                    % Calculate Center of gravity of endocardium edge
                    if nargin==3
                        LV_endo_center = varargin{:}; 
                    else
                        % In case the object is a filled disk, use the
                        % epicardial edge only, otherwise revert to the
                        % inner endocardial edge. 
                        if length(values) == 2
                            endo_filled = imfill(edge_endo,'holes');
                            stat = regionprops(endo_filled,'centroid');
                            LV_endo_center = stat.Centroid;
                            LV_endo_center = floor(LV_endo_center);
                        else
                            epi_filled = imfill(edge_epi,'holes');
                            stat = regionprops(epi_filled,'centroid');
                            LV_endo_center = stat.Centroid;
                            LV_endo_center = floor(LV_endo_center); 
                        end
                    end
                    
                    if length(values) == 1
                       edge_endo(LV_endo_center(2):LV_endo_center(2)+1,LV_endo_center(1):LV_endo_center(1)+1) = 1; 
                    end

                    % Determine the shift towards the center points
                    % of the LV endocardial mask for the cart/polar
                    % coordinate systems.
                    mat_size = size(msk); 
                    shift_AP = LV_endo_center(2) - (mat_size(1)/2); 
                    shift_LR = LV_endo_center(1) - (mat_size(2)/2);

                     % Get reference grid for polar coordinates 
                    [Y,X] = meshgrid(1:1:(size(msk,2)),1:1:(size(msk,1)));
                    Y = Y - (size(msk,2)/2) - shift_LR;
                    X = X - (size(msk,1)/2) - shift_AP; 
                    [thetaIm,rhoIm] = cart2pol(X , Y );

                    % Loop over the mask to define transmural depth
                    % and sector angles.
                    for ind_i = 1:mat_size(1)
                        for ind_j = 1:mat_size(2)
                            if msk(ind_i,ind_j,slice_idx)
                                rad_pos = sqrt((ind_i - (mat_size(1)/2) - shift_AP)^2+(ind_j - (mat_size(2)/2) - shift_LR)^2);

                                % Find local polar coordinates    
                                [theta_pos, ~] = cart2pol((ind_i - (mat_size(1)/2) - shift_AP),(ind_j - (mat_size(2)/2) - shift_LR));

                                % Determine local transmural depth 
                                tmp_endo = (abs(thetaIm - theta_pos).*edge_endo);
                                tmp_endo(tmp_endo == 0) = inf; % Set to INF so MIN can still be called for linear indexing
                                tmp_epi = (abs(thetaIm - theta_pos).*edge_epi);
                                tmp_epi(tmp_epi == 0) = inf; % Set to INF so MIN can still be called for linear indexing

                                % Find smallest value, then find idx
                                % belonging to smallest value, then
                                % call the corresponding endo/epi radius
                                [~, idxEndo] = min(tmp_endo(:)); 
                                [~, idxEpi] = min(tmp_epi(:)); 
                                r_endo = rhoIm(idxEndo);
                                r_epi = rhoIm(idxEpi);

                                % Threshold radial positions
                                % exceeding the minimum
                                % endocardial or the 
                                % maximum epicardial 
                                % borders 
                                if rad_pos < r_endo 
                                    rad_pos = r_endo; 
                                end 
                                if rad_pos > r_epi 
                                    rad_pos = r_epi; 
                                end    
                                if((rad_pos >= r_endo) && (rad_pos <= r_epi))
                                    transmural_depth_map(ind_i, ind_j, slice_idx, xcat_idx) = (rad_pos - r_endo) ./ (r_epi - r_endo);
                                    sector_angle_map(ind_i, ind_j, slice_idx, xcat_idx) = (theta_pos + pi) .* (180/pi);
                                end
                            end
                        end
                    end
                end  
            end
        end
        
        %=========================================================================
		% Get masks containing lesions in the left ventricular
		% myocardium
        function lesion_masks = getLesionMasks( MRX, LVmasks )
            % Input:    MRX                         MRXCAT object
            %        
            % Output:   lesion_msks                 Masks containing
            %                                       lesions
            % 
            % - Regular myocardium = labeled 1 
            % - Lesion = labeled 2 
            % 
            lesion_masks = LVmasks;
            
            if strcmpi(MRX.Par.lesion.enable,'yes')              
                for xcat_idx = 1:size(LVmasks,4)

                    msk = lesion_masks(:,:,:,xcat_idx);

                    [transmural_depth_map, sector_angle_map] = getLocalAngleAndDepthMaps(MRX, msk);
                    
                    % Loop over the requested sector angle / transmural
                    % depth values to generate the lesion masks
                    lesion_sector_angles = MRX.Par.lesion.sectorangles;
                    lesion_tmd_ranges = MRX.Par.lesion.tmdranges;

                    nr_lesions = size(lesion_sector_angles,1);  

                    for slice_idx  = 1:MRX.Par.scan.slices

                        lesion_msk_current = lesion_masks(:,:,slice_idx,xcat_idx);   

                        for lesion_idx = 1:nr_lesions

                            tmd_range = lesion_tmd_ranges(lesion_idx,:);
                            sector_angle_range = lesion_sector_angles(lesion_idx,:); 
                            sector_angle_current = sector_angle_map(:,:,slice_idx); 
                            transmural_depth_current = transmural_depth_map(:,:,slice_idx); 

                            % Define current transmural depth map
                            transmural_depth_current(transmural_depth_current<tmd_range(1)) = 0; 
                            transmural_depth_current(transmural_depth_current>tmd_range(2)) = 0; 
                            transmural_depth_current = logical(transmural_depth_current); 

                            % Define current sector angle map
                            sector_angle_current(sector_angle_current<sector_angle_range(1)) = 0; 
                            sector_angle_current(sector_angle_current>sector_angle_range(2)) = 0; 
                            sector_angle_current = logical(sector_angle_current); 

                            % Define lesion area 
                            lesion_area_tmp = transmural_depth_current .* sector_angle_current;
                            lesion_msk_current(find(lesion_area_tmp>0)) = 2; 
                        end
                        lesion_masks(:,:,slice_idx,xcat_idx) = lesion_msk_current;
                    end                    
                end
            end
        end
        
        %=========================================================================
        % Compute the voxel coordinates 
        function [x_coord, y_coord] = getVoxelCoordinates( MRX ) 
            % Input:    MRX                         MRXCAT object
            %
            % Output:   x_coord [rad*mm]            Voxel coordinates x 
			%           y_coord [rad*mm]		    Voxel coordinates y
            %
            % The voxel coordinates are first normalized, then shifted by 0.5
            % to center the coordinates around the image center. 
            
            % Get encoding parameters 
            pars = MRX.Par.enc; 
            
            ny = pars.ny_OVS;
            nx = pars.nx_OVS;

            dx = pars.dx;
            dy = pars.dy;

            %-------------------------------------------------------------%
            % In order to properly encode the data, one needs to take into 
            % account a small 0.5 pixel shift in case a dimension of the 
            % object data has an odd number of pixels.
            %-------------------------------------------------------------%
            if mod(nx,2) == 1 % Uneven
                o1 = (((1:nx)+0.5)/nx - 0.5)*dx; 
            else % Even 
                o1 = (((1:nx))/nx - 0.5)*dx; 
            end 

            if mod(ny,2) == 1 % Uneven
                o2 = (((1:ny)+0.5)/ny - 0.5)*dy; 
            else % Even 
                o2 = (((1:ny))/ny - 0.5)*dy; 
            end  
            
            [x,y] = ndgrid(o1, o2);  
            
            x = rot90(x,2);
            y = rot90(y,2);
            
            x_coord = x(:)*2*pi; y_coord = y(:)*2*pi;
        end
        
        %=========================================================================
        % Compute the k-space coordinates 
        function [kx_coord, ky_coord, FT, no_samples, no_profiles] = getKspaceCoordinates( MRX ) 
            % Input:    MRX                         MRXCAT object
            %
            % Output:   kx_coord [1/m]          k-space coordinates x 
			%           ky_coord [1/m]		    k-space coordinates y
            %           FT                      NUFFT object
            %           no_samples              Number of samples 
            %           no_profiles             Number of profiles
            %
            % Note: EncodeData assumes each profile has the same number of samples. 
            
            % Get encoding parameters 
            pars = MRX.Par.enc; 
            
            ny = pars.ny;
            nx = pars.nx;
            nz = MRX.Par.scan.slices; 
            
            dx = pars.dx;
            dy = pars.dy;
            
            no_profiles = MRX.Par.enc.no_interleaves;
           
            switch lower(pars.readoutmode)
                case 'epi'
                    kx_coord_scaled = zeros(nx*ny,no_profiles); 
                    ky_coord_scaled = zeros(nx*ny,no_profiles); 
                    k = zeros(nx*ny,no_profiles,nz); 
                    w = zeros(nx*ny,no_profiles,nz); 
                    cx_coord = zeros(nx*ny,no_profiles); 
                    cy_coord = zeros(nx*ny,no_profiles); 
           
                   for iz = 1:nz 
                       for prof_idx = 1:no_profiles

                            % Define k-space trajectory for the NUFFT 
                            % - k-space grid in range of nx/ny 
                            % - k-space grid scaled from [-0.5, to 0.5]
                            kx_grid = ((0:nx-1) - fix(nx/2)); 
                            ky_grid = ((0:ny-1) - fix(ny/2));
                            
                            if mod(nx,2) % odd  
                                kx_grid_scaled = kx_grid ./ (nx-1); 
                            else % even
                                kx_grid_scaled = kx_grid ./ nx; 
                            end
                            if mod(ny,2) % odd  
                                ky_grid_scaled = ky_grid ./ (ny-1); 
                            else % even
                                ky_grid_scaled = ky_grid ./ ny; 
                            end
                            
                            [ky_coord_current,kx_coord_current] = meshgrid(ky_grid_scaled, kx_grid_scaled);
                            ky_coord_scaled(:,prof_idx,iz) = ky_coord_current(:); 
                            kx_coord_scaled(:,prof_idx,iz) = kx_coord_current(:); 

                            % Scale data to -0.5 to 0.5 for the NUFFT 
                            k(:,prof_idx,iz) = complex(kx_coord_scaled(:,prof_idx,iz), ky_coord_scaled(:,prof_idx,iz)); 

                            % Define k-space coordinates for the forward model
                            % encoder 
                            % - Scaled using the Nx, Ny parameters 
                            [cy_coord_current,cx_coord_current] = meshgrid(ky_grid,kx_grid);
                            cy_coord(:,prof_idx,iz) = cy_coord_current(:); 
                            cx_coord(:,prof_idx,iz) = cx_coord_current(:); 
                            
                            % Density weighting 
                            w(:,prof_idx,iz) = 1; 
                       end  
                   end
                   
                otherwise 
                    error('Unknown readout mode selected'); 
            end
                        
            % Check if NUFFT toolbox (J.Fessler) and wrapper (M.Lustig) are present
            errormsg = ['NUFFT toolbox by J. Fessler and/or NUFFT wrapper ' ...
                'by M. Lustig not found in Matlab path. ' ...
                'Please download and install from ' ...
                ' http://web.eecs.umich.edu/~fessler/code/index.html' ...
                ' and http://www.eecs.berkeley.edu/~mlustig/Software.html'];
            if exist('NUFFT')<2 || exist('nufft')<2
                error(errormsg);
            else % in case of Matlab version < 2011b, exist is not case-sensitive
                [~,f0]=fileparts(which('nufft'));
                [~,f1]=fileparts(which('NUFFT'));
                % case-sensitive check
                if ~strcmp(f0,'nufft') || ~strcmp(f1,'NUFFT')
                    error(errormsg);
                end
            end
            
            % Define the NUFFT object(s)
            FT{nz} = [];
            for iz = 1:nz
                % Michael Lustig wrapper (magnitude only = 1, complex output = 2)
                FT{iz} = NUFFT(k(:,:,iz),w(:,:,iz),1,[0,0],[nx,ny],2);  
            end
            
            % Ensure k_x/k_y are scaled to the resolution in the image domain
            kx_coord = cx_coord ./ dx; 
            ky_coord = cy_coord ./ dy; 
            
            no_samples = size(cx_coord,1); 
            
            MRX.Par.enc.ksamples = k; 
            MRX.Par.enc.weights = w;
        end
        
        %=========================================================================
        % Generate a timing matrix for the chosen readout method
        function samplingTimes = getSamplingTimes( MRX )
            % Input:    MRX                         MRXCAT object
            %
            % Output:   samplingTimes               Sampling time matrix
            %            
            % Get encoding parameters 
            EncPars = MRX.Par.enc; 
            nx = EncPars.nx;
            ny = EncPars.ny;
           
            switch lower(MRX.Par.enc.readoutmode)
                case 'epi'
                    
                    BW_epi_hz_pixel = EncPars.readoutbandwidth;

                    % Compute arbitrary sampling time matrix
                    samplingTimeVector = linspace(-1,1,nx*ny);
                    samplingTimes = reshape(samplingTimeVector,nx,ny);

                    % Rescale so BW_ky_hz_pix is defined at the kx=0 line 
                    sm = samplingTimes/max(samplingTimes(:));                            
                    sm = sm*1/2/BW_epi_hz_pixel;                                     
                    tlead = (sm(2,1)-sm(1,1))*size(sm,1)/2;                           
                    samplingTimes = (sm(1,1)-tlead)/sm(1,1)*sm;  

                    % Shift by one dt to align set kx=0,ky=0 with time
                    % point 0 for uneven number of Nx 
                    if mod(nx,2) == 0
                        samplingTimes = samplingTimes - samplingTimes(fix(nx/2)+1,fix(ny/2)+1); 
                    end
                    
                    % Actual bandwidth of trajectory along kx=0 line 
                    BW_kx0 = 1 / (samplingTimes(fix(nx/2)+1,ny) - samplingTimes(fix(nx/2)+1,1));
                        
                    % Apply row-flipping (if required) to achieve zig-zag EPI pattern
                    % during encoding 
                    if EncPars.rowflipping
                        samplingTimes(:,1:2:end) = flip(samplingTimes(:,1:2:end),1);       
                    end 
                    
                    % Rotate timing matrix to simulate alternate EPI blip direction
                    if strcmpi(EncPars.flipepiblipdirection,'yes')
                        samplingTimes = fliplr(samplingTimes); 
                    end 
                    
                otherwise 
                    error('Unknown readout mode selected'); 
                    
            end 
            % Assign to MRX object
            MRX.Par.enc.samplingtimes = reshape(samplingTimes,[],1);
        end
    end
	
    methods ( Hidden )
		%=========================================================================
		% Text waitbar (replaces graphical waitbar)
		%=========================================================================
		function strlen = textwaitbar( MRX, t, str_len_in, title )
		% MRX:			MRXCAT instance
		% T:         	between 0 and # MRXCAT frames
        %
		% STR_LEN_IN:   length of previous waitbar
		% TITLE:        optional
		% 
		% HOWTO USE:    E.g. in a for loop, write 
		%               
		%               strlen = 0;
		%               for k=1:N 
		%                   strlen = textwaitbar(MRX, k, strlen, 'Calculating task name');
		%                   .... (tasks WITHOUT command line output)
		%               end 

		% L.Wissmann, 12 Apr 2013
        
        frac    = t/MRX.Par.scan.frames;

		len     = 40;
		numblk  = round(frac*len);
		strlen  = 0;

		if nargin > 1 && ~isempty(str_len_in) && str_len_in > 0
			for k=1:str_len_in
				fprintf('\b');
			end
		else
			fprintf('\n------------------------------------------\n');
		end
		if nargin > 2
			fprintf('%s\n\n',title);
			strlen = strlen + length(title)+2; 
		else
			stdstr = 'Computing ...\n\n'; 
			fprintf(stdstr);
			strlen = strlen + length(stdstr)-2;
		end
		fprintf('[');
		strlen = strlen + 1;
		for k=1:numblk
			fprintf('o');
			strlen = strlen+1;
		end
		for k=1:len-numblk
			fprintf(' ');
			strlen = strlen+1;
		end
		fprintf(']');
		strlen = strlen + 1;

		if frac >= 1
			fprintf('\n\nDone.\n------------------------------------------\n');
		end

        end
    end
    
    methods ( Hidden, Static )
        
        %=========================================================================
		% Coil combine data  
        function img = combineCoils( img, sen, method)
            % Input:    MRX                         MRXCAT object
            %           img                         Input images
            %                                       (multi-coil) 
            %           sen                         Coil-sensitivities
            %           method                      Coil-combine method
            %        
            % Output:   img                         Coil-combined data
            % 
            
            if nargin == 2 
                method = 'roe'; 
            end
            
            switch lower( method )
                case {'roemer','roe'}
                    % ----------------------------------------------- %
                    % Roemer reconstruction 
                    % ----------------------------------------------- %
                    nom = sum(bsxfun(@times, img, conj(sen)),4); 
                    denom = sum(bsxfun(@times, sen, conj(sen)),4); 
                    img = nom ./ denom; 
                    img(isnan(img)) = 0; 

                case {'sos','sum-of-squares'}
                    % ----------------------------------------------- %
                    % Sum-of-squares reconstruction
                    % ----------------------------------------------- %
                    img = sqrt(sum(real(img).*real(img)+imag(img).*imag(img),4));

               otherwise
                    error("Unknown coil-combination method selected"); 
           end
        end
        
        %=========================================================================
        % FFT (image -> k-space)
        %=========================================================================
        function img = i2k(img, dims)
            
            dim_img = size(img);
            
            if nargin < 2
                factor  = prod(dim_img);
                img = 1/sqrt(factor)*fftshift(fftn(ifftshift( img )));
            else
                for dim = dims
                    if size(img,dim)>1
                        img = 1/sqrt(dim_img(dim))*fftshift(fft(ifftshift( img, dim ),[],dim),dim);
                    end
                end
            end
        end
        
        %=========================================================================
        % inverse FFT (k- -> image space)
        %=========================================================================
        function img = k2i(img, dims)
            
            dim_img = size(img); 
            
            if nargin < 2
                factor  = prod(dim_img);
                img = sqrt(factor)*fftshift( ifftn(ifftshift(img)) );
            else             
                for dim = dims
                    if size(img,dim)>1
                        img = sqrt(dim_img(dim))*fftshift( ifft(ifftshift(img,dim),[],dim), dim);
                    end
                end
            end
        end
    end
    
end
