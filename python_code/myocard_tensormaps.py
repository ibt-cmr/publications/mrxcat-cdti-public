""" Module containing convenience functions to generate tensor maps potentially containing
lesions, Helix angle variations and blob-like e2a maps"""

__all__ = ['create_eigenbasis_map', 'get_interpolated_tensormap', 'get_stack_of_e2a_maps', 'get_stack_of_tensor_maps']

from typing import Tuple, Union
import sys

import tensorflow as tf
import numpy as np

import python_code.map_interpolation as interpolation
import cdtipy.coordinates as coordinate_transforms
import cdtipy.utils as utils
from skimage.transform import resize
from skimage.morphology import binary_opening, disk
import cv2


def get_interpolated_tensormap(mask: tf.Tensor, fov: Tuple[float, float], eigen_value_pool: np.ndarray,
                               seed_fraction: float, ha_range: float = 120., interpolation_kernel: str = 'rbf',
                               kernel_kwargs: dict = None,
                               e2a_kwargs: dict = None,
                               helix_map: np.ndarray = None, 
                               sheetlet_map: np.ndarray = None,
                              ):
    """ Constructs an ideal eigen-basis with a specified transmural helix angle variation and a randomly generated
    sheetlet angle map, which is subsequently scaled with a random subsample of the eigen-value-pool with descending
    eigenvalue ordering. The Diffusion-Tensor in slice-coordinates follows as:

    .. math::
        D_{sl} = U \Lambda U^T \quad with \quad U = [ev1, ev2, ev3] \quad (column_i = ev_i)

        and \quad \Lambda=diag(\lambda_1, \lambda_2, \lambda_3) \quad | \quad \lambda_1 >= \lambda_2 >= \lambda_3

    Example values for e2a_kwargs:
                {'low_res_step': 3.5, 'seed_fraction': 1/5, 'prob_high': 0.6, 'distance_weights': (1., 0.6),
                 'offset': 4., 'high_angle_value': 90.}

    :param mask: (X, Y)
    :param fov: (float, float)
    :param eigen_value_pool: (-1, 3) Pool of Eigenvalue triplet that is samples uniformly to get reference values
                                before interpolation.
    :param seed_fraction: float from  [0., 1.]. Determines the number of seedpoints in the mask prior to interpolation.
    :param ha_range: range of helix angle over transmural depth
    :param interpolation_kernel: str from ['rbf', 'nearest']
    :param kernel_kwargs: dict pass-through keyword arguments for kernel defaults to ("kernel_variance":3e-6, "distance_weights":(1., 0.85))
    :param e2a_kwargs: dict pass through keyword argument for e
    :param helix_map: If given no new HA map is created
    :param sheetlet_map: If given no new random sheetlet_map is created    
    :return: (X, Y, 3, 3) interpolated tensor map
    """
    # Construct ideal Tensor-eigen-basis from helix_angle and sheetlet_map
    polar_coordinates, cartesian_coordinates = coordinate_transforms.get_mask_centered_coordinates(np.array(mask),
                                                                                                   np.array(fov))
    if sheetlet_map is None:
        sheetlet_map = utils.fill_dense_map(mask, e2a_map(mask, np.array(fov), **e2a_kwargs))

    if helix_map is None:
        transmural_position = coordinate_transforms.compute_transmural_position(cartesian_coordinates, mask)
        helix_map = ideal_helix_angle_map(transmural_position=transmural_position, angle_range=ha_range)

    eigen_basis, _ = create_eigenbasis_map(cartesian_coordinates, mask, helix_map, sheetlet_map,
                                           homogeneous_handedness=True)

    # Pick random points from mask as interpolation reference points
    mask_indices = tf.where(mask > 0.5)  # noqa
    flat_eigen_basis = tf.gather_nd(eigen_basis, mask_indices)
    flat_polar_coordinates = tf.gather_nd(polar_coordinates, mask_indices)

    n_seeds = int(tf.reduce_sum(tf.cast(mask, tf.float32)) * seed_fraction)
    random_seed_indices = np.random.choice(np.array(range(flat_eigen_basis.shape[0])), replace=False, size=n_seeds)

    random_ref_eigenbasis = tf.gather(flat_eigen_basis, random_seed_indices)
    random_ref_coords_pol = tf.gather(flat_polar_coordinates, random_seed_indices)

    # Get n_seeds eigen-value triplets and scale the eigen-basis sorted in descending order
    random_indices = np.random.choice(np.array(range(eigen_value_pool.shape[0])), size=n_seeds, replace=False)
    ev_sample = tf.sort(tf.gather(eigen_value_pool, random_indices), axis=-1)[..., tf.newaxis, ::-1]
    diagonal = tf.eye(3, 3, batch_shape=random_ref_eigenbasis.shape[0:1]) * ev_sample
    random_ref_diffusion_tensors = tf.einsum('xij, xjm, xmn -> xin', random_ref_eigenbasis, diagonal,
                                             tf.transpose(random_ref_eigenbasis, [0, 2, 1]))

    if interpolation_kernel == 'rbf':
        kernel = interpolation.rbf_interpolation_kernel
    else:
        kernel = interpolation.nearest_neighbour_kernel

    # Interpolate log-Euclidean with
    flat_tensor_maps = interpolation.interpolate_tensor(random_ref_diffusion_tensors, random_ref_coords_pol,
                                                        flat_polar_coordinates,
                                                        interpolation_kernel=kernel,
                                                        distance_metric=lambda gr, ref: interpolation.circular_distance(gr, ref, weights=kernel_kwargs.get("distance_weights", (1., 0.85))),
                                                        kernel_variance=kernel_kwargs.get('kernel_variance', 3e-6))

    return utils.fill_dense_map(mask, flat_tensor_maps)


def create_eigenbasis_map(coordinates: Union[np.ndarray, tf.Tensor], mask: Union[np.ndarray, tf.Tensor],
                          helix_angle_map: tf.Tensor, sheetlet_angle_map: tf.Tensor,
                          homogeneous_handedness: bool = False) -> (tf.Tensor, tf.Tensor):
    """

    :param coordinates: (X, Y, 2)
    :param mask: (X, Y)
    :param helix_angle_map: (X, Y) in radians
    :param sheetlet_angle_map: (X, Y) in radians
    :param homogeneous_handedness: pass through to compute_local_basis
    :return: (local tensor-eigenbasis, local coordinate basis (rad, circ, long))
    """
    helix_angle_map, sheetlet_angle_map = helix_angle_map[..., tf.newaxis], sheetlet_angle_map[..., tf.newaxis]

    coordinates = tf.constant(coordinates, dtype=tf.float32)
    mask = tf.constant(mask, dtype=tf.float32)
    local_coordinate_basis = coordinate_transforms.compute_local_basis_2d(coordinates, mask,
                                                    homogeneous_handedness=homogeneous_handedness).numpy()
    local_rad_bv, local_circ_bv, local_long_bv = [basis_vector for basis_vector in
                                                  local_coordinate_basis.transpose([3, 0, 1, 2])]
    sin_ha = tf.math.sin(helix_angle_map)
    cos_ha = tf.math.cos(helix_angle_map)

    # Assign Eigen-vectors
    ev1 = cos_ha * local_circ_bv + sin_ha * local_long_bv
    ev2 = -sin_ha * local_circ_bv + cos_ha * local_long_bv
    ev3 = local_rad_bv

    # Adjust Sheetlet angle
    sin_sa = tf.math.sin(sheetlet_angle_map)
    cos_sa = tf.math.cos(sheetlet_angle_map)
    temp2, temp3 = tf.constant(ev2), tf.constant(ev3)
    ev2 = - sin_sa * temp3 + cos_sa * temp2
    ev3 = cos_sa * temp3 + sin_sa * temp2

    eigen_vectors = [ev / tf.linalg.norm(ev, axis=-1, keepdims=True) for ev in (ev1, ev2, ev3)]
    local_eigen_basis = tf.stack(eigen_vectors, axis=-1)
    return local_eigen_basis, local_coordinate_basis


def ideal_helix_angle_map(transmural_position: Union[np.ndarray, tf.Tensor], angle_range: float):
    """

    :param transmural_position: (X, Y) 2D map of scalar value representing the transmural position
    :param angle_range: in degree - absolute interval width of linear helix-angle variation over transmural depth
    :return: (X, Y) Helix angle values in radians
    """
    angle_range = angle_range / 180 * np.pi
    helix_angle_map = -1. * (transmural_position * angle_range - angle_range / 2)
    return helix_angle_map


def e2a_map(mask: Union[np.ndarray, tf.Tensor], fov: tf.Tensor, low_res_step: int, seed_fraction: float,
            distance_weights: Tuple[float, float], prob_high: float, offset: float = 0, high_angle_value: float = 90.,
            return_low_res: bool = False, verbose=False, kernel_variance: float = 0.025):
    """ Constructs a sheetlet-angle map that mainly has two populations of of angles. By seeding on a lower resolution
    and interpolating with a nearest neighbour kernel to a higher resolution, the map should look patchy. By weighting
    the distance in circular/radial direction, the shape of the patches can be modified to be more or less elongated.

    The ratio of low/high angle values is initially determined by the parameter prob_high, as the seeds are drawn
    from a binomial distribution yielding 1 with p=prob_high. By 'offset' and 'high_angle_value' this is transformed
    into proper angles.

    :param mask: tf.Tensor/np.ndarray - Binary mask of shape (X, Y)
    :param fov: (float, float) - in meter.
    :param low_res_step: int - factor of down-sampling for seeding
    :param seed_fraction: float [0, 1] - percentage of seed points within low resolution mask / 100
    :param distance_weights: (radial_weight, circular_weight) both ~1. For lower circular weight, the population patches
                             get more elongated in circumferential direction
    :param prob_high: float [0, 1] - probability of binomial distribution yielding a higher angle
    :param offset: see docstring in degree
    :param high_angle_value: see docstring in degree
    :param return_low_res: if True - returns a tuple of two maps (low and high-res)
    :param verbose:
    :param kernel_variance:
    :return: (#pixels in original-resolution-mask, ) in radians
    """
    mask_lowres = tf.round(tf.constant(resize(mask, (mask.shape[0]//low_res_step, mask.shape[1]//low_res_step)),
                                       tf.float32))
    mask_highres = tf.constant(mask, dtype=tf.float32)

    polar_coordinates_lowres = tf.gather_nd(coordinate_transforms.get_relative_polar_coordinates(
                                            mask_lowres, fov, inner_border_offset=1.0), tf.where(mask_lowres > 0.5))
    polar_coordinates_highres = tf.gather_nd(coordinate_transforms.get_relative_polar_coordinates(
                                             mask_highres, fov, inner_border_offset=1.0), tf.where(mask_highres > 0.5))

    number_of_pixels_in_mask = int(tf.reduce_sum(mask_lowres))
    n_seeds = int(number_of_pixels_in_mask * seed_fraction)
    random_seed_indices = np.random.choice(np.arange(0, number_of_pixels_in_mask, dtype=np.int64), replace=False,
                                           size=n_seeds)

    reference_values = tf.constant(np.random.binomial(1, prob_high, size=(n_seeds,)) *
                                   (high_angle_value - offset) + offset, dtype=tf.float32)
    random_ref_coords_pol_lowres = tf.gather(polar_coordinates_lowres, random_seed_indices)

    def weighted_distance(grid_points, reference_coordinates):
        return interpolation.circular_distance(grid_points, reference_coordinates, weights=distance_weights)

    interpolated_values_low_res = interpolation.interpolate_scalar(reference_values, random_ref_coords_pol_lowres,
                                                                   polar_coordinates_lowres,
                                                                   distance_metric=weighted_distance,
                                                                   interpolation_kernel=interpolation.nearest_neighbour_kernel,
                                                                   verbose=verbose)
    interpolated_values_high_res = interpolation.interpolate_scalar(interpolated_values_low_res,
                                                                    polar_coordinates_lowres, polar_coordinates_highres,
                                                                    distance_metric=weighted_distance,
                                                                    interpolation_kernel=interpolation.rbf_interpolation_kernel,
                                                                    kernel_variance=kernel_variance,
                                                                    verbose=verbose)
    if return_low_res:
        return np.deg2rad(interpolated_values_low_res), np.deg2rad(interpolated_values_high_res)
    else:
        return np.deg2rad(interpolated_values_high_res)


def get_stack_of_e2a_maps(reference_values: tf.Tensor, reference_mask: tf.Tensor, masks: Union[np.ndarray, tf.Tensor],
                          fov: Union[Tuple[float, float], np.ndarray, tf.Tensor]) -> tf.Tensor:
    """ Interpolates stack of scalar maps

    :param reference_values: (N or 1, X * Y) - values that are used as reference points for interpolation
    :param reference_mask: (N or 1, X, Y) - coordinates corresponding to the reference values first dimension of axis=1
                                            is transmural depth and second dimenion is angle
    :param masks: (N, M, X, Y)
    :param fov: (float, float)
    :return: tf.Tensor - (N, M, X, Y)
    """
    n_slices, n_motion_states = tf.shape(masks)[0], tf.shape(masks)[1]
    fov = tf.constant(fov, dtype=tf.float32)

    reference_coordinates = tf.gather_nd(coordinate_transforms.get_relative_polar_coordinates(
                     reference_mask, fov, inner_border_offset=0.0), tf.where(reference_mask > 0.5))[tf.newaxis, ...]
    reference_coordinates = tf.tile(reference_coordinates, [n_slices, 1, 1])

    if tf.shape(reference_values)[0] == 1:
        reference_values = tf.tile(reference_values, [n_slices, 1])

    result_outer = tf.TensorArray(reference_values.dtype, size=n_slices, element_shape=tf.shape(masks)[1:],
                                  infer_shape=False)
    for slice_index, masks_per_slice in enumerate(masks):
        result_inner = tf.TensorArray(reference_values.dtype, size=n_motion_states, element_shape=tf.shape(masks)[2:],
                                      infer_shape=False)
        for motion_index, single_mask in enumerate(masks_per_slice):
            polar_coordinates = tf.gather_nd(coordinate_transforms.get_relative_polar_coordinates(
                                             single_mask, fov, inner_border_offset=0.), tf.where(single_mask > 0.5))
            interpolated_values = interpolation.interpolate_scalar(reference_values[slice_index],
                                                                   reference_coordinates[slice_index],
                                                                   polar_coordinates,
                                                                   distance_metric=interpolation.circular_distance,
                                                                   interpolation_kernel=interpolation.nearest_neighbour_kernel,  # noqa
                                                                   batch_size=1000, verbose=False)
            result_inner = result_inner.write(motion_index, utils.fill_dense_map(single_mask, interpolated_values))
            tf.print(f'{slice_index+1}/{n_slices} || {motion_index+1}/{n_motion_states}', end=' ',
                     output_stream=sys.stdout)
        result_outer = result_outer.write(slice_index, result_inner.stack())
    return result_outer.stack()


def get_stack_of_tensor_maps(reference_tensors: tf.Tensor, reference_mask: tf.Tensor,
                             masks: Union[np.ndarray, tf.Tensor],
                             fov: Union[Tuple[float, float], np.ndarray, tf.Tensor]) -> tf.Tensor:
    """ Interpolates stack of scalar maps

    :param reference_tensors: (N or 1, X * Y) - values that are used as reference points for interpolation
    :param reference_mask: (1 , X, Y) - coordinates corresponding to the reference values first dimension of axis=1
                            is transmural depth and second dimenion is angle
    :param masks: (N, M, X, Y)
    :param fov: (float, float)
    :return: tf.Tensor - (N, M, X, Y, 3, 3)
    """
    n_slices, n_motion_states = tf.shape(masks)[0], tf.shape(masks)[1]
    fov = np.array(fov, dtype=np.float32)

    reference_coordinates = tf.gather_nd(coordinate_transforms.get_relative_polar_coordinates(
                     reference_mask, fov, inner_border_offset=0.05), tf.where(reference_mask > 0.5))[tf.newaxis, ...]
    reference_coordinates = tf.tile(reference_coordinates, [n_slices, 1, 1])

    if tf.shape(reference_tensors)[0] == 1:
        reference_tensors = tf.tile(reference_tensors, [n_slices, 1, 1, 1])

    result_outer = tf.TensorArray(reference_tensors.dtype, size=n_slices)
    slices_dataset = tf.data.Dataset.from_tensor_slices((masks, reference_tensors, reference_coordinates))
    for slice_index, (masks_per_slice, ref_tensors_per_slice, ref_coords_per_slice) in slices_dataset.enumerate():
        result_inner = tf.TensorArray(reference_tensors.dtype, size=n_motion_states)
        for motion_index, single_mask in tf.data.Dataset.from_tensor_slices(masks_per_slice).enumerate():
            polar_coordinates = tf.gather_nd(
                coordinate_transforms.get_relative_polar_coordinates(single_mask, fov, inner_border_offset=0.),
                tf.where(single_mask > 0.5))
            interpolated_values = interpolation.interpolate_tensor(ref_tensors_per_slice, ref_coords_per_slice,
                                                                   polar_coordinates,
                                                                   distance_metric=interpolation.circular_distance,
                                                                   interpolation_kernel=interpolation.rbf_interpolation_kernel,
                                                                   kernel_variance=0.5e-4, batch_size=1000,
                                                                   verbose=False)
            result_inner = result_inner.write(tf.cast(motion_index, tf.int32), utils.fill_dense_map(single_mask, interpolated_values))
            tf.print(f'{slice_index+1}/{n_slices} || {motion_index+1}/{n_motion_states}', end=' ',
                     output_stream=sys.stdout)
        result_outer = result_outer.write(tf.cast(slice_index, tf.int32), result_inner.stack())
    return result_outer.stack()
