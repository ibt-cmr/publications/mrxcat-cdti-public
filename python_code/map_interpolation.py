""" This module serves as a collection of functions to interpolate 3x3 positive definite tensors in log-euclidean
space as described by doi: 10.1002/mrm.20965. Additionally it contains the interpolation
method translated for scalar maps to generate e2a or lesion maps
"""
__all__ = ['interpolate_tensor', 'interpolate_scalar', 'euclidean_distance', 'circular_distance', 'rbf_interpolation_kernel']

from typing import Tuple
import sys

import tensorflow as tf
import numpy as np


def euclidean_distance(grid_points: tf.Tensor, reference_coordinates: tf.Tensor):
    """ Calculates the euclidean distance

    :param grid_points: (#points, 2) columns refer to (x, y)
    :param reference_coordinates: (#ref_points, 2) "
    :return: (#points, #ref_points)
    """
    difference_vectors = grid_points[:, tf.newaxis, :] - reference_coordinates[tf.newaxis, :, :]
    distances = tf.linalg.norm(difference_vectors, axis=-1)
    return distances


def circular_distance(grid_points: tf.Tensor, reference_coordinates: tf.Tensor,
                      weights: Tuple[float, float] = (1., 1.)) -> tf.Tensor:
    """ Returns the distance between two points, defined as the line integral along a linearly parameterized curve in
    polar coordinates:

    .. math::
        d = \int_0^1 ||d\gamma(t)/dt||_2 dt \n
        \gamma (t) = r(t) \mathbf{e}_r(t) \n
        r(t) = r_1 + \Delta r * t \quad and \quad \phi (t) = \phi_1 + \Delta \phi * t

    :param grid_points: (#points, 2) columns refer to (r, \phi) transmural_depth / polar angle
    :param reference_coordinates: (#ref_points, 2) "
    :param weights: factor by which delta r and delta phi is weighted
    :return: (#points, #ref_points)
    """

    delta_r = grid_points[:, tf.newaxis,  0] - reference_coordinates[tf.newaxis, :, 0] * weights[0]
    r_1 = grid_points[:, tf.newaxis, 0]
    delta_phi = grid_points[:, tf.newaxis, 1] - reference_coordinates[tf.newaxis, :, 1]
    pi_const = tf.constant(np.pi, dtype=delta_phi.dtype)

    # calculate delta phi as circular
    delta_phi = tf.where(tf.abs(delta_phi) > pi_const, 2. * pi_const - tf.abs(delta_phi), delta_phi) * weights[1]

    def indefinite_integral(t):
        sqrt_term = tf.sqrt(delta_r**2 + delta_phi**2 * (r_1 + t * delta_r)**2)
        ln_term = tf.math.log(sqrt_term + delta_r * delta_phi * t + delta_phi * r_1)
        f1 = (delta_r * t + r_1) / (2 * delta_r)
        f2 = delta_r / (2 * delta_phi)
        return f1 * sqrt_term + f2 * ln_term

    distances = indefinite_integral(1) - indefinite_integral(0)

    # remove Delta=0 entries
    epsilon = 1e-4
    distances = tf.where(tf.math.logical_and(tf.abs(delta_phi) < epsilon, tf.abs(delta_r) < epsilon),
                         tf.zeros_like(distances), distances)
    distances = tf.where(tf.math.logical_and(tf.abs(delta_phi) < epsilon, tf.abs(delta_r) > epsilon),
                         delta_r, distances)
    distances = tf.where(tf.math.logical_and(tf.abs(delta_r) < epsilon, tf.abs(delta_phi) > epsilon),
                         r_1 * delta_phi, distances)

    return tf.abs(distances)


# @tf.function
def rbf_interpolation_kernel(distances: tf.Tensor, **kwargs):
    """ Calculates neighbour interpolation weights according to a gaussian Kernel

    :param distances: (#grid_points, #reference_points)
    :param kwargs:  - kernel_variance: float, defaults to 1e-4.

    :return: tf.Tensor with shape (#grid_points, min(#reference_points))
             - Weights of reference position in sum of neighbours
    interpolation.
    """
    variance = kwargs.get('kernel_variance', 1e-4)
    variance = tf.constant(variance, dtype=tf.float64)
    distances = tf.cast(tf.round(distances * 1e6) * 1e-6, tf.float64)

    kernel_normalization = tf.sqrt(2. * tf.constant(np.pi, dtype=tf.float64) * variance)
    exponential_term = tf.exp(-tf.pow(distances, 2) / (2 * variance))
    kernel_weights = tf.round(exponential_term * 1e6) * 1e-6 / kernel_normalization

    kernel_weights = tf.where(tf.math.is_finite(kernel_weights), kernel_weights, tf.zeros_like(kernel_weights))
    summation_normalization = tf.reduce_sum(kernel_weights, axis=1, keepdims=True)
    return tf.where(summation_normalization > 0, kernel_weights / summation_normalization, tf.zeros_like(kernel_weights))


# @tf.function
def nearest_neighbour_kernel(distances: tf.Tensor, **kwargs):
    """ Calculates neighbour interpolation weights according to a nearest neighbour criterion

    :param distances: (#grid_points, #reference_points)

    :return: tf.Tensor with shape (#grid_points, min(#reference_points))
             - Weights of reference position in sum of neighbours
    interpolation.
    """
    nearest_neigbour_indices = tf.stack([tf.range(distances.shape[0], dtype=tf.int64),
                                         tf.argmin(distances, axis=1, output_type=tf.int64)], axis=1)
    sparse_weights = tf.sparse.SparseTensor(nearest_neigbour_indices,
                                            tf.ones(shape=(nearest_neigbour_indices.shape[0], ), dtype=tf.float32),
                                            dense_shape=tf.cast(tf.shape(distances), tf.int64))
    dense_weights = tf.sparse.to_dense(sparse_weights, default_value=0)
    return dense_weights

@tf.function(experimental_relax_shapes=True)
def interpolate_tensor(reference_tensors: tf.Tensor, reference_coordinates: tf.Tensor, interp_coordinates: tf.Tensor,
                       distance_metric: callable = euclidean_distance,
                       interpolation_kernel: callable = rbf_interpolation_kernel,
                       batch_size: int = 500, **kwargs) -> tf.Tensor:
    """ Interpolates positive definite tensors in log-euclidean space as:

    .. math::
        D_x = expm[\Sigma_i (K(d(x, r_i)) * logm(D_i)) / \Sigma_i K(d(x, r_i)]

    where d(x, .) is the handed distance metric, K(.) is the handed interpolation kernel and logm/expm denote the
    operations Matrix-Logarithm / - Exponential

    :raises tf.errors.InvalidArgument: If distance calculation results in non-finite value.

    :param reference_tensors: (N, 3, 3) flattened array of 3x3 positive definite Tensors
    :param reference_coordinates: (N, 2) flattened array of 2D coordinates that match the definition of the given
                                  distance metric
    :param interp_coordinates: (M, 2) flattened array of coordinates at which the interpolation is performed.
    :param distance_metric: callable(tf.Tensor, tf.Tensor) -> (tf.Tensor) with shapes: [(M, 2), (N, 2)] -> (M, N)
    :param interpolation_kernel: callable(tf.Tensor, **kwargs) -> tf.Tensor with shapes: [(M, N), ...] -> (M, N)
    :param batch_size: (int) number interpolation coordinates handled at the same time
    :param kwargs: only used to pass on arguments to given callables
    :return: tf.Tensor (M, 3, 3) - Interpolated Tensors at interp_coordinates
    """
    reference_coordinates = tf.cast(reference_coordinates, tf.float32)
    interp_coordinates = tf.cast(interp_coordinates, tf.float32)

    reference_tensors = tf.cast(reference_tensors, tf.complex64)
    log_reference_tensors = tf.linalg.logm(reference_tensors)

    interpolated_tensors = tf.TensorArray(tf.float32, size=0, dynamic_size=True, infer_shape=False)
    for idx, coord_batch in tf.data.Dataset.from_tensor_slices(interp_coordinates).batch(batch_size).enumerate():
        distances = distance_metric(coord_batch, reference_coordinates)
        interpolation_weights = interpolation_kernel(distances, **kwargs)
        interpolation_weights = tf.cast(interpolation_weights, tf.complex64)
        log_interpolated_tensors = tf.einsum('mn, nij -> mij', interpolation_weights, log_reference_tensors)
        interp_result = tf.math.real(tf.linalg.expm(log_interpolated_tensors))
        interpolated_tensors = interpolated_tensors.write(tf.cast(idx, tf.int32), interp_result)
        if kwargs.get('verbose', True):
            percent = tf.cast(tf.cast((idx) * 100,  tf.int32) * batch_size / tf.shape(interp_coordinates)[0], tf.int32)
            tf.print(tf.strings.reduce_join(["\rTensor-Interpolation running: ", tf.strings.format("{} %", percent)]),
                     end=' ', output_stream=sys.stdout)
    if kwargs.get('verbose', True):
        tf.print(tf.strings.reduce_join(["\rTensor-Interpolation running: ", tf.strings.format("{} %", (100, ))]),
                 end=' ', output_stream=sys.stdout)
    return interpolated_tensors.concat()


def interpolate_scalar(reference_values: tf.Tensor, reference_coordinates: tf.Tensor,
                       interp_coordinates: tf.Tensor, distance_metric: callable = euclidean_distance,
                       interpolation_kernel: callable = rbf_interpolation_kernel, batch_size: int = 500, **kwargs) -> tf.Tensor:
    """ Interpolates scalar map

    .. math::
        D_x = expm[\Sigma_i (K(d(x, r_i)) * ) / \Sigma_i K(d(x, r_i)]

    where d(x, .) is the handed distance metric, K(.) is the handed interpolation kernel and logm/expm denote the
    operations Matrix-Logarithm / - Exponential

    :raises tf.errors.InvalidArgument: If distance calculation results in non-finite value.

    :param reference_values: (N, ) flattened array of scalar_values
    :param reference_coordinates: (N, 2) flattened array of 2D coordinates that match the definition of the given
                                  distance metric
    :param interp_coordinates: (M, 2) flattened array of coordinates at which the interpolation is performed.
    :param distance_metric: callable(tf.Tensor, tf.Tensor) -> (tf.Tensor) with shapes: [(M, 2), (N, 2)] -> (M, N)
    :param interpolation_kernel: callable(tf.Tensor, **kwargs) -> tf.Tensor with shapes: [(M, N), ...] -> (M, N)
    :param batch_size: (int) number interpolation coordinates handled at the same time
    :param kwargs: only used to pass on arguments to given callables
    :return: tf.Tensor (M, 3, 3) - Interpolated Tensors at interp_coordinates
    """
    reference_coordinates = tf.cast(reference_coordinates, tf.float32)
    interp_coordinates = tf.cast(interp_coordinates, tf.float32)
    reference_values = tf.cast(reference_values, tf.float32)

    interpolated_scalars = tf.TensorArray(tf.float32, size=0, dynamic_size=True, infer_shape=False)
    dataset = tf.data.Dataset.from_tensor_slices(interp_coordinates[:, tf.newaxis]).batch(batch_size)
    for idx, coord_batch in dataset.prefetch(tf.data.experimental.AUTOTUNE).enumerate():
        distances = distance_metric(coord_batch[:, 0], reference_coordinates)
        # _ = tf.debugging.check_numerics(distances, message="Non-finite distance found")
        interpolation_weights = interpolation_kernel(distances, **kwargs)
        interpolation_weights = tf.cast(interpolation_weights, tf.float32)
        interp_result = tf.einsum('mn, n -> m', interpolation_weights, reference_values)
        interpolated_scalars = interpolated_scalars.write(tf.cast(idx, tf.int32), interp_result)
        if kwargs.get('verbose', True):
            percent = tf.cast(tf.cast((idx) * 100,  tf.int32) * batch_size / tf.shape(interp_coordinates)[0], tf.int32)
            tf.print(tf.strings.reduce_join(["\rScalar interpolation running: ", tf.strings.format("{} %", percent)]),
                     end=' ', output_stream=sys.stdout)
    if kwargs.get('verbose', True):
        tf.print(tf.strings.reduce_join(["\rScalar interpolation running: ", tf.strings.format("{} %", (100, ))]),
                 end=' ', output_stream=sys.stdout)
    return interpolated_scalars.concat()




