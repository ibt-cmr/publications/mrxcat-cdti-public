from typing import Tuple, List, Union, Optional
import os
import sys
sys.path.append('../')

import numpy as np
import tensorflow as tf
import scipy.io as sio

import python_code.myocard_tensormaps as myo_module
import python_code.map_interpolation as interpolation
import python_code.sample_eigenvalues as sampling_module
import cdtipy


def load_default_samplers(directory: str):
    """
    :param directory: location of stored default samplers
    """
    sample_pool_names =  ("diffusion_healthy", "diffusion_lesion", "perfusion_healthy", "perfusion_lesion")
    samplers = [sampling_module.MCRejectionSampler.from_savepath(f"{directory}/sampler_{s}.npz",
                                                             sampling_method=sampling_module.exponentially_sample_simplex) 
            for s in sample_pool_names]
    return sample_pool_names, samplers


def main(stack_of_masks: np.ndarray, field_of_view: np.ndarray, samplers: List['MCRejectionSampler'],
         sampler_names: Tuple[str, ...], number: int = 0, save_dir: str = "./", n_samples_drawn: int = 1500,
         e2a_kwargs: dict = None):
    """
    :param samplers: List of MCRejectionSampler to generate sample pools (e.g. diffusion_healthy)
    :param field_of_view: np.array with shape (2, ) specifying the field of view 
    :param stack_of_masks: (N, M, X, Y): N=slices, M=motion_states
    :param int: suffix number that is appended to save name
    :param save_dir: directory into which the result is saved
    :param n_samples_drawn: number of ev-triples sampled by MCRejection sampler. Must be larger than #Pixels in mask * seed_fraction
    :return: None, but saves eigen_values (N,M,X,Y,3) and e2a maaps (N,M,X,Y)
    """
    
    n_slices, n_motion_states = stack_of_masks.shape[0:2]
    
    if e2a_kwargs is None:
        e2a_kwargs = {'low_res_step': 3, 'seed_fraction': 1/5, 'prob_high': 0.43, 'kernel_variance': 0.015, 
                      'distance_weights': (1., 0.8), 'offset': 5., 'high_angle_value': 90.}
    
    for name, sampler in zip(sampler_names, samplers):
        sample_pool  = sampler.get_samples(n_samples_drawn)
        ref_mask = stack_of_masks[0, 0]
        ref_tensor_map = myo_module.get_interpolated_tensormap(ref_mask, field_of_view, eigen_value_pool=sample_pool, seed_fraction=0.125,
                                                               interpolation_kernel='rbf', kernel_kwargs={'kernel_variance':1.5e-6},          
                                                               ha_range=140., e2a_kwargs=e2a_kwargs)
        
        ref_tensors_flat = tf.gather_nd(ref_tensor_map, tf.where(ref_mask))[tf.newaxis]
        stack_of_tensormaps = myo_module.get_stack_of_tensor_maps(ref_tensors_flat, ref_mask, stack_of_masks, field_of_view)
        
        
        save_ev = np.zeros([*(ref_mask.shape), n_slices, n_motion_states, 3], np.float32)
        save_e2a = np.zeros([*(ref_mask.shape), n_slices, n_motion_states], np.float32)
        
        # Save only e2a and eigen values
        for i in range(n_slices):
            for j in range(n_motion_states):
                mets = cdtipy.tensor_metrics.metric_set(stack_of_tensormaps[i, j])
                save_ev[:, :, i, j, :] = mets['evals']
                save_e2a[:, :, i, j] = cdtipy.angulation.angulation_set(mets['evecs'], stack_of_masks[i, j], field_of_view)['E2A']
        sio.savemat(f"{save_dir}/{name}_ev_maps{number}.mat", {"ev_maps": save_ev})
    sio.savemat(f"{save_dir}/e2a_maps{number}.mat", {"e2a_maps": save_e2a})


if __name__ == "__main__":
    
    data_dir = "../Data/dti_freebreathing/evmaps/"
    stack_of_masks = np.array(sio.loadmat(f"{data_dir}/stack_of_masks.mat")["masks"],  dtype=np.float32).transpose(2, 3, 1, 0)
    
    sampler_names, samplers = load_default_samplers("./")
    
    
    
    main(stack_of_masks, fov, samplers[0:1], sampler_names, number= 2, save_dir= "./", n_samples_drawn=1200,
                                        e2a_kwargs = {'low_res_step': 4, 'seed_fraction': 1/5, 'prob_high': 0.55, 'kernel_variance': 0.025,
                                                      'distance_weights': (1., 0.8), 'offset': 5., 'high_angle_value': 90.})